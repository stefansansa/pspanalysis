"""
plot_all_figures.py

This is a high level module to plot the figures for paper 1.

Below are figure numbers with captions and the function used to plot it as of PSP_draft_plasmaproperties
paper on Overleaf (2021/12/14).

Accompanying this module are two pickle files that contain the processed variables from *pspbreaks.py* and the FLD
magnetic field components averaged to 128s (value of *chunk_time* in *env.py*).

Figure 1
--------
function:
plot_summary_components()

calls:
changesign() and calc_parker_models() from this module

caption:
An overview of the solar wind plasma measurements during PSP orbit 5. The solid curves are
analytical estimates of the plasma quantities discussed in the main text.


Figure 2
--------
function:
plot_figure01()

calls:
gettingmagdata() and mag_break_freq_calc() from *pspbreaks.py*, using the defaults in *env.py*.

caption:
Magnetic field spectrum from a solar wind interval measured by PSP. Blue dotted line: initial
spectrum. Grey lines: individual slopes measured over XXX points, and used to determine the 1) green slope
which is the average fit for the inertial range, and 2) red slope which is the average fit for the dissipation range.
The vertical black line represents the break frequency approximation. The green highlighted section defines
the cutoff for the inertial range estimation and the red-highlighted section the cutoff for the dissipation range
estimation.

Figure 3
--------
function:
plot_figure02

calls:
ploterrorbar() from this module
gettingmagdata() from *pspbreaks.py*

caption:
Power spectral distributions at different heliocentric distances. The blue spectrum is from an
interval measured at 0.13 au, the orange spectrum the interval 0.4 au, and the green spectrum as measured
at 0.65 au. Vertical dashed lines indicate the estimated break frequency for each spectrum and the shaded
regions the error estimate as explained in Section 2.


Figure 4
--------
function:
plot_breakfreq

calls:
binbyrad() from this module

caption:
Break frequency, as a function of radial distance, compared with the Duan et al. (2020) power
law estimation fb ∝ r−1.11±0.01 determined from PSP data in the range 0.17 to 0.63 AU. The dashed line
shows a power law fit to our results.

Figure 5
--------
function:
plot_breakwavenumber_meanrad

calls:
ploterrorbar() and calc_parker_models() from this module.

caption:
The calculated breakscale wavenumber kd (given in rad.km−1) as a function of radial distance
(in AU) is shown by blue markers. The orange, green, and red curves show the assumed proton gyroscale,
proton inertial length, and cyclotron wavenumber respectively. The latter approximations are calculated
from the analytical solar wind estimations as described in Section 2.


Figure 6
--------
function:
histbins_breakscalewavenumber

calls:
plothistbin() from this module.

caption:
The breakscale wavenumber estimations are binned into 5 radial intervals and shown in the top
panel, with the wavenumber given in rad.km−1 and radial distance in AU. The corresponding distributions
obtained for each respective binned dataset are shown in the lower panels as a function of wavenumber.


Figure 7
--------
function:
plot_figure04

calls:
plothistbins() and calc_parker_models() from this module.

caption:
The estimated breakscale wavenumber kd, in rad.km−1, versus the radial distance for 10 intervals
ranging from 0.1 to 0.7 AU. The green markers represent the mean kd values, and the orange markers the
median kd values. The vertical error bars indicate the error derived from the break frequency estimates.
Horizontal error bars indicate the radial range covered. Comparison is made with kd reported by Bruno &
Trenchi (2014) for radial distances 0.42 to 5.3 AU (blue solid line). Also shown here are approximations
for the proton gyroscale (green dotted line), proton inertial lengthscale (blue dotted line), and cyclotron
resonance scale (pink dotted line).


Figure 8
--------
function:
histbins_dissipation_index
histbins_inertial_index

calls:
plothistbins() from this module.

caption:
Similar to Fig. 6, but now the value of the inertial range power law index is binned into several
radial intervals.


Figure 9
--------
function:
plot_figure03

calls:
plothistbins() from this module.

caption:
The inertial and dissipation range power law indices as a function of radial distance.

Figure 10
---------
function:

caption:



"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pspbreaks as ppp
import psp_tools as ptools
import env
import plasma_tools as plasma


mpl_params = {
    'axes.titlesize': 16,
    'legend.fontsize': 16,
    'axes.labelsize': 16,
    'xtick.labelsize': 13,
    'ytick.labelsize': 13,
    'text.usetex': False,
    'figure.figsize': [15, 8]
}

mpl.rcParams.update(mpl_params)

# --- AVERAGE SOLAR WIND CONDITIONS NEAR EARTH ---
B_const_earth = 4.75
T_const_earth = 3.9e4
N_const_earth = 7

# FIGURE 1
def plot_summary_components(output_pkl_file='./outputDF2_2020050700_2020061900.pkl.4',
                            fld_pkl_file='./fld_orbit5_128s_avg.pkl',
                            figHeight=env.figH,
                            figWidth=env.figW,
                            legend_loc='best',
                            plot_model_lines=False):
    """
    FOR FIGURE 1 OF THE PAPER

    Read a PKL file with output data and make plot looking like Fig. 2 of Moncuquet etal 2020.

    Args:
        output_pkl_file: [str] Path to the pickle file
        figHeight: [int] Figure height in inch
        figWidth: [int] Figure width in inch
        rlimit_min: [float] Minimum of radial distance between which models shouldn't be plotted
        rlimit_max: [float] Maximum of radial distance between which models shouldn't be plotted
        legend_loc: [str or int] Default legend location, passed to `loc` argument in `matplotlib.pyplot.plot()`

    Returns:
        axs: [matplotlib axes] Set of axes

    """

    model_marker = ''
    model_linestyle = '-'

    # make new figure
    fig1, axs = plt.subplots(nrows=4,
                             ncols=1,
                             sharex=True,
                             figsize=(figWidth, figHeight))

    # read data
    df0 = pd.read_pickle(output_pkl_file)
    df0.mean_rad.interpolate(limit=5, limit_area='inside', inplace=True)

    df = changesign(df0)

    drad = df.mean_rad.mul(1 / plasma.AU_KM)

    bmag = df.modBinst.mul(1e9)

    # get mean VSW from the data
    vsw_mean_km = df.vsw_mean.mean() / 1000
    print('mean VSW = %f' % vsw_mean_km)

    # use calc_parker_models to get the HMF
    r, B_mag_rad, dens_rad, \
    temp_rad, Omega_p, w_p, Valf, \
    Vth_proton_rad, l_d, p_g, p_ii = calc_parker_models(N=100,
                                                        vsw_rad_km=df.vsw_mean.mean() / 1000,
                                                        rad_min=0.1,
                                                        rad_max=drad.max())

    # plot B-field
    axs[0].plot(drad, bmag, ls='', marker='o', color='C0', label='FLD Magnetic field', alpha=0.4)

    if plot_model_lines:
        axs[0].plot(r, B_mag_rad, linewidth=2, linestyle=model_linestyle, marker=model_marker, markersize=3, color='C3',
                    label='Assumed Parker HMF')
        axs[0].plot(-r, B_mag_rad, linewidth=2, linestyle=model_linestyle, marker=model_marker, markersize=3, color='C3')

    axs[0].legend(numpoints=3, loc=legend_loc)
    axs[0].set_yscale('log')
    axs[0].set_ylabel('B [nT]')
    axs[0].grid()

    # plot density
    axs[1].plot(drad, df.meanDensity / 1e6, marker='o', ls='', color='C1', alpha=0.4,
                label='SPI Proton density')

    if plot_model_lines:
        axs[1].plot(r, dens_rad, markersize=3, linestyle=model_linestyle, marker=model_marker, color='C3',
                    label='Modelled proton density')
        axs[1].plot(-r, dens_rad, markersize=3, linestyle=model_linestyle, marker=model_marker, color='C3')
        # axs[1].plot(-r, dens_rad, linewidth=2, linestyle='--', color='C1', label='Proton number density (#/cc)')

    axs[1].legend(numpoints=3, loc=legend_loc)
    axs[1].set_yscale('log')
    axs[1].set_ylabel('Density [$\mathrm{cm^{-3}}$]')
    axs[1].grid()

    # plot temperature
    axs[2].plot(drad, df.meanT, marker='o', ls='', color='C2', alpha=0.4, label='SPI Temperature')

    if plot_model_lines:
        axs[2].plot(r, temp_rad, linestyle=model_linestyle, marker=model_marker,markersize=3, linewidth=2, color='C3',
                    label='Modelled temperature')
        axs[2].plot(-r, temp_rad, linestyle=model_linestyle, marker=model_marker, markersize=3, linewidth=2, color='C3')

    axs[2].set_yscale('log')
    axs[2].set_ylabel('Temperature [K]')
    axs[2].grid()
    axs[2].legend(numpoints=3, loc=legend_loc)

    axs[3].plot(drad, df.vsw_mean / 1000, marker='o', ls='', color='C4', alpha=0.4, label='SPC $\mathrm{V_{SW}}$')
    # axs[3].axhline(y=400, xmin=-0.4, xmax=1, c="blue", linewidth=0.5, zorder=0)

    if plot_model_lines:
        axs[3].axhline(y=vsw_mean_km, color='C4', ls='--', marker='',
                       label='Mean $\mathrm{V_{SW}}$ (%.0f $\mathrm{km.s^{-1}}$)' % vsw_mean_km)

    axs[3].legend(numpoints=3, loc=legend_loc)
    axs[3].set_ylabel('Speed [$\mathrm{km.s^{-1}}$]')
    axs[3].grid()

    fld = pd.read_pickle(fld_pkl_file)
    # remove duplicate indices
    fld = fld[~fld.index.duplicated()]

    # reindex to match df
    fld1 = ptools.newindex(df=fld, ix_new=df.index)
    bx = fld1.BRTN__0
    by = fld1.BRTN__1

    # ### CALCULATE SPIRAL ANGLE ###
    # spiral = calc_spiral_angle(fld1.BRTN__1, fld1.BRTN__0)
    # axs[4].plot(drad, spiral, marker='o', alpha=0.4, ls='')

    axs[-1].set_xlabel('Radial distance [AU]')

    axs[0].set_xlim([1.05 * drad.min(), drad.max() * 1.05])
    plt.tight_layout()
    plt.subplots_adjust(hspace=0.0)

    return axs


# FIGURE 2
def plot_figure01(dtime='2020060700', chunk_plot='r', savefig=True):
    """
    First figure of the paper. It's just a depiction of how the break freq is derived from a power spectral density
    versus freq curve. It shows the PSD-FREQ curve, the multiple linear fits, the inertial and steep regions,
    the estimated break freq and error estimates.

    It's basically the same process employed by WICKS etal (YYYY).

    - First the MAG data from the FIELDS instrument is loaded for the appropriate date (note the correct date format).
    - Then the break frequency is estimated by the `breakfreq_lines()` function. This is the implementation of RWICKS
    method. Called by `mag_break_freq_calc()` in the `pspbreak.py` module.
    - Only the estimate of a single interval (or "chunk") is plotted. Set `chunk_plot = N` to plot chunk number N.
    - A new figure window is opened and figure is plotted, and saved if `savefig = True`.

    Args:
        dtime: [str] The date/time to be loaded. Has to be in format YYYYMMDD to correspond to FIELDS data file name.
        chunk_plot: [str / int] Which chunk should be plotted.  Set `chunk_plot=N` to plot chunk number N. Set
        `chunk_plot='r'` to plot a random one. Set `chunk_plot` to some illogical number like -1 then no plot is made.
        savefig: [bool] Option to save figure with generic file name.

    Returns:
        Nothing

    """

    # set timestamp YYYYMMDD
    dt_user = pd.datetime.strptime(dtime, env.input_time_format)
    dt_user_floor = pd.Timestamp(dt_user).floor(env.mag_time_delta)

    # read data
    mag_df = ppp.gettingmagdata(dt_user_floor, fixdatarate=True, savepoint=False)

    # run break freq calculation, including the plot
    break_freq_vars = ppp.mag_break_freq_calc(mag_df, chunk_plot=chunk_plot)

    # set axis labels
    plt.xlabel('Freq [Hz]')
    plt.ylabel('Power [$\mathrm{nT^2.Hz^{-1}}$]')
    plt.legend()
    plt.grid(which='major')

    # save figure with generic file name
    if savefig:
        figname = 'figure01_%s_chunk_%s' % (dtime, str(chunk_plot)) + env.figfile_ext
        figpath = os.path.join(env.image_dir, figname)
        print('Saving figure to ', str(figpath))
        plt.savefig(figpath)

    return


# FIGURE 3
def plot_figure02(data_filename='./outputDF2_2020050700_2020061900.pkl.4', npoints=2, ix_sel=None):
    """
    Figure 2 of the paper is supposed to be something similar to FIGURE 2 from the paper [@DUAN2020].
    In short, plot the estimated break frequency at various radial distances DRAD. To do this is tedious.

    - Firstly, the output PKL file from the main function `new_work3()` (this module) is loaded. Change the file name
    to load the correct file. The default corresponds to my path and file. The resulting dataframe `delta_t` contains a
    number of variables, including the break frequency `breakfreq` and mean radial distance `mean_rad`.
    - Second step is to throw out all instances where range error or interval error occurs. Range error is where the
    estimated break frequency is beyond the frequencies observed in that interval of MAG data (obviously wrong).
    - Interval error is where estimated break frequency is beyond the inertial range max or dissipation range minimum.
    - Third step. Interpolate radial distance. We can safely interpolate (not extrapolate) because s/c movement is
    regular and predictable.
    - If `ix_sel=None` then make a plot of BREAKFREQ vs DRAD and allow the user to pick `npoints` points. After picking
    the desired number of points, press enter. Be sure to zoom in in order to pick a single point, otherwise more than
    one surrounding point will be selected.
    - The break freq is estimated for every selected point and the estimate plot (like FIG01) is made for each.
    - The final product is a figure with the raw MAG data and the BREAKFREQ estimate for each selected point on the
    same axis.


    Args:
        data_filename: [str] File to be loaded.
        npoints: [int] Number of points to select / plot.
        ix_sel: [list] or `None`. If `None` the user must select points by clicking. If you know the index number of the
        point then input the list of points.

    Returns:
        fb_out: [list] List of dictionaries, one for each selected point. Each dictionary contains the trace of the
        MAG vector (used to get the PSD), the frequencies, timestamp of centre of interval, and all estimated
        quantities (breakfreq, breakfreq error estimates) and error indices (range and interval errors).

    """

    # run like this: ppp.plot_figure02(npoints=3,ix_sel=[2498, 663, 31]); note the list of points.

    # load previous output DF
    print('loading outputDF')
    df = pd.read_pickle(data_filename)

    # set range an interval errors to NAN
    df.mask(df.breakfreq_ei > 0, inplace=True)
    df.mask(df.breakfreq_er > 0, inplace=True)
    df.mask(df.breakfreq_eb > 0, inplace=True)

    # interpolate missing values in DRAD
    print('interpolating mean_rad')
    # drad = df.mean_rad.interpolate(limit=10, limit_area='inside').values / plasma.AU_KM
    df['drad_int_au'] = df.mean_rad.interpolate(limit=10, limit_area='inside').values / plasma.AU_KM

    df.dropna(inplace=True)

    drad = df.drad_int_au.values

    # plot the BREAKFREQ estimate versus DRAD using the `ploterrorbar()` function
    if ix_sel is None:
        print('plot FBREAK vs DRAD')
        fig = ploterrorbar(df.breakfreq.values,
                           df.breakfreq_p.values,
                           df.breakfreq_m.values,
                           ix=df.drad_int_au.values,
                           errlimit=5,
                           pickpoints=True)

        # pick points live
        print('pick %d points' % npoints)

        # pick points off a graph
        ix_sel = test_pickpoints(fig, n=npoints)
        # ix_sel = test_pick2(fig, n=npoints)

        # prompt user to continue
        name = input('Done picking %d points? Press ENTER to continue\n' % npoints)
        print('Indices of selected points: ', ix_sel)

    # initialise FBOUT
    fb_out = list(np.zeros(len(ix_sel)))

    # initialise new figure window
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(env.figW, env.figH))
    ax.set_xscale('log')
    ax.set_yscale('log')

    # initialise list of colors to use in the for-loop
    colslist = ['C0', 'C1', 'C2', 'C3', 'C4']

    for i in range(len(ix_sel)):
        ix = ix_sel[i]

        # get correct timestamp, based on index number
        ix_ts = pd.Timestamp(df.index[ix]).floor(env.mag_time_delta)
        print(ix_ts)

        # load the correct MAG data file
        magdf = ppp.gettingmagdata(dt_user=ix_ts, fixdatarate=True)

        # calculate the time interval corresponding to the selected index
        t0 = df.index[ix] - pd.Timedelta(int(env.chunktime / 2), 'S')
        t1 = df.index[ix + 1] + pd.Timedelta(int(env.chunktime / 2), 'S')

        # get estimate of the break frequency
        fb_out[i] = ppp.mag_break_freq_calc(magdf[t0:t1],
                                            chunk_plot=0)

        # build label for the BREAKFREQ estimate
        fbreak_label = 'R = %.2f AU' % drad[ix]

        # plot the trace of MAG vector versus frequency
        ax.plot(fb_out[i]['freq'][0],
                fb_out[i]['btrace'][0],
                color=colslist[np.mod(i, len(colslist))],
                label=fbreak_label)

        ax.axvline(fb_out[i]['breakfreq'][0],
                   ls='--',
                   lw=2,
                   # color=colslist[np.mod(i, len(colslist))],
                   color='k')

        ax.axvspan(fb_out[i]['breakfreq_em'][0],
                   fb_out[i]['breakfreq_ep'][0],
                   color=colslist[np.mod(i, len(colslist))],
                   alpha=0.3)

    # put legend and grid
    ax.set_xlabel('Frequency [Hz]')
    ax.set_ylabel('PSD [$\mathrm{nT^2.Hz^{-1}}$]')
    ax.legend()
    ax.grid()

    return fb_out


# FIGURE 4
def plot_breakfreq(figHeight=env.figH,
                   figWidth=env.figW,
                   data_filename='./outputDF2_2020050700_2020061900.pkl.4',
                   num_points=10):

    from scipy.optimize import curve_fit
    # import matplotlib as mpl

    # font = {'family': 'normal', 'weight': 'normal', 'size': 26}
    # mpl.rc('font', **font)

    # load data
    df = pd.read_pickle(data_filename)

    # set range an interval errors to NAN
    # df[df.breakfreq_ei == 1] = np.nan
    # df[df.breakfreq_er == 1] = np.nan
    df.mask(df.breakfreq_eb > 0, inplace=True)

    # BINBYRAD function to plot the specified number of DRAD ranges of the inertial and dissipation range indices
    dradbins, bfreq_ix_means, stdev = binbyrad(df,
                                               n_radbins=11,
                                               yvar='breakfreq',
                                               drad='mean_rad',
                                               hist_xlabel='inertial index [-]',
                                               yerr_m_var='breakfreq_m',
                                               yerr_p_var='breakfreq_p')

    # find the centre of the bins
    x = (dradbins[:-1] + np.diff(dradbins) / 2) / plasma.AU_KM

    # plot the bin centre versus inertial and diss index
    fig = plt.figure(figsize=(figWidth, figHeight))
    ax = fig.gca()

    yerrp = stdev[0]
    yerrm = stdev[1]

    yerr_mat = np.zeros((2, len(stdev[0])))
    yerr_mat[0, :] = np.abs(bfreq_ix_means - yerrm)
    yerr_mat[1, :] = np.abs(bfreq_ix_means - yerrp)

    popt, pcov = curve_fit(power_law, x, bfreq_ix_means, p0=[1, 1], bounds=[[1e-3, 1e-3], [1e20, 50]])

    ax.errorbar(x,
                bfreq_ix_means,
                yerr=yerr_mat,
                fmt='o',
                color='C1',
                # mfc=(1, 1, 1, 0.6),
                # mfc='w',
                ecolor='gray',
                capsize=8,
                label='Estimated break frequency',
                lw=2,
                zorder=5,
                markersize=8)

    xlinspace = np.linspace(np.min(x), np.max(x), 100)
    duan_fx = popt[0] * xlinspace ** (-1.11)
    duan_fx_m = popt[0] * xlinspace ** (-1.11 - 0.01)
    duan_fx_p = popt[0] * xlinspace ** (-1.11 + 0.01)

    ax.plot(xlinspace, power_law(xlinspace, *popt),
            ls='--',
            color='black',
            # label='y = $%2.2f x^{-%2.2f}$ (fit to PSP Orbit 5)' % (popt[0], popt[1]),
            # label='Exponential fit ($f_b = %2.2f R^{-%2.2f}$)' % (popt[0], popt[1]),
            label='Power law fit',
            lw=2)

    ax.plot(xlinspace, duan_fx, lw=2)
    # ax.fill_between(xlinspace, duan_fx_m, duan_fx_p, alpha=0.5, label='Duan et al. (2020), $y \propto x^{-1.11 \pm 0.01}$')
    ax.fill_between(xlinspace, duan_fx_m, duan_fx_p, alpha=0.5, label='Duan et al. (2020)')

    # put legend and grid
    ax.set_xlabel('Radial distance [AU]')
    ax.set_ylabel('Break Frequency [Hz]')

    handles, labels = ax.get_legend_handles_labels()

    handles = [handles[1], handles[2], handles[0]]
    labels = [labels[1], labels[2], labels[0]]

    ax.legend(handles, labels, fontsize=mpl_params['legend.fontsize'])
    ax.grid()

    return 999


# FIGURE 5
def plot_breakwavenumber_meanrad(data_filename='./outputDF2_2020050700_2020061900.pkl.4'):
    # df = pd.read_pickle('./outputDF2_2020050700_2020051000.pkl')

    # load data
    df = pd.read_pickle(data_filename)

    # mask errors in break freq
    df.mask(df.breakfreq_ei > 0, inplace=True)
    df.mask(df.breakfreq_er > 0, inplace=True)
    df.mask(df.breakfreq_eb > 0, inplace=True)

    # interpolate mean_rad; it's safe to do so because the SC follows predictable path
    drad = df.mean_rad.interpolate(limit=100, limit_area='inside') / plasma.AU_KM

    # make errorbar plots
    ploterrorbar(df.breakscale_k.values * 1000,
                 xp=df.breakscale_k_p * 1000,
                 xm=df.breakscale_k_m * 1000,
                 ix=drad,
                 errlimit=5,
                 label='Breakscale wavenumber $\mathrm{k_d}$',
                 datcol='C0')

    # use calc_parker_models to get the HMF
    r, B_mag_rad, dens_rad, \
    temp_rad, Omega_p, w_p, Valf, \
    Vth_proton_rad, l_d, p_g, p_ii = calc_parker_models(N=100,
                                                        vsw_rad_km=df.vsw_mean.mean() / 1000,
                                                        rad_min=0.1,
                                                        rad_max=drad.max(),
                                                        B_earth_const=B_const_earth,
                                                        sol_rot=25.4,
                                                        dens_earth_const=N_const_earth,
                                                        temp_earth_const=T_const_earth)

    plt.plot(r, 1 / p_g, linewidth=2, linestyle='--', color='C1', label='Proton gyroscale', zorder=99)
    plt.plot(r, 1 / p_ii, linewidth=2, linestyle='--', color='C2', label='Proton inertial length', zorder=99)
    plt.plot(r, 1 / l_d, linewidth=2, linestyle='--', color='C3', label='Cyclotron resonance scale', zorder=99)

    plt.title(str(df.index[0]) + ' $--$ ' + str(df.index[-1]))
    plt.yscale('log')
    plt.ylabel('Wavenumber [$\mathrm{rad.km^{-1}}$]')
    plt.xlabel('Radial distance [AU]')

    plt.legend(fontsize=mpl_params['legend.fontsize'])
    plt.grid(which='major', axis='both')
    plt.xlim([0, 1])

    return 999


# FIGURE 6
def histbins_breakscalewavenumber(df_fname='./outputDF2_2020050700_2020061900.pkl.4'):


    df = pd.read_pickle(df_fname)

    # get BREAKSCALE wavenumber in units of [1/km] to match [@BRUNO2014]
    df['breakscale_k_km'] = df.breakscale_k.values * 1000
    df['breakscale_k_ep_km'] = df.breakscale_k_p.values * 1000
    df['breakscale_k_em_km'] = df.breakscale_k_m.values * 1000

    # set X and Y parameters to be plotted
    # X is radial distance [AU]
    xpar = df.mean_rad / plasma.AU_KM
    # Y is breakscale wavenumber [rad/km]
    ypar = df.breakscale_k_km

    # label for the Y parameter
    ypar_label = 'Wavenumber $\mathrm{k_d}$ [$\mathrm{rad.km^{-1}}$]'

    x, y = plothistbins(x=xpar,
                        y=ypar,
                        xlabel='R [AU]',
                        ylabel=ypar_label,
                        ax0_yscale='log',
                        equal_len_div=False,
                        figWidth=9,
                        figHeight=9,
                        legend_loc=1,
                        mean_label='',
                        median_label='')

    return 999


# FIGURE 7
def plot_figure04(df=None,
                  num_points=10,
                  data_filename='./outputDF2_2020050700_2020061900.pkl.4',
                  inset_axis=None,
                  legend_location=1):
    """
    Figure 4 of the paper.

    This function creates a plot with estimated breakscale wavenumber versus mean radial distance DRAD.
    The figure is supposed to be similar in format to FIGURE 3A of BRUNO & TRENCHI (2014) paper.

    - load PKL file from the specified file name `data_filename`. This file points to the dataframe produced by the
    function `new_work3()` (this module).
    - Use the nifty `binbyrad` function to plot the breakscale versus DRAD bins. This will generate one figure which
    will not be saved to file - DIY.
    - load the data from the [@BRUNO2014] paper.

    Args:
        df: [pandas.DataFrame or None] Data frame to use. If NONE then file will be loaded
        num_points: [int] Number of radial bins to use
        data_filename: [str] File to be loaded.

    Returns:
        bt3a: [numpy.ndarray] The BRUNO & TRENCHI data.

    """

    from matplotlib.ticker import FormatStrFormatter

    if df is None:
        df = pd.read_pickle(data_filename)
        df[df.breakfreq_er == 1] = np.nan
        df[df.breakfreq_ei == 1] = np.nan

    df.mask(df.breakfreq_eb > 0, inplace=True)
    df.mask(df.breakfreq_er > 0, inplace=True)
    df.mask(df.breakfreq_ei > 0, inplace=True)

    # get BREAKSCALE wavenumber in units of [1/km] to match [@BRUNO2014]
    df['breakscale_k_km'] = df.breakscale_k.values * 1000
    df['breakscale_k_ep_km'] = df.breakscale_k_p.values * 1000
    df['breakscale_k_em_km'] = df.breakscale_k_m.values * 1000

    df.dropna(inplace=True)

    # use PLOTHISTBINS to divide the breakscale along mean_rad divisions
    xdat = df.mean_rad.interpolate(limit=5, limit_area='inside').values
    ydat = df.breakscale_k_km.values

    xsumm, ysumm = plothistbins(y=ydat,
                                x=xdat/plasma.AU_KM,
                                y_error_neg=df.breakscale_k_em_km.values,
                                y_error_pos=df.breakscale_k_ep_km.values,
                                n_intervals=num_points,
                                equal_len_div=True)

    ymeans = np.array(ysumm['mean'][:])
    ymeds = np.array(ysumm['median'][:])
    dradbins = [i for i in xsumm['xmin']]
    dradbins.append(xsumm['xmax'][-1])
    yerr_pm = [ysumm['pos_err'], ysumm['neg_err']]

    yerr_for_errorbar = np.zeros((2, len(ymeds)))
    yerr_for_errorbar1 = np.zeros((2, len(ymeds)))
    yerr_for_errorbar[0, :] = np.abs(ymeans - np.array(ysumm['pos_err'][:]))
    yerr_for_errorbar[1, :] = np.abs(ymeans - np.array(ysumm['neg_err'][:]))
    yerr_for_errorbar1[0, :] = np.abs(ymeds - np.array(ysumm['pos_err'][:]))
    yerr_for_errorbar1[1, :] = np.abs(ymeds - np.array(ysumm['neg_err'][:]))

    # load BRUNO & TRENCHI 2014 (FIG 3A) data
    # row 0 is DRAD [AU] and row 1 is BREAKSCALE [1/km]
    bt3a = np.array([[0.42, 0.56, 0.99, 0.99, 0.99, 1.40, 3.20, 5.28],
                     [0.00906E00, 0.00556E00, 0.00415E00, 0.00340E00, 0.00405E00, 0.00214E00, 8.25906E-4, 5.32073E-4]])

    # get bin centres
    drad_centre = dradbins[:-1] + np.diff(dradbins) / 2

    # plot [@BRUNO2014] data
    fig, ax = plt.subplots()
    ax.plot(bt3a[0, :], bt3a[1, :], marker='o', ls='-', color='C0', label='Bruno & Trenchi (2014)', lw=2)
    ax.tick_params(which='both')

    # plot medians of BREAKSCALE versus DRAD in AU with errorbars indicating DRAD bin width
    ax.errorbar(drad_centre,
                ymeds,
                xerr=np.diff(dradbins) / 2,
                yerr=yerr_for_errorbar1,
                fmt='o',
                color='C1',
                ecolor='gray',
                capsize=3,
                label='$\mathrm{k_d}$ median',
                lw=2)

    ax.errorbar(drad_centre,
                ymeans,
                xerr=np.diff(dradbins) / 2,
                yerr=yerr_for_errorbar,
                fmt='o',
                color='C2',
                ecolor='gray',
                capsize=3,
                label='$\mathrm{k_d}$ mean',
                lw=2)

    # use calc_parker_models to get the HMF
    r, B_mag_rad, dens_rad, \
    temp_rad, Omega_p, w_p, Valf, \
    Vth_proton_rad, l_d, p_g, p_ii = calc_parker_models(N=500,
                                                        vsw_rad_km=df.vsw_mean.mean() / 1000,
                                                        rad_min=0.1,
                                                        rad_max=6)

    ax.plot(r, 1 / p_g, linewidth=2, linestyle=':', color='C3', label='Proton gyroscale')
    ax.plot(r, 1 / (p_ii), linewidth=2, linestyle=':', color='C4', label='Proton inertial length')
    ax.plot(r, 1 / l_d, linewidth=2, linestyle=':', color='C5', label='Cyclotron resonance scale')

    # put labels
    plt.title(str(df.index[0]) + ' -- ' + str(df.index[-1]))
    ax.set_xlabel('Radial distance [AU]')
    ax.set_ylabel('Wavenumber [rad/km]')
    ax.legend(fontsize=mpl_params['legend.fontsize'], loc=legend_location)
    ax.set_yscale('log')
    ax.set_xscale('log')
    ax.grid(which='major', axis='both')

    plt.tick_params(axis='x', which='major')
    plt.tick_params(axis='x', which='minor')
    ax.xaxis.set_major_formatter(FormatStrFormatter("%.1f"))
    ax.xaxis.set_minor_formatter(FormatStrFormatter("%.1f"))

    if inset_axis is False:
        pass
    else:
        if inset_axis is None:
            inset_axis = [0.37, 0.4, 0.55, 0.58]

        axins = ax.inset_axes(inset_axis)
        axins.errorbar(drad_centre,
                       ymeds,
                       xerr=np.diff(dradbins) / 2,
                       yerr=yerr_for_errorbar1,
                       fmt='o',
                       color='C1',
                       ecolor='gray',
                       capsize=3,
                       label='',
                       lw=2)

        axins.errorbar(drad_centre,
                       ymeans,
                       xerr=np.diff(dradbins) / 2,
                       yerr=yerr_for_errorbar,
                       fmt='o',
                       color='C2',
                       ecolor='gray',
                       capsize=3,
                       label='',
                       lw=2)

        axins.plot(bt3a[0, :], bt3a[1, :], marker='o', ls='-', color='C0', lw=2)

        axins.yaxis.set_label_position('right')
        axins.yaxis.tick_right()

        axins.plot(r, 1 / p_g, linewidth=2, linestyle=':', color='green', label='Proton gyroscale')
        axins.plot(r, 1 / p_ii, linewidth=2, linestyle=':', color='blue', label='Proton inertial length')
        axins.plot(r, 1 / l_d, linewidth=2, linestyle=':', color='magenta', label='Cyclotron resonance scale')

        axins.set_yscale('log')
        axins.set_xscale('log')
        axins.set_xlim([0.9 * np.min(drad_centre), 1.1 * np.max(drad_centre)])
        axins.set_ylim([0.9 * np.min(ymeds), 1.15 * np.max(ymeds)])

        axins.patch.set_color((0.9, 0.9, 0.9))
        axins.patch.set_alpha(1.0)

        ax.indicate_inset_zoom(axins, edgecolor="black")

    return bt3a

# FIGURE 8
def histbins_dissipation_index(df_fname='./outputDF2_2020050700_2020061900.pkl.4'):

    df = pd.read_pickle(df_fname)

    # get BREAKSCALE wavenumber in units of [1/km] to match [@BRUNO2014]
    df['breakscale_k_km'] = df.breakscale_k.values * 1000
    df['breakscale_k_ep_km'] = df.breakscale_k_p.values * 1000
    df['breakscale_k_em_km'] = df.breakscale_k_m.values * 1000

    df.mask(df.steep_ind_e.abs() > 1e3, inplace=True)
    df.dropna(inplace=True)

    xpar = df.mean_rad / plasma.AU_KM
    ypar = df.steep_ind.values
    ypar_label = 'Dissipation Range Index'

    x, y = plothistbins(x=xpar,
                        y=ypar,
                        xlabel='R [AU]',
                        ylabel=ypar_label,
                        ax0_yscale='linear',
                        equal_len_div=False,
                        figWidth=9,
                        figHeight=9,
                        legend_loc=2,
                        mean_label='',
                        median_label='')

    return 999


def histbins_inertial_index(df_fname='./outputDF2_2020050700_2020061900.pkl.4'):

    df = pd.read_pickle(df_fname)

    # get BREAKSCALE wavenumber in units of [1/km] to match [@BRUNO2014]
    df['breakscale_k_km'] = df.breakscale_k.values * 1000
    df['breakscale_k_ep_km'] = df.breakscale_k_p.values * 1000
    df['breakscale_k_em_km'] = df.breakscale_k_m.values * 1000

    df.mask(df.inert_ind_e.abs() > 1e2, inplace=True)
    # df.mask(df.steep_ind_e.abs() > 1e3, inplace=True)
    # df.dropna(inplace=True)

    xpar = df.mean_rad / plasma.AU_KM
    ypar = df.inert_ind

    ypar_label = 'Inertial Index'

    x, y = plothistbins(x=xpar,
                        y=ypar,
                        xlabel='R [AU]',
                        ylabel=ypar_label,
                        ax0_yscale='linear',
                        equal_len_div=False,
                        figWidth=9,
                        figHeight=9,
                        legend_loc=1,
                        mean_label='',
                        median_label='')

    return 999

# FIGURE 9
def plot_figure03(data_filename='./outputDF2_2020050700_2020061900.pkl.4',
                  n_radbins=20,
                  figWidth=env.figW,
                  figHeight=env.figH):
    """
    Figure 3 of the paper. Plot the inertial range index and the dissipation range index versus radial distance DRAD.

    - Firstly, the output PKL file from the main function `new_work3()` (this module) is loaded. Change the file name
    to load the correct file. The default corresponds to my path and file. The resulting dataframe `delta_t` contains a
    number of variables, including the break frequency inertial index `inert_ind`, dissipation range index (`steep_ind`)
    and mean radial distance `mean_rad`.
    - Set interval and range errors to NAN (we don't want to plot these)
    - Use the nifty `binbyrad` function to plot the inertial and dissipation range indices versus DRAD. This will
    generate two figures. They are not saved to file - DIY.
    - A summary plot is made with the median of the inertial and dissipation index values for every DRAD bin. Also
    not saved to file.


    Args:
        data_filename: [str] File to be loaded.

    Returns:

    """

    # load data
    df = pd.read_pickle(data_filename)

    # set range an interval errors to NAN
    df.mask(df.breakfreq_er > 0, inplace=True)
    df.mask(df.breakfreq_ei > 0, inplace=True)
    df.mask(df.breakfreq_eb > 0, inplace=True)

    df.dropna(inplace=True)

    # use PLOTHISTBINS to divide the breakscale along mean_rad divisions
    xdat = df.mean_rad.interpolate(limit=5, limit_area='inside').values/plasma.AU_KM
    ydat = df.inert_ind.values
    y_err = df.inert_ind_e.values

    xsumm, ysumm = plothistbins(y=ydat,
                                x=xdat,
                                y_error_neg=y_err,
                                y_error_pos=y_err,
                                n_intervals=n_radbins,
                                equal_len_div=False)

    inert_ix_means = ysumm['mean'][:]
    inert_ix_medians = ysumm['median'][:]
    dradbins = [i for i in xsumm['xmin']]
    dradbins.append(xsumm['xmax'][-1])
    inert_ix_pm = np.array([ysumm['pos_err'], ysumm['neg_err']])

    ydat = df.steep_ind.values
    y_err = df.steep_ind_e.values

    xsumm, ysumm = plothistbins(y=ydat,
                                x=xdat,
                                y_error_neg=y_err,
                                y_error_pos=y_err,
                                n_intervals=n_radbins,
                                equal_len_div=False)

    diss_ix_means = ysumm['mean'][:]
    diss_ix_medians = ysumm['median'][:]
    dradbins = [i for i in xsumm['xmin']]
    dradbins.append(xsumm['xmax'][-1])
    diss_ix_pm = np.array([ysumm['pos_err'], ysumm['neg_err']])

    # find the centre of the bins
    bin_centres = (dradbins[:-1] + np.diff(dradbins) / 2)

    fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(figWidth, figHeight))

    axs[0].errorbar(bin_centres, inert_ix_means,
                    yerr=inert_ix_pm,
                    fmt='o',
                    color='C0',
                    ecolor='gray',
                    capsize=3,
                    label='',
                    lw=2)

    axs[1].errorbar(bin_centres, diss_ix_means,
                    yerr=diss_ix_pm,
                    fmt='o',
                    color='C0',
                    ecolor='gray',
                    capsize=3,
                    label='',
                    lw=2)

    axs[1].set_xlabel('Radial distance [AU]')
    axs[0].set_ylabel('Inertial range index')
    axs[1].set_ylabel('Dissipation range index')

    axs[0].axhline(-5 / 3, color='k', ls='--', label='Kolmogorov')
    axs[0].axhline(-3 / 2, color='r', ls='--', label='Iroshnikov-Kraichnan')

    # put legend and grid
    axs[0].legend()
    # axs[0].grid()
    # axs[1].legend()
    # axs[1].grid()

    plt.subplots_adjust(hspace=0.01)
    plt.tight_layout()

    return dradbins, inert_ix_means, diss_ix_means


def plot_figure03a(data_filename='./outputDF2_2020050700_2020061900.pkl.4',
                   n_radbins=20,
                   figWidth=env.figW,
                   figHeight=env.figH):
    """
    THIS IS SAME AS PLOT_FIGURE03 BUT NOW WITH TURBULENCE AGE ON THE X-AXIS

    Figure 3 of the paper. Plot the inertial range index and the dissipation range index versus radial distance DRAD.

    - Firstly, the output PKL file from the main function `new_work3()` (this module) is loaded. Change the file name
    to load the correct file. The default corresponds to my path and file. The resulting dataframe `delta_t` contains a
    number of variables, including the break frequency inertial index `inert_ind`, dissipation range index (`steep_ind`)
    and mean radial distance `mean_rad`.
    - Set interval and range errors to NAN (we don't want to plot these)
    - Use the nifty `binbyrad` function to plot the inertial and dissipation range indices versus DRAD. This will
    generate two figures. They are not saved to file - DIY.
    - A summary plot is made with the median of the inertial and dissipation index values for every DRAD bin. Also
    not saved to file.


    Args:
        data_filename: [str] File to be loaded.

    Returns:

    """

    # load data
    df = pd.read_pickle(data_filename)

    # set range an interval errors to NAN
    df.mask(df.breakfreq_er > 0, inplace=True)
    df.mask(df.breakfreq_ei > 0, inplace=True)
    df.mask(df.breakfreq_eb > 0, inplace=True)

    df.dropna(inplace=True)

    # use PLOTHISTBINS to divide the breakscale along mean_rad divisions
    df['mean_rad_int'] = df.mean_rad.interpolate(limit=5, limit_area='inside').values

    # define XDAT as the AGE
    xdat = (df.mean_rad_int*1000 / df.vsw_mean)/(24*60*60)

    # define YDAT as INERTIAL
    ydat = df.inert_ind.values
    y_err = df.inert_ind_e.values

    xsumm, ysumm = plothistbins(y=ydat,
                                x=xdat,
                                y_error_neg=y_err,
                                y_error_pos=y_err,
                                n_intervals=n_radbins,
                                equal_len_div=False)

    inert_ix_means = ysumm['mean'][:]
    inert_ix_medians = ysumm['median'][:]
    dradbins = [i for i in xsumm['xmin']]
    dradbins.append(xsumm['xmax'][-1])
    inert_ix_pm = np.array([ysumm['pos_err'], ysumm['neg_err']])

    ydat = df.steep_ind.values
    y_err = df.steep_ind_e.values

    xsumm, ysumm = plothistbins(y=ydat,
                                x=xdat,
                                y_error_neg=y_err,
                                y_error_pos=y_err,
                                n_intervals=n_radbins,
                                equal_len_div=False)

    diss_ix_means = ysumm['mean'][:]
    diss_ix_medians = ysumm['median'][:]
    dradbins = [i for i in xsumm['xmin']]
    dradbins.append(xsumm['xmax'][-1])
    diss_ix_pm = np.array([ysumm['pos_err'], ysumm['neg_err']])

    # find the centre of the bins
    bin_centres = (dradbins[:-1] + np.diff(dradbins) / 2)

    fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(figWidth, figHeight))

    axs[0].errorbar(bin_centres, inert_ix_means,
                    yerr=inert_ix_pm,
                    fmt='o',
                    color='C0',
                    ecolor='gray',
                    capsize=3,
                    label='',
                    lw=2)

    axs[1].errorbar(bin_centres, diss_ix_means,
                    yerr=diss_ix_pm,
                    fmt='o',
                    color='C0',
                    ecolor='gray',
                    capsize=3,
                    label='',
                    lw=2)

    axs[1].set_xlabel('Turbulence Age [day]')
    axs[0].set_ylabel('Inertial range index')
    axs[1].set_ylabel('Dissipation range index')

    axs[0].axhline(-5 / 3, color='k', ls='--', label='Kolmogorov')
    axs[0].axhline(-3 / 2, color='r', ls='--', label='Iroshnikov-Kraich nan')

    # put legend and grid
    axs[0].legend()
    # axs[0].grid()
    # axs[1].legend()
    # axs[1].grid()

    plt.subplots_adjust(hspace=0.01)
    plt.tight_layout()

    return dradbins, inert_ix_means, diss_ix_means


# plot models versus observed KD scatterplot

def kd_models_scatter(output_pkl_file='./outputDF2_2020050700_2020061900.pkl.4'):
    """
    KD versus models scatter plot

    On the X axis: cyclotron resonance scale, proton gyroscale, ion inertial length
    On the Y axis: k_d the breakscale wavelength [rad/km]

    Returns:

    """

    import plasma_tools as pt

    # read data
    df = pd.read_pickle(output_pkl_file)

    # mask errors in break freq
    df.mask(df.breakfreq_ei > 0, inplace=True)
    df.mask(df.breakfreq_er > 0, inplace=True)
    df.mask(df.breakfreq_eb > 0, inplace=True)
    df.dropna(inplace=True)

    # interpolate mean_rad; it's safe to do so because the SC follows predictable path
    # drad = df.mean_rad.interpolate(limit=100, limit_area='inside') / plasma.AU_KM
    df['drad'] = df.mean_rad.interpolate(limit=100, limit_area='inside') / plasma.AU_KM

    N = df.shape[0]
    print('N = %d' % N)

    # # Get the kd wavelength
    # r, B_mag_rad, dens_rad, \
    # temp_rad, Omega_p, w_p, Valf, \
    # Vth_proton_rad, l_d, p_g, p_ii = calc_parker_models(N=N,
    #                                                     vsw_rad_km=df.vsw_mean.mean() / 1000,
    #                                                     rad_min=drad.min(),
    #                                                     rad_max=drad.max())

    # kd = 1/l_d

    # kd = pt.cycl_res_wavelen(B=B_mag_rad_T, mass=pt.MP_KG, dens=dens_rad_m, temp=temp_rad)

    B = df.modBinst
    Np = df.meanDensity
    T = df.meanT

    kd_m = pt.cycl_res_wavelen(B=B, mass=pt.MP_KG, dens=Np, temp=T)

    pg_m = pt.gyroscale(temp=T, mass=pt.MP_KG, B=B)
    pg_wl_m = 1/pg_m

    # proton inertial lentgh [m]
    pi_m = pt.inertialscale(B=B, mass=pt.MP_KG, dens=Np)
    pi_wl_m = 1/pi_m

    # make errorbar plots
    ploterrorbar(df.breakscale_k.values,
                 xp=df.breakscale_k_p,
                 xm=df.breakscale_k_m,
                 ix=df.drad.values,
                 errlimit=5,
                 label='Breakscale wavenumber $\mathrm{k_d}$',
                 datcol='C0')

    # plt.plot(df.drad.values, kd_m, zorder=99,color='C2',marker='s')
    plt.plot(df.drad.values, 1/pg_m, zorder=99,color='C3',marker='s')
    # plt.plot(df.drad.values, pi_m, zorder=99,color='C4',marker='s')

    breakscale_wn_km = df.breakscale_k.values*1000
    kd_km = kd_m*1000
    pg_wl_km = pg_wl_m*1000
    pi_wl_km = pi_wl_m*1000

    # make scatter plots
    kleur = 'C1'
    x = kd_km
    xlabel_str = '$\\mathrm{k_d}$ [rad/km]'
    y = breakscale_wn_km
    ylabel_str = '$\\mathrm{k_d}$ [km]'
    plt.figure()
    plt.scatter(x, y, marker='o', alpha=0.2, color=kleur, label='R=%.3f' % np.corrcoef(x, y)[0,1])
    plt.legend()
    plt.plot([np.nanmin(x), np.nanmax(x)], [np.nanmin(x), np.nanmax(x)], marker='o', ls='--', color='k')
    plt.gca().set_aspect('equal', adjustable='box')

    # make scatter plots
    kleur = 'C2'
    x = pg_wl_km
    y = breakscale_wn_km
    plt.figure()
    plt.scatter(x, y, marker='o', alpha=0.2, color=kleur, label='R=%.3f' % np.corrcoef(x, y)[0,1])
    plt.legend()
    plt.plot([np.nanmin(x), np.nanmax(x)], [np.nanmin(x), np.nanmax(x)], marker='o', ls='--', color='k')
    plt.gca().set_aspect('equal', adjustable='box')


    # make scatter plots
    kleur = 'C3'
    x = pi_wl_km
    y = breakscale_wn_km
    plt.figure()
    plt.scatter(x, y, marker='o', alpha=0.2, color=kleur, label='R=%.3f' % np.corrcoef(x, y)[0,1])
    plt.legend()
    plt.plot([np.nanmin(x), np.nanmax(x)], [np.nanmin(x), np.nanmax(x)], marker='o', ls='--', color='k')
    plt.gca().set_aspect('equal', adjustable='box')


    return df



########################
def div_subsets(x, n_intervals, equal_len_div=True):
    """
    Function to separate input argument *x* in to a number of subsets totalling *n_intervals*. The division is done
    according to method indicated by *equidistant_division*.

    Args:
        x: [ndarray] Array to be divided.
        n_intervals: [int] Number of intervals.
        equal_len_div: [boolean] If *True* then return equal-length subsets; else the subsets are formed by
        dividing in to sets of equal distance in *x*

    Returns:
        subsets: [list] List of arrays, each array containing the indices for the specific subset

    """

    # initialise subsets to be returned
    subsets = []

    if equal_len_div:
        # sort the input array
        x_indices = np.argsort(x)

        # divide in to subsets of equal length
        x_intervals = np.linspace(0, len(x_indices) - 1, n_intervals + 1, dtype=int)

        # for each interval get the appropriate indices, append to *subsets*
        for i in range(len(x_intervals) - 1):
            subsets.append(x_indices[x_intervals[i]:x_intervals[i + 1]])
    else:
        # divide equally within the range of *x*
        x_intervals = np.linspace(np.nanmin(x), np.nanmax(x), n_intervals + 1)

        # for each interval append the appropriate indices
        for i in range(len(x_intervals) - 1):
            subsets.append(np.squeeze(np.where((x >= x_intervals[i]) & (x < x_intervals[i + 1]))))

    return subsets


def plothistbins(x, y, n_intervals=5, n_hist_bins='auto', x_plot=None, equal_len_div=True,
                 xlabel='x', ylabel='y', fig_suptitle='', ax0_xscale='linear', ax0_yscale='linear',
                 y_error_neg=None, y_error_pos=None, figWidth=env.figW, figHeight=env.figH,
                 legend_loc=1, mean_label=None, median_label=None):
    """
    Make a fancy plot showing the division of a data set along with the histogram for each subset.

    Args:
        x: [ndarray] The array containing data to sub-divide the variable *Y* by
        y: [ndarray] The data array (Y-axis of the scatterplot)
        n_intervals: [int] Number of intervals (default 5)
        n_hist_bins: [int] Number of bins in the histogram. If no argument supplied then use 'auto' from numpy.histogram.
         (default value: 'auto')
        x_plot: [ndarray] Parameter to use on the X-axis of the plots. If no argument given, then *x_plot* is set
        to *x*. (default value: None)
        equal_len_div: [boolean] If true then the subset division is done to have equal length subsets. This argument
        is forwarded directly to `div_subsets()`. (default value: True)
        xlabel: [str] Label for the x-axes of the scatter plot (axis 0)
         (default value: empty string)
        ylabel: [str] Label for the y-axis of the scatter plot and the x-axes of the histogram plots
         (axes 1 to *n_intervals*). (default value: empty string)
        fig_suptitle: [str] Label for the super title of the plot. (default value: empty string)
        ax0_xscale: [str] Scale of the x-axis to use for the scatter plot (0-th axis) (default value: 'linear')
        ax0_yscale: [str] Scale of the y-axis to use for the scatter plot (0-th axis) (default value: 'linear')

    Returns:
        y_summary: [dict] Dictionary of summary statistics for each subset / histogram.

    """

    from matplotlib import gridspec

    # initiliase the figure
    fig = plt.figure(figsize=(figWidth, figHeight))
    axs = []

    # intialise the axes using gridspec
    k = 2
    # number of gridspec axis elements to generate
    Nax = k + 2 + 2 * n_intervals
    # generate Nax axes
    gs = gridspec.GridSpec(k + 2 + 2 * n_intervals, 1)
    # append the first axis to the list *axs*
    axs.append(fig.add_subplot(gs[0:k + 1, 0], xscale=ax0_xscale, yscale=ax0_yscale))

    # set axis titles
    axs[0].set_xlabel(xlabel)
    axs[0].set_ylabel(ylabel)
    axs[0].grid(which='major')

    # set j to after the first axis (the scatterplot axis)
    j = k + 2

    # initialise the summary dictionary
    y_summary = {k: [] for k in ['mean', 'median', 'std', 'ix_sel', 'xmin', 'xmax', 'pos_err', 'neg_err']}
    x_summary = {k: [] for k in ['mean', 'median', 'std', 'ix_sel', 'xmin', 'xmax']}

    # list of colours to use ## TODO don't hardcode
    collist = ['C0', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6']

    if x_plot is None:
        x_plot = x

    # build the figure title
    suptitle_drad = ' %s = [%.2f, %.2f]' % (xlabel, np.nanmin(x_plot), np.nanmax(x_plot))
    fig.suptitle(fig_suptitle + suptitle_drad)

    # get the subsets
    ssx = div_subsets(x=x, n_intervals=n_intervals, equal_len_div=equal_len_div)

    # for each subset interval, get the histogram and make the plot
    for i in range(n_intervals):

        if i == 0:
            # make the first axis
            axs.append(fig.add_subplot(gs[j:j + k, 0]))
            j = j + k
        else:
            # make subsequent axes, sharing the x-axis with the first histogram
            axs.append(fig.add_subplot(gs[j:j + k, 0], sharex=axs[i]))
            j = j + k

        # get the selected data
        x_sel = x_plot[ssx[i]]
        y_sel = y[ssx[i]]

        if y_error_pos is not None:
            y_error_pos_sel = np.nanmean(y_error_pos[ssx[i]])
            y_error_neg_sel = np.nanmean(y_error_neg[ssx[i]])

        # number of points in this subset
        N = len(x_sel)

        # get stats of each sub-set of data
        y_summary['mean'].append(np.nanmean(y_sel))
        y_summary['median'].append(np.nanmedian(y_sel))
        y_summary['std'].append(np.std(y_sel[~np.isnan(y_sel)]))
        y_summary['xmin'].append(np.nanmin(x_sel))
        y_summary['xmax'].append(np.nanmax(x_sel))

        if y_error_pos is not None:
            y_error_pos_sel = np.nanmean(y_error_pos[ssx[i]])
            y_error_neg_sel = np.nanmean(y_error_neg[ssx[i]])
            y_summary['pos_err'].append(y_error_pos_sel)
            y_summary['neg_err'].append(y_error_neg_sel)

        # get stats of each sub-set of data
        x_summary['mean'].append(np.nanmean(x_sel))
        x_summary['median'].append(np.nanmedian(x_sel))
        x_summary['std'].append(np.std(x_sel[~np.isnan(x_sel)]))
        x_summary['xmin'].append(np.nanmin(x_sel))
        x_summary['xmax'].append(np.nanmax(x_sel))

        # calculate the histogram, returning bins and edges
        hist, bin_edges = np.histogram(y_sel[~np.isnan(y_sel)], bins=n_hist_bins)

        # get the colour from *collist*
        curr_col = collist[np.mod(i, len(collist))]
        # plot the x vs y scatter on top axis
        axs[0].plot(x_sel, y_sel, marker='o', ls='', color=curr_col, alpha=0.5)

        # plot the histogram on the i-th axis
        axs[i + 1].step(bin_edges, np.append(hist, hist[-1]),
                        where='mid',
                        color=curr_col,
                        label='N=%d\n%s = (%.2f, %.2f)' % (N, xlabel, np.nanmin(x_sel), np.nanmax(x_sel)))
        axs[i + 1].grid(which='major')

        # plot the mean and median
        if mean_label is None:
            mean_label = 'avg = %.2e' % y_summary['mean'][-1]

        if median_label is None:
            median_label = 'med = %.2e' % y_summary['median'][-1]
        axs[i + 1].axvline(y_summary['mean'][-1], ls='--', color=curr_col, label=mean_label)
        axs[i + 1].axvline(y_summary['median'][-1], ls=':', color=curr_col, label=median_label)

        axs[i + 1].legend(loc=legend_loc)
        # add legend

    # put the common histogram y-axis label # TODO deal with hardcoding of values
    fig.text(0.04, 0.40, 'Count [-]', va='center', rotation='vertical', fontsize=12)
    # add x-axis label to the last histogram
    axs[-1].set_xlabel(ylabel)

    # adjust subplot spacing # TODO deal with hardcoding of values
    plt.subplots_adjust(top=0.95,
                        bottom=0.07,
                        left=0.1,
                        right=0.97,
                        hspace=0.0,
                        wspace=0.2)

    return x_summary, y_summary


def calc_parker_models(N,
                       vsw_rad_km,
                       rad_min,
                       rad_max,
                       B_earth_const=plasma.B_const_earth,
                       sol_rot=plasma.SOLAR_ROTATION_DAYS,
                       dens_earth_const=plasma.N_const_earth,
                       temp_earth_const=plasma.T_const_earth):
    """

    Args:
        N:
        vsw_rad_km:
        rad_min:
        rad_max:
        B_earth_const:
        sol_rot:
        dens_earth_const:
        temp_earth_const:

    Returns:

    """
    import plasma_tools as pt

    print('------------------- XXXXXXXXXXXXXXXXXXX in calc_parker_models --------------------- ')

    # length of VSW array
    # N = len(vsw_rad_km)

    # create radial distance array for VSW
    d_rad = np.linspace(rad_min, rad_max, num=N)

    # convert VSW from km/s to AU / hr
    # vsw_au_hr = vsw_rad_km / pt.AU_KM * (60 * 60)
    ### TODO correctly calc VSW in AU/hr
    vsw_au_hr = 0.001 * vsw_rad_km / pt.AU_KM * (60 * 60)

    # solar rotation rate in /AU
    sol_omega = 2 * np.pi / (sol_rot * 24)

    # Only valid for the equatorial plane # TODO what is sunbeta?
    sun_beta = sol_omega / vsw_au_hr

    # reference value B_0 in nT
    B_0 = B_earth_const / np.sqrt(1 + sun_beta ** 2)
    # B_mag = B_0 / r ** 2 * np.sqrt(1. + r * SunBeta * r * SunBeta)

    # Parker HMF with source surface at 0 AU, convert to Tesla
    # B_mag_rad = B_0 * np.sqrt(1 + (d_rad * sun_beta ** 2)) * d_rad ** (-2)
    B_mag_rad = B_0 * np.sqrt(1 + (d_rad * sun_beta) ** 2) * d_rad ** (-2)
    B_mag_rad_T = B_mag_rad * 1e-9

    # 1/r^2 density decrease, convert from #/cm^3 to #/m^3
    dens_rad = dens_earth_const / d_rad ** 2
    dens_rad_m = dens_rad * 1e6

    # Adiabatic decrease for proton temperature
    temp_rad = temp_earth_const * d_rad ** (-4 / 3)

    # radial proton thermal speed [m/s]
    Vth_proton_rad = pt.thermal_speed(temp=temp_rad, mass=pt.MP_KG)

    # proton gyro frequency [rad/s]
    Omega_p = pt.cyclotron_freq(B=B_mag_rad_T, mass=pt.MP_KG)

    # proton plasma frequency [rad/s]
    w_p = pt.plasma_freq(dens=dens_rad_m, mass=pt.MP_KG)

    # Alven speed [m/s]
    Valf = pt.alfven_speed(B=B_mag_rad_T, mass=pt.MP_KG, dens=dens_rad_m)

    # proton cyclotron resonance wavelength [m]
    print('temp_const = %e' % temp_earth_const)
    kd = pt.cycl_res_wavelen(B=B_mag_rad_T, mass=pt.MP_KG, dens=dens_rad_m, temp=temp_rad)

    # cyclotron resonance scale [1/m]
    l_d = 1 / kd

    # proton gyroscale [m]
    p_g = pt.gyroscale(temp=temp_rad, mass=pt.MP_KG, B=B_mag_rad_T)

    # proton inertial lentgh [m]
    p_ii = pt.inertialscale(B=B_mag_rad_T, mass=pt.MP_KG, dens=dens_rad_m)

    Valf = Valf / 1000
    Vth_proton_rad = Vth_proton_rad / 1000

    l_d = l_d / 1000
    p_g = p_g / 1000
    p_ii = p_ii / 1000

    return d_rad, B_mag_rad, dens_rad, temp_rad, Omega_p, w_p, Valf, Vth_proton_rad, l_d, p_g, p_ii
    # return d_rad, B_mag_rad, dens_rad, temp_rad, Omega_p, w_p, Valf, Vth_proton_rad, l_d, p_g, vsw_au_hr


def changesign(df, start_date='20200607', end_date='20200619', date_fmt_str='%Y%m%d'):
    """
    Change sign of dataframe time index to flip around the perihelion.

    Args:
        df: [pandas dataframe] Input data frame
        date_fmt_str: [str] Start date after perihelion in `date_fmt_str` format
        end_date: [str] End date after perihelion in `date_fmt_str` format
        start_date: [str] Date format

    Returns:
        df: [pandas dataframe] Output data frame
    """

    start = df.index.searchsorted(pd.datetime.strptime(start_date, date_fmt_str))

    end = df.index.searchsorted(pd.datetime.strptime(end_date, date_fmt_str))

    df.mean_rad.iloc[start:end] *= -1

    return df


def calc_spiral_angle(b_t, b_r):
    """
    Calculate spiral angle

    Args:
        b_t:
        b_r:

    Returns:

    """

    s = np.arctan(-b_t / b_r)

    return s



def ploterrorbar(x, xp, xm, ix=None, errlimit=None, pt_fmt='o', datcol='C1', errcol='gray', ax=None, pickpoints=False,
                 label=None):
    """
    Helper to utilise the `matplotlib.pyplot.errorbar()` function.


    Args:
        x: [] Data to be plotted
        xp: [] The positive ('plus') error
        xm: [] Negative ('minus') error
        ix: [] The index (x-axis) to plot against
        errlimit: [] Only plot errorbars if they are smaller than `errlimit` times the value of the data point
        pt_fmt: [str] Format string for the data points
        datcol: [str] Format string for the data plot colour
        errcol: [str] Format string for the errorbar colour
        ax: [matplotlib.axis] Axis to plot on. Default is `None` - new figure opened

    Returns:
        ax.figure the Figure handle
    """

    # remove data points with too high error
    if errlimit is not None:
        xp[np.abs(xp - x) / errlimit > x] = np.nan
        x[np.abs(xp - x) / errlimit > x] = np.nan

        xm[np.abs(xm - x) / errlimit > x] = np.nan
        x[np.abs(xm - x) / errlimit > x] = np.nan

    # create generic x-axis index to plot against
    if ix is None:
        ix = np.arange(len(x))

    # create new figure window and get the axis handle
    if ax is None:
        fig = plt.figure()
        ax = fig.gca()

    # use MATPLOTLIB ERRORBAR function to make the plot
    if pickpoints:
        ax.errorbar(ix, x, yerr=[np.abs(x - xm), np.abs(x - xp)],
                    capsize=2,
                    fmt=pt_fmt,
                    color=datcol,
                    ecolor=errcol,
                    picker=True,
                    pickradius=5,
                    label=label)
    else:
        ax.errorbar(ix, x, yerr=[np.abs(x - xm), np.abs(x - xp)],
                    capsize=2,
                    fmt=pt_fmt,
                    color=datcol,
                    ecolor=errcol,
                    label=label)

    return ax.figure


def test_pickpoints(fig, n=3, ):
    """
    Code brazenly copied from stackoverflow. It facilitates picking a point on a graph with the mouse.


    Args:
        fig: [matplotlib.figure.Figure] Figure handle to use
        n: [int] Number of points to pick

    Returns:

    """

    def onpick(event):
        """
        Picker event

        Args:
            event:
        """

        # continue while number of points not picked yet
        if len(ind_list) < n:
            thisline = event.artist
            xdata = thisline.get_xdata()
            ydata = thisline.get_ydata()
            ind = event.ind[0]
            print(ind)
            ind_list.append(ind)
            # points = tuple(zip(xdata[ind[0]], ydata[ind]))
            points = [xdata[ind], ydata[ind]]

            # plot a black 'X' on the picked point
            plt.plot(xdata[ind], ydata[ind], 'kx', zorder=999)
            plt.draw()
        else:
            print('already at %d points' % n)

    # get axis, put title
    ax = fig.gca()
    ax.set_title('click on %d points' % n)

    # pick event
    ind_list = []
    fig.canvas.mpl_connect('pick_event', onpick)
    plt.show()

    return ind_list



def binbyrad(peri_dat, yvar='breakfreq', drad='mean_rad', n_radbins=5, n_bins=None, fig_suptitle='', hist_xlabel='',
             lax_scale=['linear', 'linear'], rax_scale=['linear', 'linear'], yerr_p_var=None, yerr_m_var=None):
    """
    Make a fancy plot to show distribution of values (YVAR) binned by radial distance DRAD.


    Args:
        peri_dat: [pandas.DataFrame] Dataframe that contains the YVAR to be plotted and the DRAD to bin against.
        yvar: [str] The string name of the variable to be binned
        drad: [str] The string name of the mean radial distance variable
        n_radbins: [int] Number of DRAD bins to be used / plotted (`n_radbins - 1` bins will be plotted)
        n_bins: [int] Number of bins to create in each histogram. Default behaviour `n_bins=None` results in number of
         bins being determined automatically by `numpy.histogram_bin_edges()`
        fig_suptitle: [str] Title of the main figure
        hist_xlabel: [str] X-axis label of the histograms; this is the full name of YVAR
        lax_scale: [list of str] Scale descriptors of left-hand set of axes, for x- and y-axis. E.g.
         `lax_scale=['linear', 'log'] will have linear scale x-axis and log-scale y-axis.
        rax_scale: [list of str] Scale descriptors of right-hand set of axes, for x- and y-axis. E.g.
         `lax_scale=['linear', 'log'] will have linear scale x-axis and log-scale y-axis.

    Returns:
        rad_bins: [numpy.ndarray] The DRAD bin edges
        ymed_arr: [numpy.ndarray] Array of the median value of each histogram

    """

    # plt.style.use('seaborn-muted')
    # plt.style.use('seaborn')
    # plt.style.use('seaborn-pastel')
    # plt.style.use('seaborn-talk')
    # plt.style.use('seaborn-colorblind')
    # plt.style.use('fivethirtyeight')

    # generate the DRAD bins
    rad_bins = np.linspace(peri_dat[drad].min(), peri_dat[drad].max(), n_radbins)

    # initialise histogram and bin edges lists
    hist_list = []
    bin_edges_list = []
    std_list = []

    # initialise YMED_ARR with NANs
    ymed_arr = np.full_like(rad_bins[1:], fill_value=np.nan)
    yerrp = np.full_like(rad_bins[1:], fill_value=np.nan)
    yerrm = np.full_like(rad_bins[1:], fill_value=np.nan)

    # set up the x and y data values
    x0 = peri_dat[drad].values
    y0 = peri_dat[yvar].values

    # set up the histogram bins
    if n_bins is None:
        allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins='auto')
    else:
        allbins = np.linspace(peri_dat[yvar].min(), peri_dat[yvar].max(), n_bins)

    # # some other options for generating bins -- just keeping here as a reminder
    # allbins = np.linspace(peri_dat[yvar].min(), peri_dat[yvar].max(), n_bins)
    # allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins='fd')
    # allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins='auto')
    # allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins='doane')
    # allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins=n_bins)

    # initiliase the figure
    fig, axs = plt.subplots(nrows=n_radbins - 1, ncols=2, sharex='col')

    # build the figure title (adding the DRAD range to user-supplied FIG_SUPTITLE)
    suptitle_drad = ' D = [%.2f, %.2f] AU' % (peri_dat[drad].min() / plasma.AU_KM, peri_dat[drad].max() / plasma.AU_KM)
    fig.suptitle(fig_suptitle + suptitle_drad)

    # add blank axis titles to fix spacing
    axs[0, 0].set_title(' ')
    axs[0, 1].set_title(' ')

    for r in range(len(rad_bins[1:])):
        # convert to AU
        x = peri_dat[drad][(peri_dat[drad] >= rad_bins[r]) & (peri_dat[drad] < rad_bins[r + 1])].values

        # convert from AU to KM
        x = x / plasma.AU_KM

        # get the data to be used on the Y axis from the PERI_DAT dataframe, for this bin
        y = peri_dat[yvar][(peri_dat[drad] >= rad_bins[r]) & (peri_dat[drad] < rad_bins[r + 1])]

        # calculate standard deviation for each bin
        std_list.append(np.std(y))

        # execute the histogram, after dropping missing values
        hist, bin_edges = np.histogram(y.dropna().values, bins=allbins)

        # get the median of the current histogram
        # ymed_arr[r] = np.nanmedian(y.dropna().values)
        # ymed_arr[r] = np.nanmean(y.dropna().values)
        ymed_arr[r] = y.mean()
        # ymed_arr[r] = y.median()
        # ymed_arr[r] = bin_edges[np.argmax(hist)]

        if yerr_p_var is not None:
            yerrp[r] = peri_dat[yerr_p_var][(peri_dat[drad] >= rad_bins[r]) & (peri_dat[drad] < rad_bins[r + 1])].mean()
            yerrm[r] = peri_dat[yerr_m_var][(peri_dat[drad] >= rad_bins[r]) & (peri_dat[drad] < rad_bins[r + 1])].mean()

        # add current histogram to list
        hist_list.append(hist)

        # add bin edges to list (and convert to AU)
        bin_edges_list.append(bin_edges / plasma.AU_KM)

        # plot the histogram
        axs[r, 0].step(bin_edges, np.append(hist, hist[-1]), color='C1', where='mid',
                       label='D = (%.2f, %.2f) AU [%d]' % (
                           rad_bins[r] / plasma.AU_KM, rad_bins[r + 1] / plasma.AU_KM, len(x)))

        # plot the median of the histogram, with appropriate label
        axs[r, 0].axvline(ymed_arr[r], ls='--', color='C2', label='median = %.2e' % ymed_arr[r])

        # set axis scales
        axs[r, 0].set_xscale(lax_scale[0])
        axs[r, 0].set_yscale(lax_scale[1])

        axs[r, 1].set_xscale(rax_scale[0])
        axs[r, 1].set_yscale(rax_scale[1])

        # plot the data versus DRAD
        axs[r, 1].plot(x0 / plasma.AU_KM, y0, marker='.', ls='', ms=4, label='all')
        axs[r, 1].plot(x, y, marker='.', ls='', label='sel')
        axs[r, 1].set_xscale(rax_scale[0])
        axs[r, 1].set_yscale(rax_scale[1])

        # put legends and grids
        axs[r, 0].legend()
        axs[r, 1].legend()
        # axs[r, 0].grid(which='major', axis='both')
        # axs[r, 1].grid(which='major', axis='both')

        axs[r, 1].yaxis.set_label_position("right")
        axs[r, 1].yaxis.tick_right()
        axs[r, 1].set_ylabel(hist_xlabel)
        axs[r, 0].set_ylabel('Count')

    axs[r, 0].set_xlabel(hist_xlabel)
    axs[r, 1].set_xlabel('Radial distance D [AU]')

    plt.tight_layout()

    if yerr_m_var is not None:
        return rad_bins, ymed_arr, [yerrp, yerrm]

    # return rad_bins, ymed_arr, np.array(std_list)
    return rad_bins, ymed_arr


def power_law(x, a, b):
    return a * np.power(x, -b)


def scatter_wavenumbers(df=None,
                        output_pkl_file='./outputDF2_2020050700_2020061900.pkl.4',
                        N_rad_intervals=None,
                        option=1):
    """
    KD binned by DRAD versus models scatter plot

    On the X axis: cyclotron resonance scale, proton gyroscale, ion inertial length
    On the Y axis: k_d the breakscale wavelength [rad/km]


    Returns:

    """

    import plasma_tools as pt

    # read data
    if df is None:
        df = pd.read_pickle(output_pkl_file)

    # mask errors in break freq
    df.mask(df.breakfreq_ei > 0, inplace=True)
    df.mask(df.breakfreq_er > 0, inplace=True)
    df.mask(df.breakfreq_eb > 0, inplace=True)
    df.dropna(inplace=True)

    print('N = {}'.format(df.shape[0], '%d'))
    N = df.shape[0]

    if N_rad_intervals is None:
        N_rad_intervals = N

    # interpolate mean_rad; it's safe to do so because the SC follows predictable path
    # drad = df.mean_rad.interpolate(limit=100, limit_area='inside') / plasma.AU_KM
    df['drad'] = df.mean_rad.interpolate(limit=100, limit_area='inside') / plasma.AU_KM

    # divide in to subsets
    if N_rad_intervals == N:
        print('asdf')
        ssx = np.arange(0,N)
    else:
        ssx = div_subsets(x=df.drad.values, n_intervals=N_rad_intervals, equal_len_div=False)

    print('%f %f %f' % (df.drad.min(), df.drad.max(), (df.drad.max()-df.drad.min())/N_rad_intervals))

    drad_binned = np.zeros(N_rad_intervals)
    kd_binned = np.zeros_like(drad_binned)
    kd_res_binned = np.zeros_like(drad_binned)
    kd_gyro_binned = np.zeros_like(drad_binned)
    kd_ii_binned = np.zeros_like(drad_binned)
    Npoints = np.zeros((N_rad_intervals,3))

    B = df.modBinst
    Np = df.meanDensity
    T = df.meanT

    kd_breakscale_radkm = df.breakscale_k.values*1000
    kd_resonance = 1000*pt.cycl_res_wavelen(B=B, mass=pt.MP_KG, dens=Np, temp=T)
    kd_gyroscale = 1000*np.pi/pt.gyroscale(T, pt.MP_KG, B)
    kd_ii = 1000*2*np.pi/pt.inertialscale(B=B, mass=pt.MP_KG, dens=Np)

    j = 0
    for isel in ssx:

        isel = np.atleast_1d(isel)

        if len(isel) >= 1:
            drad_binned[j] = np.nanmean(df.drad.values[isel])
            kd_binned[j] = np.nanmean(kd_breakscale_radkm[isel])
            kd_res_binned[j] = np.nanmean(kd_resonance[isel])
            kd_gyro_binned[j] = np.nanmean(kd_gyroscale[isel])
            kd_ii_binned[j] = np.nanmean(kd_ii[isel])

            Npoints[j, 0] = np.nanmin(df.drad.values[isel])
            Npoints[j, 1] = np.nanmax(df.drad.values[isel])
            Npoints[j, 2] = len(isel)
            # print('%.4f, %.4f, %.4f' % (Npoints[j,0],Npoints[j,1],Npoints[j,1]-Npoints[j,0]))

        j += 1

    xlim = [1e-3, 1.2]

    if option == 1:

        fig1, axs = plt.subplots(nrows=1,
                                 ncols=3,
                                 sharex=True,
                                 sharey=True,
                                 figsize=(15, 5))

        # plt.figure()
        axs[0].scatter(kd_res_binned, kd_binned, marker='o', edgecolor='C0', facecolor='none')
        # plot 45deg line
        axs[0].plot(xlim, xlim, ls='--', color='gray')
        axs[0].set_xscale('log')
        axs[0].set_yscale('log')
        axs[0].set_xlabel('Cyclotron resonance [rad/km]')
        axs[0].set_ylabel('$\\mathrm{k_d}$ [rad/km]')
        axs[0].grid()

        # plt.figure()
        axs[1].scatter(kd_gyro_binned, kd_binned, marker='s', edgecolor='C1', facecolor='none')
        # plot 45deg line
        axs[1].plot(xlim, xlim, ls='--', color='gray')
        axs[1].set_xscale('log')
        axs[1].set_yscale('log')
        axs[1].set_xlabel('Proton gyroscale [rad/km]')
        axs[1].set_ylabel('$\\mathrm{k_d}$ [rad/km]')
        axs[1].grid()

        axs[2].scatter(kd_ii_binned, kd_binned, marker='<', edgecolor='C2', facecolor='none')
        # plot 45deg line
        # x = np.linspace(*ax.get_xlim())
        axs[2].plot(xlim, xlim, ls='--', color='gray')
        axs[2].set_xscale('log')
        axs[2].set_yscale('log')
        axs[2].set_xlabel('Ion inertial length [rad/km]')
        axs[2].set_ylabel('$\\mathrm{k_d}$ [rad/km]')
        axs[2].grid()

        plt.tight_layout()

    elif option == 2:

        fig1, ax = plt.subplots(nrows=1,
                                 ncols=1,
                                 sharex=True,
                                 sharey=True,
                                 figsize=(8, 8))

        # ax = axs[0]
        ax.scatter(kd_res_binned, kd_binned,
                   marker='o', edgecolor='C0', facecolor='none',
                   label='Cyclotron resonance ($\\mathrm{R^2=%.2f}$)' % np.corrcoef(kd_res_binned, kd_binned)[0, 1]**2)
        ax.scatter(kd_gyro_binned, kd_binned,
                   marker='s', edgecolor='C1', facecolor='none',
                   label='Proton gyroscale ($\\mathrm{R^2=%.2f}$)' % np.corrcoef(kd_gyro_binned, kd_binned)[0, 1]**2)
        ax.scatter(kd_ii_binned, kd_binned,
                   marker='<', edgecolor='C2', facecolor='none',
                   label='Ion inertial length ($\\mathrm{R^2=%.2f}$)' % np.corrcoef(kd_ii_binned, kd_binned)[0, 1]**2)

        plt.legend(scatterpoints=2)

        ax.plot(xlim, xlim, ls='--', color='gray')
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_xlabel('Model wavenumber [rad/km]')
        ax.set_ylabel('$\\mathrm{k_d}$ [rad/km]')
        ax.grid()
        ax.set_aspect('equal', adjustable='box')

        plt.tight_layout()

    plt.figure()
    # plt.plot(Npoints[:,0], Npoints[:,2])
    plt.scatter(Npoints[:,1]-Npoints[:,0], Npoints[:,2], marker='o')


    return kd_binned, ssx