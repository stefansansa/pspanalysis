"""
For testing new calculation and algorithms.

pspbreaks.py and env.py are both utilised / imported



"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import pspbreaks as ppp
import psp_tools as ptools
import env
import plasma_tools as plasma

from parker_heliosphere import importComp

# from anel_parker_heliosphere import importComp

# import brewer2mpl

# plt.style.use('seaborn-whitegrid')
# plt.style.use('seaborn-muted')
# plt.style.use('seaborn')
# plt.style.use('seaborn-pastel')
# plt.style.use('seaborn-talk')
# plt.style.use('seaborn-colorblind')
# plt.style.use('fivethirtyeight')


mpl_params = {
    'axes.titlesize': 13,
    'legend.fontsize': 13,
    'axes.labelsize': 13,
    'xtick.labelsize': 13,
    'ytick.labelsize': 13,
    'text.usetex': False,
    'figure.figsize': [15, 8]
}
# plt.rcParams.update(params)
mpl.rcParams.update(mpl_params)
legend_fontsize = 13


# bmap = brewer2mpl.get_map('Set2', 'qualitative', 7)
# colors = bmap.mpl_colors


def plot_spec(varsdict, fig_title='', chunk_number=110):
    """
    Plot
    - spectrum of B_tr with estimated break frequency indicated, and
    - the inertial and dissipation range fits for a single chunk of B-field data

    Args:
        varsdict: [dict] Dictionary with various variables
        fig_title: [str] Figure title
        chunk_number: [int] The chunk interval to plot

    Returns:
        None

    """

    breakfreq = varsdict['breakfreq']
    interpw = varsdict['freq_log']
    psd_lst = varsdict['Psd_log']
    spec_lst = varsdict['spectral_ts']
    breakfreq_r = varsdict['breakfreq_er']
    breakfreq_p = varsdict['breakfreq_ep']
    breakfreq_m = varsdict['breakfreq_em']

    breakfreq[breakfreq_r > 0.5] = np.nan

    traceb_ts = np.zeros((len(psd_lst[0]), len(psd_lst)))
    spectralix_ts = np.zeros((len(spec_lst[0]), len(spec_lst)))

    # reshape psd
    for i in range(len(psd_lst)):
        traceb_ts[:, i] = psd_lst[i]

    for i in range(len(spec_lst)):
        spectralix_ts[:, i] = spec_lst[i]

    # make first figure
    fig1, axs = plt.subplots(nrows=2,
                             ncols=1,
                             sharex=True,
                             figsize=(env.figW, env.figH))

    # first axis, contour plot
    N = np.max(breakfreq.shape)
    ix = np.arange(1, N + 1)
    cs0 = axs[0].contourf(ix, interpw[0], np.log10(traceb_ts))

    axs[0].fill_between(ix[breakfreq_r < 0.5],
                        breakfreq_m[breakfreq_r < 0.5],
                        breakfreq_p[breakfreq_r < 0.5],
                        color='k',
                        alpha=0.4)

    axs[0].semilogy(ix, breakfreq, lw=1, color='k', ls='-')

    axs[0].set_xlabel('Spec num (%d s per spec)' % env.chunktime)
    axs[0].set_ylabel('Freq [Hz]')
    axs[0].set_title('%s\nPower spec $\log_{10}(P)$' % fig_title)
    axs[0].grid(which='major')

    cbar0 = plt.colorbar(cs0, ax=axs[0])

    # second axis, contour plot
    cs1 = axs[1].contourf(ix, interpw[0], spectralix_ts)
    axs[1].fill_between(ix[breakfreq_r < 0.5],
                        breakfreq_m[breakfreq_r < 0.5],
                        breakfreq_p[breakfreq_r < 0.5],
                        color='k',
                        alpha=0.5)

    axs[1].semilogy(ix, breakfreq, lw=1, color='k', ls='-')

    cbar1 = fig1.colorbar(cs1, ax=axs[1])
    axs[1].set_xlabel('Spec num (%d s per spec)' % env.chunktime)
    axs[1].set_ylabel('Freq [Hz]')
    axs[1].set_title('Spec index')
    axs[1].grid(which='major')

    fig1.suptitle(fig_title)
    plt.tight_layout()

    # the other figure
    fig2, axs = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(env.figW, env.figH))

    x = interpw
    y = psd_lst

    # create the best-fit lines for the inertial and dissipation ranges
    inert_fit_y = 10 ** (varsdict['inert_const'][chunk_number] +
                         varsdict['inert_ind'][chunk_number] * np.array([-2, 1]))
    inert_fit_x = [0.01, 10]

    diss_fit_y = 10 ** (varsdict['steep_const'][chunk_number] +
                        varsdict['steep_ind'][chunk_number] * np.array([-1, 1]))
    diss_fit_x = [0.1, 10]

    # plot the PSD versus frequency
    axs[0].loglog(x[chunk_number], y[chunk_number], label='Psd (FFT)')

    # plot break frequency
    axs[0].axvline(breakfreq[chunk_number], ls='--', color='r', label='Break freq')

    # plot the inertial and dissipation range fits
    axs[0].loglog(inert_fit_x, inert_fit_y, label='Inert fit line')
    axs[0].loglog(diss_fit_x, diss_fit_y, label='Diss fit line')

    # indicate the range of dissipation range and inertial range frequencies
    axs[0].axvspan(varsdict['steep_min'][chunk_number], varsdict['steep_max'][chunk_number], color='m', alpha=0.2)
    axs[0].axvspan(varsdict['inert_min'][chunk_number], varsdict['inert_max'][chunk_number], color='m', alpha=0.2)

    axs[0].set_xlabel('Freq [Hz]')
    axs[0].set_ylabel('Power')
    axs[0].legend()
    axs[0].grid()

    # Figure 2, axis 2
    x = interpw
    y = spec_lst
    dissfit = varsdict['steep_ind'][chunk_number]
    inertfit = varsdict['inert_ind'][chunk_number]

    # plot the spectral index versus frequency
    axs[1].semilogx(x[chunk_number], y[chunk_number], label='Spectral index (FFT)')

    # plot the break frequency
    axs[1].axvline(breakfreq[chunk_number], ls='--', color='r', label='Break freq')

    # plot the inertial and dissipation range fits
    axs[1].axhline(inertfit, label='Inertial fit', color='C1')
    axs[1].axhline(dissfit, label='Dissipation fit', color='C2')

    # labels etc
    axs[1].set_xlabel('Freq [Hz]')
    axs[1].set_ylabel('Spectral index')
    axs[1].legend()
    axs[1].grid()
    fig2.suptitle(fig_title)
    plt.tight_layout()
    fig2.subplots_adjust(hspace=0.0)

    return


def new_work2(peri_dat, yvar='breakfreq', n_radbins=10, n_bins=10, fig_suptitle='', hist_xlabel='',
              lax_scale=['linear', 'linear'], rax_scale=['linear', 'linear']):
    # plt.style.use('dark_background')

    rad_bins = np.linspace(peri_dat.mean_rad.min(), peri_dat.mean_rad.max(), n_radbins)

    hist_list = []
    bin_edges_list = []

    x0 = peri_dat.mean_rad.values
    y0 = peri_dat[yvar].values

    # allbins = np.linspace(peri_dat[yvar].min(), peri_dat[yvar].max(), n_bins)
    # allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins='fd')
    allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins='auto')
    # allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins='doane')
    # allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins=n_bins)

    fig, axs = plt.subplots(nrows=n_radbins - 1, ncols=2, sharex='col')
    # fig, axs = plt.subplots(nrows=n_radbins-1, ncols=2, sharex=True)

    fig.suptitle(fig_suptitle)

    axs[0, 0].set_title(' ')
    axs[0, 1].set_title(' ')

    for r in range(len(rad_bins[1:])):
        # convert to AU
        x = peri_dat.mean_rad[
                (peri_dat.mean_rad >= rad_bins[r]) & (peri_dat.mean_rad < rad_bins[r + 1])].values / plasma.AU_KM

        # if yvar == 'breakscale_k':
        #     y = 1/peri_dat[yvar][(peri_dat.mean_rad>=rad_bins[r])&(peri_dat.mean_rad<rad_bins[r+1])]/1000
        # else:
        #     y = peri_dat[yvar][(peri_dat.mean_rad>=rad_bins[r])&(peri_dat.mean_rad<rad_bins[r+1])]

        y = peri_dat[yvar][(peri_dat.mean_rad >= rad_bins[r]) & (peri_dat.mean_rad < rad_bins[r + 1])]

        # bins_vec = np.histogram_bin_edges(y.dropna().values, bins='fd')

        # hist, bin_edges = np.histogram(y.dropna().values, bins=bins_vec)
        hist, bin_edges = np.histogram(y.dropna().values, bins=allbins)
        # hist, bin_edges = np.histogram(y.dropna().values, bins=n_bins)

        hist_list.append(hist)
        bin_edges_list.append(bin_edges / plasma.AU_KM)
        axs[r, 0].step(bin_edges, np.append(hist, hist[-1]), color='C1', where='mid',
                       label='D = (%.2f, %.2f) AU' % (rad_bins[r] / plasma.AU_KM, rad_bins[r + 1] / plasma.AU_KM))

        axs[r, 0].set_xscale(lax_scale[0])
        axs[r, 0].set_yscale(lax_scale[1])

        axs[r, 1].set_xscale(rax_scale[0])
        axs[r, 1].set_yscale(rax_scale[1])

        axs[r, 1].plot(x0 / plasma.AU_KM, y0, marker='.', ms=4, label='all')
        axs[r, 1].plot(x, y, marker='.', ls='', label='sel')
        axs[r, 1].set_xscale(rax_scale[0])
        axs[r, 1].set_yscale(rax_scale[1])

        axs[r, 0].legend()
        axs[r, 1].legend()

        axs[r, 0].grid()
        axs[r, 1].grid()

        axs[r, 1].yaxis.set_label_position("right")
        axs[r, 1].yaxis.tick_right()
        axs[r, 1].set_ylabel(hist_xlabel)
        axs[r, 0].set_ylabel('Count')

    axs[r, 0].set_xlabel(hist_xlabel)
    axs[r, 1].set_xlabel('Radial distance D [AU]')

    plt.tight_layout()

    return 999


def new_work(dtime=env.time_selected, endtime=env.endtime_selected):
    '''
    SC_POS_HCI
    CATDESC: PSP Spacecraft position in the Heliocentric Inertial system, in km
    UNITS: km
    VALIDMIN: -3.00000e+08 -3.00000e+08 -3.00000e+08
    VALIDMAX: 3.00000e+08 3.00000e+08 3.00000e+08
    DIM_SIZES: 3
    VAR_NOTES:
    Also called Ecliptic J2000. Z is the solar north rotational axis, and X is the solar
    ascending node on the J2000 ecliptic.
    '''

    # interpret user-selected time
    dt_user = pd.datetime.strptime(dtime, env.input_time_format)
    enddate = pd.datetime.strptime(endtime, env.input_time_format)

    datelist = pd.date_range(dt_user, enddate, freq=env.mag_time_delta)
    print(datelist)

    # MeanRad, Proton_Gyrofreq, MeanVth, Alfven_Speed, Proton_Inertial_Length, Proton_Gyroscale, MeanModBINST, MeanModBSC, MeanDensity, MeanT
    newMeanRad = np.array([])
    newProton_Gyrofreq = np.array([])
    newMeanVth = np.array([])
    newAlfven_Speed = np.array([])
    newProton_Inertial_Length = np.array([])
    newProton_Gyroscale = np.array([])
    newMeanModBINST = np.array([])
    newMeanModBSC = np.array([])
    newMeanDensity = np.array([])
    newMeanT = np.array([])
    newProton_Kc = np.array([])
    newBreak_Scale_k = np.array([])
    newProton_plasma_frek = np.array([])
    newMeanTperp = np.array([])
    newMeanTpar = np.array([])

    # stefan added 2021/04/29
    newBreak_Freq = np.array([])
    newInert_ix = np.array([])
    # newSpectime = np.array([])
    newSpectime = pd.DatetimeIndex([])

    for i in datelist:

        ppp.printmessage('user selected time: %s' % i)

        # round to nearest timestamp corresponding to file frequency
        dt_user_floor = pd.Timestamp(i).floor(env.mag_time_delta)
        ppp.printmessage('floored user time: %s' % dt_user_floor)

        # initialise empty dataframes for return variables
        spi_df = pd.DataFrame()
        spc_df = pd.DataFrame()
        mag_df = pd.DataFrame()

        # calculate the break freq
        ppp.printmessage('Now going to calc break freq')
        mag_df, brk_frq_vars = ppp.break_freq_download_calc(dt_user_floor)

        # if error value returned, then skip this iteration of the for-loop
        if brk_frq_vars == env.ERRORVAL:
            continue

        # calculate the length scale
        ppp.printmessage('Now going to calc len scale')
        c = ppp.len_scale_download_calc(dt_user_floor, brk_frq_vars=brk_frq_vars)

        # if error value returned, then skip this iteration of the for-loop
        if c == env.ERRORVAL:
            continue

        # put data from the list *c* in to sensible variable names
        MeanRad = c[0]
        Proton_Gyrofreq = c[1]
        MeanVth = c[2]
        Alfven_Speed = c[3]
        Proton_Inertial_Length = c[4]
        Proton_Gyroscale = c[5]
        MeanModBINST = c[6]
        MeanModBSC = c[7]
        MeanDensity = c[8]
        MeanT = c[9]
        Proton_Kc = c[10]
        Break_Scale_k = c[11]
        Proton_plasma_frek = c[12]
        MeanTperp = c[13]
        MeanTpar = c[14]
        time_ix = c[15]
        Break_Freq = c[16]

        newMeanRad = np.append(newMeanRad, MeanRad)
        newProton_Gyrofreq = np.append(newProton_Gyrofreq, Proton_Gyrofreq)
        newMeanVth = np.append(newMeanVth, MeanVth)
        newAlfven_Speed = np.append(newAlfven_Speed, Alfven_Speed)
        newProton_Inertial_Length = np.append(newProton_Inertial_Length, Proton_Inertial_Length)
        newProton_Gyroscale = np.append(newProton_Gyroscale, Proton_Gyroscale)
        newMeanModBINST = np.append(newMeanModBINST, MeanModBINST)
        newMeanModBSC = np.append(newMeanModBSC, MeanModBSC)
        newMeanDensity = np.append(newMeanDensity, MeanDensity)
        newMeanT = np.append(newMeanT, MeanT)
        newProton_Kc = np.append(newProton_Kc, Proton_Kc)
        newBreak_Scale_k = np.append(newBreak_Scale_k, Break_Scale_k)
        newProton_plasma_frek = np.append(newProton_plasma_frek, Proton_plasma_frek)
        newMeanTperp = np.append(newMeanTperp, MeanTperp)
        newMeanTpar = np.append(newMeanTpar, MeanTpar)
        newBreak_Freq = np.append(newBreak_Freq, Break_Freq)
        newSpectime = newSpectime.append(time_ix)

        # newBreak_Freq = np.append(newBreak_Freq, brk_frq_vars['breakfreq'])
        # newInert_ix = np.append(newInert_ix, brk_frq_vars['inert_ix'])
        # # newSpectime = np.append(newSpectime, brk_frq_vars['spectime'].values)
        # newSpectime = newSpectime.append(brk_frq_vars['spectime'])

        # 'spectime': spectime,
        # 'interpw': interpw,
        # 'trace_B_ts': trace_B_ts,
        # 'spectralix_ts': spectralix_ts,
        # 'breakfreq': breakfreq,
        # 'inert_const': inert_const,
        # 'inert_ix': inert_ix,
        # 'steep_const': steep_const,
        # 'steep_ix': steep_ix,
        # 'steep_maxw': steep_maxw,
        # 'steep_minw': steep_minw,
        # 'inert_maxw': inert_maxw,
        # 'inert_minw': inert_minw,
        # 'filename': savefilename

    # MeanModBINST
    # MeanModBSC
    # MeanDensity
    # MeanT
    # Proton\_Gyrofreq
    # w\_p (proton plasma frequency)
    # MeanRad
    # MeanVth
    # Alfven\_Speed
    # Proton\_Inertial\_Length
    # Proton\_Gyroscale
    # Proton\_Kc (cyclotron resonance scale)
    # Break\_Scale\_k
    # Break\_freq
    # inertial range index -- hierdie sal tussen Rob se code wees, weet nie wat sy naampie is nie.
    # T\_TENSOR: CDF\_FLOAT [6180, 6] - van SPI af, ek scheme dit sal ook die Mean moet wees.
    # Break\_Scale\_k

    # vars to save
    output_df = pd.DataFrame(index=newSpectime)
    # output_df = pd.DataFrame([])
    output_df['mean_rad'] = newMeanRad
    output_df['modBinst'] = newMeanModBINST
    output_df['modBSC'] = newMeanModBSC
    output_df['meanDensity'] = newMeanDensity
    output_df['meanT'] = newMeanT
    output_df['g_proton'] = newProton_Gyrofreq
    output_df['w_proton'] = newProton_plasma_frek
    output_df['mean_vth'] = newMeanVth
    output_df['v_alf'] = newAlfven_Speed
    output_df['ilen_proton'] = newProton_Inertial_Length
    output_df['gscale_proton'] = newProton_Gyroscale
    output_df['kc_proton'] = newProton_Kc
    output_df['breakscale_k'] = newBreak_Scale_k
    output_df['breakfreq'] = newBreak_Freq

    # output_df2 = pd.DataFrame(index=newSpectime)
    # output_df2['breakfreq'] = newBreak_Freq
    # output_df2['irange_ix'] = newInert_ix

    plotscales(newMeanRad, newProton_Gyrofreq, newMeanVth, newAlfven_Speed, newProton_Inertial_Length,
               newProton_Gyroscale, newMeanModBINST, newMeanModBSC, newMeanDensity, newMeanT, newProton_Kc,
               newBreak_Scale_k, newProton_plasma_frek, newMeanTperp, newMeanTpar)

    output_filename = '_'.join(('outputDF', env.time_selected, env.endtime_selected)) + '.pkl'
    output_df.to_pickle(output_filename)

    return output_df


def mag_psd_spec(magdf, N0=0, N1=1000):
    # get mag data
    s = magdf.BRTN__m.values[N0:N1]
    diff = 1 / magdf.rate[0]

    Nax = len(s)

    # now get psd
    nfft = 512
    noverlap = 0

    t_seg, new_len = time_seg(Nax, nfft, noverlap)
    N = len(t_seg[:, 0])
    sig_fft = np.zeros((nfft, N), dtype=complex)
    print(sig_fft.shape)

    print(t_seg)

    for i in range(N):
        # signal chunk
        si = s[t_seg[i, 0]:t_seg[i, 1]]

        # detrend chunk
        sidt = detrend(si)

        # apply window
        sidtw = np.hanning(len(sidt)) * sidt

        # get fft of the chunk
        sig_fft[:, i] = np.fft.fft(sidtw, nfft) / np.sqrt(nfft / diff)

    psd = np.sum(np.squeeze(sig_fft * np.conj(sig_fft)), axis=1)

    # psd = np.zeros(nfft)
    # for j in range(nfft):
    #     psd[j] = np.mean(np.abs(sig_fft[j, :]) ** 2)

    k = np.arange(0, nfft)
    frq = k / (nfft * diff)

    plt.figure()

    plt.subplot(211)
    plt.plot(s)

    plt.subplot(212)
    plt.plot(frq[:int(nfft / 2)], 10 * np.log10(psd[:int(nfft / 2)]))

    # plt.subplot(313)
    plt.psd(s, nfft, 1 / diff)

    return s


def example():
    """
    Get magnetic field data PSD for the given timeseries.
    Use morlet wavelet / fft to get the power for each window.


    Returns:

    """

    # set up time steps

    # set up frequency / scale steps

    # test example from
    # https: // www.geeksforgeeks.org / plot - the - power - spectral - density - using - matplotlib - python /
    np.random.seed(19695601)

    diff = 0.01
    ax = np.arange(0, 10, diff)
    n = np.random.randn(len(ax))
    by = np.exp(-ax / 0.05)

    cn = np.convolve(n, by) * diff
    cn = cn[:len(ax)]
    s = 0.1 * np.sin(2 * np.pi * ax) + cn

    Nax = len(ax)

    plt.figure()
    plt.plot(ax, s, label='signal s')
    #
    # # plt.subplot(211)
    # # plt.plot(ax, s)
    # # plt.subplot(212)
    # # plt.psd(s, 512, 1 / diff)
    #
    # plt.show()

    # now get psd
    nfft = 512
    noverlap = 0

    t_seg, new_len = time_seg(Nax, nfft, noverlap)
    N = len(t_seg[:, 0])
    sig_fft = np.zeros((nfft, N))
    print(sig_fft.shape)

    print(t_seg)

    for i in range(N):
        # signal chunk
        si = s[t_seg[i, 0]:t_seg[i, 1]]

        # detrend chunk
        sidt = detrend(si, det_key='none')

        # apply window
        sidtw = np.hanning(len(sidt)) * sidt

        # get fft of the chunk
        sig_fft[:, i] = np.fft.fft(sidtw, nfft)

    # psd = np.sum(np.squeeze(sig_fft * np.conj(sig_fft)), axis=1)

    psd = np.zeros(nfft)
    for j in range(nfft):
        psd[j] = np.mean(np.abs(sig_fft[j, :]) ** 2)

    plt.figure()
    k = np.arange(0, nfft)
    frq = k / (nfft * diff)
    plt.plot(frq[:int(nfft / 2)], 10 * np.log10(diff * psd[:int(nfft / 2)]))

    plt.psd(s, nfft, 1 / diff)

    return s


def detrend(x, det_key='none'):
    import matplotlib as mpl

    y = mpl.mlab.detrend(x, key=det_key)

    return y


def time_seg(signal_length, width, noverlap):
    """
    Helper function for PSD determination.
    This makes overlapping time sequences with appropriate padding from signal
    of length *signal_length*, for window widths *width* and overlap *noverlap*

    Args:
        signal_length: length of original signal
        width: window width
        noverlap: overlap between windows

    Returns:
        M: two-column matrix with start and end index of each overlapping
        window.
        new_len: length of the new signal. Should be same or longer than
        original since some padding may be necessary.

    """

    # make time segments
    i = 0
    z = 0
    t0 = []
    tN = []

    while signal_length > z:
        t0.append(i * width - i * noverlap)
        tN.append((i + 1) * width - i * noverlap)
        z = tN[-1]
        i += 1

    M = np.zeros((len(t0), 2), dtype=int)
    M[:, 0] = t0
    M[:, 1] = tN

    new_len = len(t0)

    return M, new_len


def get_mag_data(dt_user):
    """
    Get mag data only for a certain date, given by input arg
    Args:
        dt_user: the input date

    Returns:

    """

    # use user datetime to build filename to download
    full_url = ppp.psp_urlbuilder(dt_user,
                                  dt_fmt=env.file_datepart_format_mag,
                                  baseurl=env.baseurl_mag,
                                  file_hdr=env.file_header,
                                  file_flags=env.file_flags_mag,
                                  data_version=env.mag_data_version,
                                  file_ext=env.mag_file_ext)
    print(full_url)
    mag_filename = ppp.print_fn_only(full_url, dir_del='/')

    # download the file
    try:
        dlres = ppp.download_data(full_url, outpath=env.data_dir)
        if np.isnan(dlres):
            raise FileNotFoundError('File not found on disk or at URL')

    except FileNotFoundError as err:
        print(err)
        return np.nan

    # use the file to calculate break frequency

    # # first load the CDF file
    mag_cdf = ppp.load_cdf_file(os.path.join(env.data_dir, mag_filename))

    # # convert CDF to pandas DF
    mag_df = ppp.read_mag_data(mag_cdf)

    return mag_df


def cdf_print_vars(cdf_dat):
    i = 0
    for k in range(len(cdf_dat.keys())):
        print('%s:' % str(i).zfill(2), cdf_dat[k].name(), cdf_dat[k].shape)
        i += 1

    return 999


def test_gettindata(instr='spc', dstr='2020060100'):
    # dt_user = pd.datetime.strptime(env.time_selected, env.input_time_format)
    dt_user = pd.datetime.strptime(dstr, env.input_time_format)

    print('user time is ', dt_user)

    if instr == 'mag':
        df = ppp.gettingmagdata(dt_user=dt_user, fixdatarate=True)
    else:
        df = ppp.gettingdata(dt_user, instr=instr)

    return df


def gettingdata(dt_user, instr='spc'):
    import psp_tools as ttt
    import pspbreaks as ppp

    # TODO move the getting-reading data part out
    # use user datetime to build filename to download

    if instr == 'spc':
        full_url = ttt.psp_urlbuilder(dt_user, dt_fmt=env.file_datepart_format_spc, baseurl=env.baseurl_spc,
                                      file_hdr=env.file_header, file_flags=env.file_flags_spc,
                                      data_version=env.spc_data_version, file_ext=env.spc_file_ext)
    elif instr == 'spi':
        full_url = ttt.psp_urlbuilder(dt_user, dt_fmt=env.file_datepart_format_spi, baseurl=env.baseurl_spi,
                                      file_hdr=env.file_header, file_flags=env.file_flags_spi,
                                      data_version=env.spi_data_version, file_ext=env.spi_file_ext)
    else:
        return 999

    # get filename
    cdf_filename = ttt.print_fn_only(full_url, dir_del='/')

    pkl_filename = ttt.replace_filename_extension(cdf_filename, newextension='pkl')

    cdf_filepath = os.path.join(env.data_dir, cdf_filename)
    pkl_filepath = os.path.join(env.data_dir, pkl_filename)

    # if pkl filepath exists, then just read it
    if os.path.exists(pkl_filepath):
        print('PKL file exist')
        out_df = pd.read_pickle(pkl_filepath)
    else:
        print('PKL file DONT exist')
        # download the file
        dlres = ttt.download_data(full_url, outpath=env.data_dir)

        # first load the spc CDF file
        out_cdf = ttt.load_cdf_file(os.path.join(env.data_dir, cdf_filename))

        if out_cdf == env.ERRORVAL:
            print('NO SPC data')
            out_df = env.ERRORVAL
        else:
            # convert SPI CDF to pandas DF
            if instr == 'spc':
                # out_df = ppp.read_spc_data(out_cdf)
                out_df = ppp.read_sp_data(out_cdf, instr=instr)
            elif instr == 'spi':
                out_df = ppp.read_sp_data(out_cdf, instr=instr)

    return out_df


def gettingmagdata(dt_user, fixdatarate=True):
    import psp_tools as ttt
    import pspbreaks as ppp

    # build the URL
    # use user datetime to build filename to download
    full_url = ttt.psp_urlbuilder(dt_user,
                                  dt_fmt=env.file_datepart_format_mag,
                                  baseurl=env.baseurl_mag,
                                  file_hdr=env.file_header,
                                  file_flags=env.file_flags_mag,
                                  data_version=env.mag_data_version,
                                  file_ext=env.mag_file_ext)

    # get the filename without the full path
    cdf_filename = ttt.print_fn_only(full_url, dir_del='/')
    pkl_filename = ttt.replace_filename_extension(cdf_filename, newextension='pkl')
    x_pkl_filename = ttt.replace_filename_extension(cdf_filename, newextension='x.pkl')

    cdf_filepath = os.path.join(env.data_dir, cdf_filename)
    pkl_filepath = os.path.join(env.data_dir, pkl_filename)
    x_pkl_filepath = os.path.join(env.data_dir, x_pkl_filename)

    # TODO should we read fixed datarate data here?

    out_df0 = env.ERRORVAL

    # if pkl filepath exists, then just read it
    if os.path.exists(x_pkl_filepath) & fixdatarate:
        print('X_PKL file exist')
        out_df0 = pd.read_pickle(x_pkl_filepath)
        fixdatarate = False
    elif os.path.exists(pkl_filepath):
        print('PKL file exist')
        out_df0 = pd.read_pickle(pkl_filepath)
    else:
        print('PKL file DONT exist')
        # download the file
        dlres = ttt.download_data(full_url, outpath=env.data_dir)

        # first load the CDF file
        mag_cdf = ttt.load_cdf_file(cdf_filepath)

        if mag_cdf != env.ERRORVAL:
            # convert CDF to pandas DF
            out_df0 = ppp.read_mag_data(mag_cdf)

            # # TODO: SAVEPOINT fn = mag_filename + '.pkl'
            # # file path to save to
            # ttt.printmessage('---> SAVEPOINT <----')
            # mag_save_filename = os.path.join(env.data_dir,
            #                                  ttt.replace_filename_extension(mag_filename, env.pkl_ext))
            # ttt.savepickle(mag_save_filename, varslist=mag_df0)

    if isinstance(out_df0, float):
        out_df0 = pd.DataFrame()

    if fixdatarate & len(out_df0) > 0:
        print('fixdatarate')
        out_df = ppp.fix_data_rate(out_df0,
                                   out_df0[env.mag_data_rate_var_name],
                                   L=env.cycle_dur,
                                   floor_index=False,
                                   interpmethod=env.interpolation_method,
                                   dataratevarname=env.mag_data_rate_var_name)
    else:
        out_df = out_df0

    return out_df


def test_error_linfit(x, y, deg=1):
    # fit line to x and y
    fitpars, cov = np.polyfit(x, y, deg=deg, cov=True)
    a = fitpars[0]
    b = fitpars[1]

    std_a = np.sqrt(np.diag(cov)[0])
    std_b = np.sqrt(np.diag(cov)[1])

    # std_a = np.diag(cov)[0]
    # std_b = np.diag(cov)[1]

    yfit = np.polyval([a, b], x)
    err_p = np.polyval([a + std_a, b + std_b], x)
    err_m = np.polyval([a - std_a, b - std_b], x)
    yerr_m = np.abs(err_m - yfit)
    yerr_p = np.abs(err_p - yfit)

    plt.figure()
    plt.plot(x, y, 'o')

    # plot line
    plt.plot(x, yfit, ls='-', label='lin fit')
    plt.plot(x, np.polyval([a + std_a, b + std_b], x), ls='--', label='lin fit ++')
    plt.plot(x, np.polyval([a + std_a, b - std_b], x), ls='--', label='lin fit +-')
    plt.plot(x, np.polyval([a - std_a, b + std_b], x), ls='--', label='lin fit -+')
    plt.plot(x, np.polyval([a - std_a, b - std_b], x), ls='--', label='lin fit --')
    plt.legend()

    plt.figure()
    plt.errorbar(x, yfit, yerr=[yerr_m, yerr_p])
    plt.plot(x, np.polyval([a + std_a, b + std_b], x), ls='--', label='lin fit ++')
    plt.plot(x, np.polyval([a - std_a, b - std_b], x), ls='--', label='lin fit --')
    plt.legend()

    return fitpars, cov


def calc_drad(PosX, PosY, PosZ):
    # MeanRad = (PosX ** 2 + PosY ** 2 + PosZ ** 2) ** 0.5
    MeanRad = euclidNorm([PosX, PosY, PosZ])

    return MeanRad


def euclidNorm(x):
    """
    Calculate Euclidian norm of x

    Args:
        x: [numpy.ndarray] Input data

    Returns:
        x_norm: The Euclidian norm of x

    """
    if isinstance(x, list):
        x = np.array(x)

    # reshape along shortest axis
    xx = np.reshape(x, (np.min(np.shape(x)), np.max(np.shape(x))))

    # calculate the norm
    x_norm = np.sum(xx ** 2, axis=0) ** 0.5

    return x_norm


def test_fig03(brk):
    # fig1 = plt.figure()
    # ax1 = fig1.gca()
    #
    # fig2 = plt.figure()
    # ax2 = fig2.gca()

    # part to read mean radial distance ---
    df = pd.read_pickle('./outputDF2_2020060100_2020060700.pkl')
    print('interpolating mean_rad')
    drad = df.mean_rad.interpolate(limit=10, limit_area='inside')
    # part to read mean radial distance ---

    # ax2.plot(1,1,'ro',label='fig2')
    # ax2.legend()
    # ax2.set_title('fig2')

    # ax1.plot(1,1,'rx',label='fig1')
    # ax1.legend()
    # ax1.set_title('fig1')

    N = len(brk)
    # N = 2

    diss_ind_lst = []
    inert_ind_lst = []
    range_err_lst = []
    interval_err_lst = []
    time_ix_lst = []
    range_error = []
    interval_error = []

    prevlen = 0

    for k in range(N):
        dat = brk[k]
        # time_ix = dat['spectime']
        # time_ix_lst.extend(time_ix)
        time_ix_lst.extend(dat['spectime'])

        # inert_ind = dat['inert_ind']
        # a = [i for i in inert_ind]
        # inert_ind_lst.extend(a)
        inert_ind_lst.extend(dat['inert_ind'])

        # diss_ind = dat['steep_ind']
        # b = [i for i in diss_ind]
        # diss_ind_lst.extend(b)
        diss_ind_lst.extend(dat['steep_ind'])

        fbreak = dat['breakfreq']
        # fbreak[dat['breakfreq_ei'] == 1] = np.nan
        # fbreak[dat['breakfreq_er'] == 1] = np.nan

        i_er = np.where(dat['breakfreq_er'] == 1)[0]
        i_ei = np.where(dat['breakfreq_ei'] == 1)[0]

        range_error.extend(dat['breakfreq_er'])
        interval_error.extend(dat['breakfreq_ei'])

        range_err_lst.extend(i_er + prevlen)
        interval_err_lst.extend(i_ei + prevlen)
        #
        # ax1 = ploterrorbar(fbreak, dat['breakfreq_ep'], dat['breakfreq_em'],
        #              ix=time_ix,
        #              ax=ax1,
        #              errlimit=None,
        #              pt_fmt='.')
        #
        # ax1.plot_date(time_ix[i_ei],
        #               fbreak[i_ei],
        #               marker='x',
        #               ls='',
        #               color='k',
        #               zorder=4)
        #
        # ax1.plot_date(time_ix[i_er],
        #               fbreak[i_er],
        #               marker='+',
        #               ls='',
        #               color='g',
        #               zorder=4)
        #
        # ax2.plot_date(time_ix, inert_ind, 'bo')
        # ax2.plot_date(time_ix[i_er], inert_ind[i_er], 'rx')
        # ax2.plot_date(time_ix[i_ei], inert_ind[i_ei], 'c+')

        prevlen += len(fbreak)

    # ax2.axhline(-5/3, color='r')

    # make dataframes
    df1 = pd.DataFrame(data=inert_ind_lst, index=time_ix_lst, columns=['inertix'])
    df1['dissix'] = diss_ind_lst
    df1['range_error'] = range_error
    df1['interval_error'] = interval_error
    df2 = ptools.newindex(df1, ix_new=drad.index)
    df2['drad'] = drad.values
    # make dataframes

    inert_ind_arr = np.array(inert_ind_lst)
    diss_ind_arr = np.array(diss_ind_lst)

    inert_ind_arr[range_err_lst] = np.nan
    inert_ind_arr[interval_err_lst] = np.nan

    diss_ind_arr[range_err_lst] = np.nan
    diss_ind_arr[interval_err_lst] = np.nan

    print(np.nanmean(inert_ind_arr))
    print(np.nanmean(diss_ind_arr))

    # plt.figure()
    # plt.plot(inert_ind_arr, 'rx')
    # plt.axhline(-5/3, color='k')
    # plt.axhline(np.nanmean(inert_ind_arr), color='b')
    #
    # plt.figure()
    # plt.plot_date(time_ix_lst, diss_ind_arr, 'rx')
    # # plt.axhline(-5/3, color='k')
    # plt.axhline(np.nanmean(diss_ind_arr), color='b')

    return df2, drad, time_ix_lst


def new_work3(dtime=env.time_selected, endtime=env.endtime_selected):
    # interpret user-selected start time
    dt_user = pd.datetime.strptime(dtime, env.input_time_format)
    # floor to the applicable time-delta of the data files
    dt_user_floor = pd.Timestamp(dt_user).floor(env.mag_time_delta)

    # interpret user-selected end time
    endtime = pd.datetime.strptime(endtime, env.input_time_format)
    enddate = pd.Timestamp(endtime).floor(env.mag_time_delta)

    # generate a list of dates, based on the time-delta of the data files
    datelist = pd.date_range(dt_user_floor, enddate, freq=env.mag_time_delta)

    # print datelist
    print(str(datelist[0]) + ' -- ' + str(datelist[-1]))

    # initialise the set of variables
    MeanRad = np.array([])
    Proton_Gyrofreq = np.array([])
    MeanVth = np.array([])
    Alfven_Speed = np.array([])
    Proton_Inertial_Length = np.array([])
    Proton_Gyroscale = np.array([])
    MeanModBINST = np.array([])
    MeanModBSC = np.array([])
    MeanDensity = np.array([])
    MeanT = np.array([])
    Proton_Kc = np.array([])
    Break_Scale_k = np.array([])
    Proton_plasma_frek = np.array([])
    MeanTperp = np.array([])
    MeanTpar = np.array([])

    # stefan added 2021/04/29
    Break_Freq = np.array([])
    newInert_ix = np.array([])
    # newSpectime = np.array([])
    Spectime = pd.DatetimeIndex([])

    # TODO add erros to break freq and break scale
    Break_Freq_m = np.array([])
    Break_Freq_p = np.array([])
    Break_Scale_k_m = np.array([])
    Break_Scale_k_p = np.array([])

    Inertial_index = np.array([])
    Dissip_index = np.array([])
    breakscale_ei = np.array([])
    breakscale_er = np.array([])
    brk_index = []

    breakfreq_dict_lst = []

    for i in datelist:

        #  this is basically 'main()' from pspbreaks ...
        print('user selected time: %s' % i)

        # round to nearest timestamp corresponding to file frequency
        dt_user_floor = pd.Timestamp(i).floor(env.mag_time_delta)
        print('floored user time: %s' % dt_user_floor)

        # initialise empty dataframes for return variables
        spi_df = pd.DataFrame()
        spc_df = pd.DataFrame()
        mag_df = pd.DataFrame()

        # calculate the break freq
        print('Now going to calc break freq')
        mag_df, brk_frq_vars = ppp.break_freq_download_calc(dt_user_floor)

        # if error value returned, then skip this iteration of the for-loop
        if brk_frq_vars == env.ERRORVAL:
            continue

        breakfreq_dict_lst.append(brk_frq_vars)

        # calculate the length scale
        print('Now going to calc len scale')
        lenscl_lst = ppp.len_scale_download_calc(dt_user_floor, brk_frq_vars=brk_frq_vars)

        # if error value returned, then skip this iteration of the for-loop
        if lenscl_lst == env.ERRORVAL:
            continue

        # append the length scale parameters for this date/time to the new variables
        MeanRad = np.append(MeanRad, lenscl_lst[0])
        Proton_Gyrofreq = np.append(Proton_Gyrofreq, lenscl_lst[1])
        MeanVth = np.append(MeanVth, lenscl_lst[2])
        Alfven_Speed = np.append(Alfven_Speed, lenscl_lst[3])
        Proton_Inertial_Length = np.append(Proton_Inertial_Length, lenscl_lst[4])
        Proton_Gyroscale = np.append(Proton_Gyroscale, lenscl_lst[5])
        MeanModBINST = np.append(MeanModBINST, lenscl_lst[6])
        MeanModBSC = np.append(MeanModBSC, lenscl_lst[7])
        MeanDensity = np.append(MeanDensity, lenscl_lst[8])
        MeanT = np.append(MeanT, lenscl_lst[9])
        Proton_Kc = np.append(Proton_Kc, lenscl_lst[10])
        Break_Scale_k = np.append(Break_Scale_k, lenscl_lst[11])
        Proton_plasma_frek = np.append(Proton_plasma_frek, lenscl_lst[12])
        MeanTperp = np.append(MeanTperp, lenscl_lst[13])
        MeanTpar = np.append(MeanTpar, lenscl_lst[14])
        Spectime = Spectime.append(lenscl_lst[15])
        Break_Freq = np.append(Break_Freq, lenscl_lst[16])
        # TODO put here: break_freq plus and minus, break_scale_k plus and minus
        Break_Freq_m = np.append(Break_Freq_m, lenscl_lst[17])
        Break_Freq_p = np.append(Break_Freq_p, lenscl_lst[18])
        Break_Scale_k_m = np.append(Break_Scale_k_m, lenscl_lst[19])
        Break_Scale_k_p = np.append(Break_Scale_k_p, lenscl_lst[20])

        # TODO incorporate the inertial and dissipation range indices (slopes)
        Inertial_index = np.append(Inertial_index, brk_frq_vars['inert_ind'])
        Dissip_index = np.append(Dissip_index, brk_frq_vars['steep_ind'])
        breakscale_ei = np.append(breakscale_ei, brk_frq_vars['breakfreq_ei'])
        breakscale_er = np.append(breakscale_er, brk_frq_vars['breakfreq_er'])
        brk_index.extend(brk_frq_vars['spectime'])

    # MeanModBINST
    # MeanModBSC
    # MeanDensity
    # MeanT
    # Proton\_Gyrofreq
    # w\_p (proton plasma frequency)
    # MeanRad
    # MeanVth
    # Alfven\_Speed
    # Proton\_Inertial\_Length
    # Proton\_Gyroscale
    # Proton\_Kc (cyclotron resonance scale)
    # Break\_Scale\_k
    # Break\_freq
    # inertial range index -- hierdie sal tussen Rob se code wees, weet nie wat sy naampie is nie.
    # T\_TENSOR: CDF\_FLOAT [6180, 6] - van SPI af, ek scheme dit sal ook die Mean moet wees.
    # Break\_Scale\_k

    if len(breakfreq_dict_lst) == 0:
        print('ERROR: no data in *breakfreq_dict_lst*')
        return env.ERRORVAL, env.ERRORVAL

    print('spectime length: ', len(Spectime))

    if len(Spectime) > 0:

        # vars to save
        output_df = pd.DataFrame(index=Spectime)
        # output_df = pd.DataFrame([])
        output_df['mean_rad'] = MeanRad
        output_df['modBinst'] = MeanModBINST
        output_df['modBSC'] = MeanModBSC
        output_df['meanDensity'] = MeanDensity
        output_df['meanT'] = MeanT
        output_df['g_proton'] = Proton_Gyrofreq
        output_df['w_proton'] = Proton_plasma_frek
        output_df['mean_vth'] = MeanVth
        output_df['v_alf'] = Alfven_Speed
        output_df['ilen_proton'] = Proton_Inertial_Length
        output_df['gscale_proton'] = Proton_Gyroscale
        output_df['kc_proton'] = Proton_Kc
        output_df['breakscale_k'] = Break_Scale_k
        output_df['breakfreq'] = Break_Freq
        # TODO incorporate break freq errors
        output_df['breakscale_k_m'] = Break_Scale_k_m
        output_df['breakscale_k_p'] = Break_Scale_k_p
        output_df['breakfreq_m'] = Break_Freq_m
        output_df['breakfreq_p'] = Break_Freq_p

        # TODO incorporate the inertial and dissipation range indices (slopes)
        print(output_df.index.size)
        print(len(Inertial_index))
        output_df['inert_ind'] = ptools.newindex(pd.DataFrame(data=Inertial_index, index=brk_index),
                                                 ix_new=output_df.index)
        output_df['steep_ind'] = ptools.newindex(pd.DataFrame(data=Dissip_index, index=brk_index),
                                                 ix_new=output_df.index)
        output_df['breakfreq_ei'] = ptools.newindex(pd.DataFrame(data=breakscale_ei, index=brk_index),
                                                    ix_new=output_df.index)
        output_df['breakfreq_er'] = ptools.newindex(pd.DataFrame(data=breakscale_er, index=brk_index),
                                                    ix_new=output_df.index)

        plotscales(MeanRad, Proton_Gyrofreq, MeanVth, Alfven_Speed, Proton_Inertial_Length,
                   Proton_Gyroscale, MeanModBINST, MeanModBSC, MeanDensity, MeanT, Proton_Kc,
                   Break_Scale_k, Proton_plasma_frek, MeanTperp, MeanTpar, Break_Scale_k_p, Break_Scale_k_m)

        output_filename = '_'.join(('outputDF2', env.time_selected, env.endtime_selected)) + '.pkl'
        output_df.to_pickle(output_filename, protocol=4)

    else:
        output_df = env.ERRORVAL
        breakfreq_dict_lst = env.ERRORVAL

    # return output_df
    return output_df, breakfreq_dict_lst


def new_work4(dtime=env.time_selected, endtime=env.endtime_selected):
    # interpret user-selected start time
    dt_user = pd.datetime.strptime(dtime, env.input_time_format)
    # floor to the applicable time-delta of the data files
    dt_user_floor = pd.Timestamp(dt_user).floor(env.mag_time_delta)

    # interpret user-selected end time
    endtime = pd.datetime.strptime(endtime, env.input_time_format)
    enddate = pd.Timestamp(endtime).floor(env.mag_time_delta)

    # generate a list of dates, based on the time-delta of the data files
    datelist = pd.date_range(dt_user_floor, enddate, freq=env.mag_time_delta)

    # print datelist
    print(str(datelist[0]) + ' -- ' + str(datelist[-1]))

    # initialise the set of variables
    MeanRad = np.array([])
    Proton_Gyrofreq = np.array([])
    MeanVth = np.array([])
    Alfven_Speed = np.array([])
    Proton_Inertial_Length = np.array([])
    Proton_Gyroscale = np.array([])
    MeanModBINST = np.array([])
    MeanModBSC = np.array([])
    MeanDensity = np.array([])
    MeanT = np.array([])
    Proton_Kc = np.array([])
    Break_Scale_k = np.array([])
    Proton_plasma_frek = np.array([])
    MeanTperp = np.array([])
    MeanTpar = np.array([])
    # TODO add mean-mod-v to the set of output vars
    MeanModV = np.array([])

    # stefan added 2021/04/29
    Break_Freq = np.array([])
    newInert_ix = np.array([])
    # newSpectime = np.array([])
    Spectime = pd.DatetimeIndex([])

    # TODO add erros to break freq and break scale
    Break_Freq_m = np.array([])
    Break_Freq_p = np.array([])
    Break_Scale_k_m = np.array([])
    Break_Scale_k_p = np.array([])

    Inertial_index = np.array([])
    Dissip_index = np.array([])

    Inertial_index_err = np.array([])
    Dissip_index_err = np.array([])

    breakscale_ei = np.array([])
    breakscale_er = np.array([])
    # TODO create an error check for the errorbars
    breakscale_eb = np.array([])
    brk_index = []

    breakfreq_dict_lst = []

    # brtn_array = np.array()
    magdflist = []

    for i in datelist:

        #  this is basically 'main()' from pspbreaks ...
        print('user selected time: %s' % i)

        # round to nearest timestamp corresponding to file frequency
        dt_user_floor = pd.Timestamp(i).floor(env.mag_time_delta)
        print('floored user time: %s' % dt_user_floor)

        # initialise empty dataframes for return variables
        spi_df = pd.DataFrame()
        spc_df = pd.DataFrame()
        mag_df = pd.DataFrame()

        # calculate the break freq
        print('Now going to calc break freq')
        mag_df, brk_frq_vars = ppp.break_freq_download_calc(dt_user_floor)

        magdflist.append(mag_df)

        # if error value returned, then skip this iteration of the for-loop
        if brk_frq_vars == env.ERRORVAL:
            continue

        breakfreq_dict_lst.append(brk_frq_vars)

        # calculate the length scale
        print('Now going to calc len scale')
        lenscl_lst = ppp.len_scale_download_calc(dt_user_floor, brk_frq_vars=brk_frq_vars)

        # if error value returned, then skip this iteration of the for-loop
        # if lenscl_lst == env.ERRORVAL:
        #     continue

        if isinstance(lenscl_lst, type(env.ERRORVAL)):
            if lenscl_lst == env.ERRORVAL:
                continue

        # append the length scale parameters for this date/time to the new variables
        # MeanRad = np.append(MeanRad, lenscl_lst[0])
        MeanRad = np.append(MeanRad, lenscl_lst.MeanRad.values)
        # Proton_Gyrofreq = np.append(Proton_Gyrofreq, lenscl_lst[1])
        Proton_Gyrofreq = np.append(Proton_Gyrofreq, lenscl_lst.Proton_Gyrofreq.values)
        # MeanVth = np.append(MeanVth, lenscl_lst[2])
        MeanVth = np.append(MeanVth, lenscl_lst.MeanVth.values)
        # Alfven_Speed = np.append(Alfven_Speed, lenscl_lst[3])
        Alfven_Speed = np.append(Alfven_Speed, lenscl_lst.Alfven_Speed.values)
        # Proton_Inertial_Length = np.append(Proton_Inertial_Length, lenscl_lst[4])
        Proton_Inertial_Length = np.append(Proton_Inertial_Length, lenscl_lst.Proton_Inertial_Length)
        # Proton_Gyroscale = np.append(Proton_Gyroscale, lenscl_lst[5])
        Proton_Gyroscale = np.append(Proton_Gyroscale, lenscl_lst.Proton_Gyroscale)
        MeanModBINST = np.append(MeanModBINST, lenscl_lst.MeanModBINST)
        MeanModBSC = np.append(MeanModBSC, lenscl_lst.MeanModBSC)
        # MeanDensity = np.append(MeanDensity, lenscl_lst[8])
        MeanDensity = np.append(MeanDensity, lenscl_lst.MeanDensity)
        # MeanT = np.append(MeanT, lenscl_lst[9])
        MeanT = np.append(MeanT, lenscl_lst.MeanT)
        Proton_Kc = np.append(Proton_Kc, lenscl_lst.Proton_Kc)
        # Break_Scale_k = np.append(Break_Scale_k, lenscl_lst[11])
        Break_Scale_k = np.append(Break_Scale_k, lenscl_lst.Break_Scale_k)
        # Proton_plasma_frek = np.append(Proton_plasma_frek, lenscl_lst[12])
        Proton_plasma_frek = np.append(Proton_plasma_frek, lenscl_lst.w_p)
        # MeanTperp = np.append(MeanTperp, lenscl_lst[13])
        MeanTperp = np.append(MeanTperp, lenscl_lst.MeanTperp)
        # MeanTpar = np.append(MeanTpar, lenscl_lst[14])
        MeanTpar = np.append(MeanTpar, lenscl_lst.MeanTpar)
        # Spectime = Spectime.append(lenscl_lst[15])
        Spectime = np.append(Spectime, lenscl_lst.Spectime)
        # Break_Freq = np.append(Break_Freq, lenscl_lst[16])
        Break_Freq = np.append(Break_Freq, lenscl_lst.Break_Freq)
        # Break_Freq_m = np.append(Break_Freq_m, lenscl_lst[17])
        Break_Freq_m = np.append(Break_Freq_m, lenscl_lst.Break_Freq_m)
        # Break_Freq_p = np.append(Break_Freq_p, lenscl_lst[18])
        Break_Freq_p = np.append(Break_Freq_p, lenscl_lst.Break_Freq_p)
        # Break_Scale_k_m = np.append(Break_Scale_k_m, lenscl_lst[19])
        Break_Scale_k_m = np.append(Break_Scale_k_m, lenscl_lst.Break_Scale_k_m)
        # Break_Scale_k_p = np.append(Break_Scale_k_p, lenscl_lst[20])
        Break_Scale_k_p = np.append(Break_Scale_k_p, lenscl_lst.Break_Scale_k_p)
        # MeanModV = np.append(MeanModV, lenscl_lst[21])
        MeanModV = np.append(MeanModV, lenscl_lst.MeanModV)

        # spi_mag_sc = np.append(MeanModV, lenscl_lst[22])

        Inertial_index = np.append(Inertial_index, brk_frq_vars['inert_ind'])
        Dissip_index = np.append(Dissip_index, brk_frq_vars['steep_ind'])
        breakscale_ei = np.append(breakscale_ei, brk_frq_vars['breakfreq_ei'])
        breakscale_er = np.append(breakscale_er, brk_frq_vars['breakfreq_er'])
        breakscale_eb = np.append(breakscale_eb, brk_frq_vars['breakfreq_eb'])
        brk_index.extend(brk_frq_vars['spectime'])

        Inertial_index_err = np.append(Inertial_index_err, brk_frq_vars['inert_ind_e'])
        Dissip_index_err = np.append(Dissip_index_err, brk_frq_vars['steep_ind_e'])

    # MeanModBINST
    # MeanModBSC
    # MeanDensity
    # MeanT
    # Proton\_Gyrofreq
    # w\_p (proton plasma frequency)
    # MeanRad
    # MeanVth
    # Alfven\_Speed
    # Proton\_Inertial\_Length
    # Proton\_Gyroscale
    # Proton\_Kc (cyclotron resonance scale)
    # Break\_Scale\_k
    # Break\_freq
    # inertial range index -- hierdie sal tussen Rob se code wees, weet nie wat sy naampie is nie.
    # T\_TENSOR: CDF\_FLOAT [6180, 6] - van SPI af, ek scheme dit sal ook die Mean moet wees.
    # Break\_Scale\_k

    if len(breakfreq_dict_lst) == 0:
        print('ERROR: no data in *breakfreq_dict_lst*')
        return env.ERRORVAL, env.ERRORVAL

    print('spectime length: ', len(Spectime))

    if len(Spectime) > 0:

        # vars to save
        output_df = pd.DataFrame(index=Spectime)
        # output_df = pd.DataFrame([])
        output_df['mean_rad'] = MeanRad
        output_df['modBinst'] = MeanModBINST
        output_df['modBSC'] = MeanModBSC
        output_df['meanDensity'] = MeanDensity
        output_df['meanT'] = MeanT
        output_df['g_proton'] = Proton_Gyrofreq
        output_df['w_proton'] = Proton_plasma_frek
        output_df['mean_vth'] = MeanVth
        output_df['v_alf'] = Alfven_Speed
        output_df['ilen_proton'] = Proton_Inertial_Length
        output_df['gscale_proton'] = Proton_Gyroscale
        output_df['kc_proton'] = Proton_Kc
        output_df['breakscale_k'] = Break_Scale_k
        output_df['breakfreq'] = Break_Freq
        output_df['vsw_mean'] = MeanModV

        # TODO incorporate break freq errors
        output_df['breakscale_k_m'] = Break_Scale_k_m
        output_df['breakscale_k_p'] = Break_Scale_k_p
        output_df['breakfreq_m'] = Break_Freq_m
        output_df['breakfreq_p'] = Break_Freq_p

        # TODO incorporate the inertial and dissipation range indices (slopes)
        print(output_df.index.size)
        print(len(Inertial_index))
        output_df['inert_ind'] = ptools.newindex(pd.DataFrame(data=Inertial_index, index=brk_index),
                                                 ix_new=output_df.index)
        output_df['steep_ind'] = ptools.newindex(pd.DataFrame(data=Dissip_index, index=brk_index),
                                                 ix_new=output_df.index)
        output_df['breakfreq_ei'] = ptools.newindex(pd.DataFrame(data=breakscale_ei, index=brk_index),
                                                    ix_new=output_df.index)
        output_df['breakfreq_er'] = ptools.newindex(pd.DataFrame(data=breakscale_er, index=brk_index),
                                                    ix_new=output_df.index)
        # TODO create an error check for the errorbars
        output_df['breakfreq_eb'] = ptools.newindex(pd.DataFrame(data=breakscale_eb, index=brk_index),
                                                    ix_new=output_df.index)

        output_df['inert_ind_e'] = ptools.newindex(pd.DataFrame(data=Inertial_index_err, index=brk_index),
                                                   ix_new=output_df.index)
        output_df['steep_ind_e'] = ptools.newindex(pd.DataFrame(data=Dissip_index_err, index=brk_index),
                                                   ix_new=output_df.index)

        plotscales(MeanRad, Proton_Gyrofreq, MeanVth, Alfven_Speed, Proton_Inertial_Length,
                   Proton_Gyroscale, MeanModBINST, MeanModBSC, MeanDensity, MeanT, Proton_Kc,
                   Break_Scale_k, Proton_plasma_frek, MeanTperp, MeanTpar, Break_Scale_k_p, Break_Scale_k_m)

        output_filename = '_'.join(('outputDF2', env.time_selected, env.endtime_selected)) + '.pkl'
        output_df.to_pickle(output_filename)

    else:
        output_df = env.ERRORVAL
        breakfreq_dict_lst = env.ERRORVAL

    # return output_df
    return output_df, breakfreq_dict_lst, magdflist


def plotscales(newMeanRad, newProton_Gyrofreq, newMeanVth, newAlfven_Speed, newProton_Inertial_Length,
               newProton_Gyroscale, newMeanModBINST, newMeanModBSC, newMeanDensity, newMeanT, newProton_Kc,
               newBreak_Scale_k, newProton_plasma_frek, newMeanTperp, newMeanTpar, Break_Scale_k_p, Break_Scale_k_m):
    # plt.close('all')

    breakscale_yerr = [np.abs(newBreak_Scale_k - Break_Scale_k_m), np.abs(Break_Scale_k_p - newBreak_Scale_k)]
    breakscale_km = (1 / newBreak_Scale_k) / 1000.
    breakscale_km_p = (1 / Break_Scale_k_p) / 1000.
    breakscale_km_m = (1 / Break_Scale_k_m) / 1000.
    breakscale_k_yerr = [np.abs(breakscale_km - breakscale_km_p), np.abs(breakscale_km - breakscale_km_m)]

    # plt.figure()
    # plt.errorbar(np.arange(len(newBreak_Scale_k)), breakscale_km, yerr=breakscale_k_yerr, fmt='o')
    #
    #
    # plt.figure()
    # plt.errorbar(np.arange(len(newBreak_Scale_k)), newBreak_Scale_k, yerr=breakscale_yerr, fmt='o')
    # # plt.plot(newBreak_Scale_k, 'x-')
    # plt.plot(Break_Scale_k_m,  'rs')
    # plt.plot(Break_Scale_k_p, 'b^')
    # plt.plot(newBreak_Scale_k-breakscale_yerr[0], 'k.')
    # plt.plot(newBreak_Scale_k+breakscale_yerr[1], 'kx')

    r, B_mag, Dens, Temp, Omega_p, w_p, V_A, V_therm_proton, l_d, p_g, p_ii = importComp()

    f, ((ax1), (ax2), (ax3)) = plt.subplots(3, 1, sharex=True)

    ax1.scatter(newMeanRad / plasma.AU_KM, newProton_Gyrofreq, marker='s', facecolor='none', edgecolor='blue',
                label='PSP Proton Gyrofrequency (rad/s)')
    ax1.scatter(newMeanRad / plasma.AU_KM, newProton_plasma_frek, marker='s', facecolor='none', edgecolor='red',
                label='PSP Proton plasma frequency (rad/s)')
    ax1.plot(r, w_p, linewidth=2, linestyle='--', color='red', label='Proton plasma frequency (rad/s)')
    ax1.plot(r, Omega_p, linewidth=1, linestyle='--', color='blue', label='Proton cyclotron frequency (rad/s)')
    ax1.legend(loc='upper right')
    ax1.set_yscale('log')
    ax1.set_xscale('linear')

    ax2.scatter(newMeanRad / plasma.AU_KM, newMeanVth / 1000., marker='s', facecolor='none', edgecolor='blue',
                label='PSP Mean V thermal (km/s)')
    ax2.scatter(newMeanRad / plasma.AU_KM, newAlfven_Speed / 1000., marker='s', facecolor='none', edgecolor='red',
                label='PSP Alfven speed (km/s)')
    ax2.plot(r, V_A, linewidth=2, linestyle='--', color='red', label='Alfven speed (km/s)')
    ax2.plot(r, V_therm_proton, linewidth=1, linestyle='--', color='blue', label='Proton thermal speed (km/s)')
    ax2.legend(loc='upper right')
    ax2.set_yscale('log')
    ax2.set_xscale('linear')

    ax3.scatter(newMeanRad / plasma.AU_KM, newProton_Inertial_Length / 1000., marker='s', facecolor='none',
                edgecolor='blue', label='PSP Proton Inert L (km)')
    ax3.scatter(newMeanRad / plasma.AU_KM, newProton_Gyroscale / 1000., marker='s', facecolor='none', edgecolor='green',
                label='PSP Proton gyroscale (km)')
    ax3.scatter(newMeanRad / plasma.AU_KM, 1. / newProton_Kc / 1000., marker='s', facecolor='none', edgecolor='red',
                label='PSP Cyclotron resonance scale (km)')
    ax3.plot(r, p_g, linewidth=2, linestyle='--', color='green', label='Proton gyroscale (km)')
    ax3.plot(r, p_ii, linewidth=1, linestyle='--', color='blue', label='Proton Inert L (km)')
    ax3.plot(r, l_d, linewidth=1, linestyle='--', color='red', label='Cyclotron resonance scale (km)')

    ax3.legend(loc='upper right')
    ax3.set_yscale('log')
    ax3.set_xscale('linear')

    g, ((ax4), (ax5), (ax6)) = plt.subplots(3, 1, sharex=True)

    ax4.scatter(newMeanRad / plasma.AU_KM, newMeanModBSC / 1e-9, marker='s', facecolor='none', edgecolor='blue',
                label='PSP Magnetic field mag Spacecraft coord (nT)')
    ax4.scatter(newMeanRad / plasma.AU_KM, newMeanModBINST / 1e-9, marker='s', facecolor='none', edgecolor='red',
                label='PSP Magnetic field mag Instrument coord (nT)')
    ax4.plot(r, B_mag, linewidth=2, linestyle='--', color='red', label='Magnetic field mag (nT)')
    ax4.legend(loc='upper right')
    ax4.set_yscale('log')
    ax4.set_xscale('linear')

    ax5.scatter(newMeanRad / plasma.AU_KM, newMeanDensity / 100. / 100. / 100., marker='s', facecolor='none',
                edgecolor='orange', label='PSP Primary proton population density (#/cc)')
    ax5.plot(r, Dens, linewidth=2, linestyle='--', color='orange', label='Proton number density (#/cc)')
    ax5.legend(loc='upper right')
    ax5.set_yscale('log')
    ax5.set_xscale('linear')

    ax6.scatter(newMeanRad / plasma.AU_KM, newMeanT, marker='s', edgecolor='blue', facecolor='none',
                label='PSP eff temp (K)')
    ax6.plot(r, Temp, linewidth=2, linestyle='--', color='blue', label='Effective Temp (K)')
    ax6.legend(loc='upper right')
    ax6.set_yscale('log')
    ax6.set_xscale('linear')

    h, ((ax3)) = plt.subplots(1, 1, sharex=True)

    ax3.errorbar(newMeanRad / plasma.AU_KM,
                 breakscale_km,
                 yerr=breakscale_k_yerr,
                 fmt='ro',
                 capsize=4,
                 label='CALCULATION FROM FFT (km)',
                 ecolor='gray')

    # ax3.scatter(newMeanRad / plasma.AU_KM, 1. / newBreak_Scale_k / 1000., marker='go', label='CALCULATION FROM FFT (km)')
    # ax3.scatter(newMeanRad / plasma.AU_KM, breakscale_km_m, marker='^', color='b', label='breakscale_m')
    # ax3.scatter(newMeanRad / plasma.AU_KM, breakscale_km_p, marker='v', color='r', label='breakscale_p')

    # TODO use errorbars to plot breakscale

    ax3.plot(r, p_g, linewidth=2, linestyle='--', color='green', label='Proton gyroscale (km)')
    ax3.plot(r, p_ii, linewidth=1, linestyle='--', color='blue', label='Proton Inert L (km)')
    ax3.plot(r, l_d, linewidth=1, linestyle='--', color='red', label='Cyclotron resonance scale (km)')

    ax3.legend(loc='upper right')
    ax3.set_yscale('log')
    ax3.set_xscale('linear')
    ax3.set_xlabel('Mean Rad Distance [AU]')
    ax3.set_ylabel('Scale [km]')

    k, ((ax3)) = plt.subplots(1, 1, sharex=True)

    # (observed - predicted)/observed
    resid_k_d = (1. / newBreak_Scale_k / 1000. - 1. / newProton_Kc / 1000.) / 1. / newBreak_Scale_k / 1000.
    # (observed - predicted)/observed
    resid_gyro = (newProton_Gyroscale / 1000. - 1. / newProton_Kc / 1000.) / 1. / newBreak_Scale_k / 1000.
    # (observed - predicted)/observed
    resid_inertial = (newProton_Inertial_Length / 1000. - 1. / newProton_Kc / 1000.) / 1. / newBreak_Scale_k / 1000.

    ax3.hist(resid_k_d, color='red', alpha=0.3, label='Cyclotron resonance', range=(-1000, 2000), bins=100)
    ax3.hist(resid_gyro, color='blue', alpha=0.3, label='Gyroscale', range=(-2000, 2000), bins=100)
    ax3.hist(resid_inertial, color='green', alpha=0.3, label='Proton inertial', range=(-2000, 2000), bins=100)
    ax3.legend()

    # f.savefig(os.path.join(env.image_dir, 'plasma_quantities_1.pdf'), dpi=300, bbox_inches='tight')
    # g.savefig(os.path.join(env.image_dir, 'plasma_quantities_2.pdf'), dpi=300, bbox_inches='tight')
    # h.savefig(os.path.join(env.image_dir, 'plasma_quantities_3.pdf'), dpi=300, bbox_inches='tight')
    # k.savefig(os.path.join(env.image_dir, 'plasma_quantities_4.pdf'), dpi=300, bbox_inches='tight')

    return 999


def ploterrorbar(x, xp, xm, ix=None, errlimit=None, pt_fmt='o', datcol='C1', errcol='gray', ax=None, pickpoints=False,
                 label=None):
    """
    Helper to utilise the `matplotlib.pyplot.errorbar()` function.


    Args:
        x: [] Data to be plotted
        xp: [] The positive ('plus') error
        xm: [] Negative ('minus') error
        ix: [] The index (x-axis) to plot against
        errlimit: [] Only plot errorbars if they are smaller than `errlimit` times the value of the data point
        pt_fmt: [str] Format string for the data points
        datcol: [str] Format string for the data plot colour
        errcol: [str] Format string for the errorbar colour
        ax: [matplotlib.axis] Axis to plot on. Default is `None` - new figure opened

    Returns:
        ax.figure the Figure handle
    """

    # remove data points with too high error
    if errlimit is not None:
        xp[np.abs(xp - x) / errlimit > x] = np.nan
        x[np.abs(xp - x) / errlimit > x] = np.nan

        xm[np.abs(xm - x) / errlimit > x] = np.nan
        x[np.abs(xm - x) / errlimit > x] = np.nan

    # create generic x-axis index to plot against
    if ix is None:
        ix = np.arange(len(x))

    # create new figure window and get the axis handle
    if ax is None:
        fig = plt.figure()
        ax = fig.gca()

    # use MATPLOTLIB ERRORBAR function to make the plot
    if pickpoints:
        ax.errorbar(ix, x, yerr=[np.abs(x - xm), np.abs(x - xp)],
                    capsize=2,
                    fmt=pt_fmt,
                    color=datcol,
                    ecolor=errcol,
                    picker=True,
                    pickradius=5,
                    label=label)
    else:
        ax.errorbar(ix, x, yerr=[np.abs(x - xm), np.abs(x - xp)],
                    capsize=2,
                    fmt=pt_fmt,
                    color=datcol,
                    ecolor=errcol,
                    label=label)

    return ax.figure


def test_pickpoints(fig, n=3, ):
    """
    Code brazenly copied from stackoverflow. It facilitates picking a point on a graph with the mouse.


    Args:
        fig: [matplotlib.figure.Figure] Figure handle to use
        n: [int] Number of points to pick

    Returns:

    """

    def onpick(event):
        """
        Picker event

        Args:
            event:
        """

        # continue while number of points not picked yet
        if len(ind_list) < n:
            thisline = event.artist
            xdata = thisline.get_xdata()
            ydata = thisline.get_ydata()
            ind = event.ind[0]
            print(ind)
            ind_list.append(ind)
            # points = tuple(zip(xdata[ind[0]], ydata[ind]))
            points = [xdata[ind], ydata[ind]]

            # plot a black 'X' on the picked point
            plt.plot(xdata[ind], ydata[ind], 'kx', zorder=999)
            plt.draw()
        else:
            print('already at %d points' % n)

    # get axis, put title
    ax = fig.gca()
    ax.set_title('click on %d points' % n)

    # pick event
    ind_list = []
    fig.canvas.mpl_connect('pick_event', onpick)
    plt.show()

    return ind_list


def test_pick2(fig, n=3):
    # fig, ax = plt.subplots()
    # ax.set_title('click on points')
    #
    # line, = ax.plot(np.random.rand(100), 'o',
    #                 picker=True, pickradius=5)  # 5 points tolerance

    pointslist = []

    def onpick(event):

        if len(pointslist) < n:

            print('asdf ', len(pointslist))
            thisline = event.artist
            xdata = thisline.get_xdata()
            ydata = thisline.get_ydata()
            ind = event.ind
            points = tuple(zip(xdata[ind], ydata[ind]))
            print('onpick points:', points)
            pointslist.append(points)

        else:
            print('Plist len = ', len(pointslist))

    fig.canvas.mpl_connect('pick_event', onpick)

    plt.show()

    # print(pointslist)

    return pointslist


def binbyrad(peri_dat, yvar='breakfreq', drad='mean_rad', n_radbins=5, n_bins=None, fig_suptitle='', hist_xlabel='',
             lax_scale=['linear', 'linear'], rax_scale=['linear', 'linear'], yerr_p_var=None, yerr_m_var=None):
    """
    Make a fancy plot to show distribution of values (YVAR) binned by radial distance DRAD.


    Args:
        peri_dat: [pandas.DataFrame] Dataframe that contains the YVAR to be plotted and the DRAD to bin against.
        yvar: [str] The string name of the variable to be binned
        drad: [str] The string name of the mean radial distance variable
        n_radbins: [int] Number of DRAD bins to be used / plotted (`n_radbins - 1` bins will be plotted)
        n_bins: [int] Number of bins to create in each histogram. Default behaviour `n_bins=None` results in number of
         bins being determined automatically by `numpy.histogram_bin_edges()`
        fig_suptitle: [str] Title of the main figure
        hist_xlabel: [str] X-axis label of the histograms; this is the full name of YVAR
        lax_scale: [list of str] Scale descriptors of left-hand set of axes, for x- and y-axis. E.g.
         `lax_scale=['linear', 'log'] will have linear scale x-axis and log-scale y-axis.
        rax_scale: [list of str] Scale descriptors of right-hand set of axes, for x- and y-axis. E.g.
         `lax_scale=['linear', 'log'] will have linear scale x-axis and log-scale y-axis.

    Returns:
        rad_bins: [numpy.ndarray] The DRAD bin edges
        ymed_arr: [numpy.ndarray] Array of the median value of each histogram

    """

    # plt.style.use('seaborn-muted')
    # plt.style.use('seaborn')
    # plt.style.use('seaborn-pastel')
    # plt.style.use('seaborn-talk')
    # plt.style.use('seaborn-colorblind')
    # plt.style.use('fivethirtyeight')

    # generate the DRAD bins
    rad_bins = np.linspace(peri_dat[drad].min(), peri_dat[drad].max(), n_radbins)

    # initialise histogram and bin edges lists
    hist_list = []
    bin_edges_list = []
    std_list = []

    # initialise YMED_ARR with NANs
    ymed_arr = np.full_like(rad_bins[1:], fill_value=np.nan)
    yerrp = np.full_like(rad_bins[1:], fill_value=np.nan)
    yerrm = np.full_like(rad_bins[1:], fill_value=np.nan)

    # set up the x and y data values
    x0 = peri_dat[drad].values
    y0 = peri_dat[yvar].values

    # set up the histogram bins
    if n_bins is None:
        allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins='auto')
    else:
        allbins = np.linspace(peri_dat[yvar].min(), peri_dat[yvar].max(), n_bins)

    # # some other options for generating bins -- just keeping here as a reminder
    # allbins = np.linspace(peri_dat[yvar].min(), peri_dat[yvar].max(), n_bins)
    # allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins='fd')
    # allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins='auto')
    # allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins='doane')
    # allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins=n_bins)

    # initiliase the figure
    fig, axs = plt.subplots(nrows=n_radbins - 1, ncols=2, sharex='col')

    # build the figure title (adding the DRAD range to user-supplied FIG_SUPTITLE)
    suptitle_drad = ' D = [%.2f, %.2f] AU' % (peri_dat[drad].min() / plasma.AU_KM, peri_dat[drad].max() / plasma.AU_KM)
    fig.suptitle(fig_suptitle + suptitle_drad)

    # add blank axis titles to fix spacing
    axs[0, 0].set_title(' ')
    axs[0, 1].set_title(' ')

    for r in range(len(rad_bins[1:])):
        # convert to AU
        x = peri_dat[drad][(peri_dat[drad] >= rad_bins[r]) & (peri_dat[drad] < rad_bins[r + 1])].values

        # convert from AU to KM
        x = x / plasma.AU_KM

        # get the data to be used on the Y axis from the PERI_DAT dataframe, for this bin
        y = peri_dat[yvar][(peri_dat[drad] >= rad_bins[r]) & (peri_dat[drad] < rad_bins[r + 1])]

        # calculate standard deviation for each bin
        std_list.append(np.std(y))

        # execute the histogram, after dropping missing values
        hist, bin_edges = np.histogram(y.dropna().values, bins=allbins)

        # get the median of the current histogram
        # ymed_arr[r] = np.nanmedian(y.dropna().values)
        # ymed_arr[r] = np.nanmean(y.dropna().values)
        ymed_arr[r] = y.mean()
        # ymed_arr[r] = y.median()
        # ymed_arr[r] = bin_edges[np.argmax(hist)]

        if yerr_p_var is not None:
            yerrp[r] = peri_dat[yerr_p_var][(peri_dat[drad] >= rad_bins[r]) & (peri_dat[drad] < rad_bins[r + 1])].mean()
            yerrm[r] = peri_dat[yerr_m_var][(peri_dat[drad] >= rad_bins[r]) & (peri_dat[drad] < rad_bins[r + 1])].mean()

        # add current histogram to list
        hist_list.append(hist)

        # add bin edges to list (and convert to AU)
        bin_edges_list.append(bin_edges / plasma.AU_KM)

        # plot the histogram
        axs[r, 0].step(bin_edges, np.append(hist, hist[-1]), color='C1', where='mid',
                       label='D = (%.2f, %.2f) AU [%d]' % (
                           rad_bins[r] / plasma.AU_KM, rad_bins[r + 1] / plasma.AU_KM, len(x)))

        # plot the median of the histogram, with appropriate label
        axs[r, 0].axvline(ymed_arr[r], ls='--', color='C2', label='median = %.2e' % ymed_arr[r])

        # set axis scales
        axs[r, 0].set_xscale(lax_scale[0])
        axs[r, 0].set_yscale(lax_scale[1])

        axs[r, 1].set_xscale(rax_scale[0])
        axs[r, 1].set_yscale(rax_scale[1])

        # plot the data versus DRAD
        axs[r, 1].plot(x0 / plasma.AU_KM, y0, marker='.', ls='', ms=4, label='all')
        axs[r, 1].plot(x, y, marker='.', ls='', label='sel')
        axs[r, 1].set_xscale(rax_scale[0])
        axs[r, 1].set_yscale(rax_scale[1])

        # put legends and grids
        axs[r, 0].legend()
        axs[r, 1].legend()
        # axs[r, 0].grid(which='major', axis='both')
        # axs[r, 1].grid(which='major', axis='both')

        axs[r, 1].yaxis.set_label_position("right")
        axs[r, 1].yaxis.tick_right()
        axs[r, 1].set_ylabel(hist_xlabel)
        axs[r, 0].set_ylabel('Count')

    axs[r, 0].set_xlabel(hist_xlabel)
    axs[r, 1].set_xlabel('Radial distance D [AU]')

    plt.tight_layout()

    if yerr_m_var is not None:
        return rad_bins, ymed_arr, [yerrp, yerrm]

    # return rad_bins, ymed_arr, np.array(std_list)
    return rad_bins, ymed_arr


def binbypoints(peri_dat, yvar='breakfreq', drad='mean_rad', n_radbins=5, n_bins=None, fig_suptitle='', hist_xlabel='',
                lax_scale=['linear', 'linear'], rax_scale=['linear', 'linear']):
    """
    Make a fancy plot to show distribution of values (YVAR) binned by NUMBER OF POINTS, NOT DRAD

    Args:
        peri_dat: [pandas.DataFrame] Dataframe that contains the YVAR to be plotted and the DRAD to bin against.
        yvar: [str] The string name of the variable to be binned
        drad: [str] The string name of the mean radial distance variable
        n_radbins: [int] Number of DRAD bins to be used / plotted (`n_radbins - 1` bins will be plotted)
        n_bins: [int] Number of bins to create in each histogram. Default behaviour `n_bins=None` results in number of
         bins being determined automatically by `numpy.histogram_bin_edges()`
        fig_suptitle: [str] Title of the main figure
        hist_xlabel: [str] X-axis label of the histograms; this is the full name of YVAR
        lax_scale: [list of str] Scale descriptors of left-hand set of axes, for x- and y-axis. E.g.
         `lax_scale=['linear', 'log'] will have linear scale x-axis and log-scale y-axis.
        rax_scale: [list of str] Scale descriptors of right-hand set of axes, for x- and y-axis. E.g.
         `lax_scale=['linear', 'log'] will have linear scale x-axis and log-scale y-axis.

    Returns:
        rad_bins: [numpy.ndarray] The DRAD bin edges
        ymed_arr: [numpy.ndarray] Array of the median value of each histogram

    """

    import math

    # generate the binned intervals
    rad_bins = np.linspace(0, len(peri_dat), n_radbins).astype(int)
    print(rad_bins)

    # initialise histogram and bin edges lists
    hist_list = []
    bin_edges_list = []

    # initialise YMED_ARR and stdev with NANs
    ymed_arr = np.full_like(rad_bins[1:], fill_value=np.nan, dtype=float)
    stdev = np.full_like(rad_bins[1:], fill_value=np.nan)
    my_ymed_arr = np.array([])
    my_stdev = np.array([])
    dist = np.array([])

    # set up the x and y data values
    x0 = peri_dat[drad].values
    y0 = peri_dat[yvar].values

    # set up the histogram bins
    if n_bins is None:
        allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins='auto')
    else:
        allbins = np.linspace(peri_dat[yvar].min(), peri_dat[yvar].max(), n_bins)

    # initiliase the figure
    fig, axs = plt.subplots(nrows=n_radbins - 1, ncols=2, sharex='col')

    # build the figure title (adding the DRAD range to user-supplied FIG_SUPTITLE)
    suptitle_drad = ' D = [%.2f, %.2f] AU' % (peri_dat[drad].min() / plasma.AU_KM, peri_dat[drad].max() / plasma.AU_KM)
    fig.suptitle(fig_suptitle + suptitle_drad)

    # add blank axis titles to fix spacing
    axs[0, 0].set_title(' ')
    axs[0, 1].set_title(' ')

    for r in range(len(rad_bins[1:])):
        # get the data to be used on the Y axis from the PERI_DAT dataframe, for this bin
        x = x0[rad_bins[r]:rad_bins[r + 1]]
        y = y0[rad_bins[r]:rad_bins[r + 1]]

        # convert from KM to AU
        x = x / plasma.AU_KM

        # execute the histogram, after dropping missing values
        hist, bin_edges = np.histogram(y, bins=allbins)

        # get the median of the current histogram
        ymed_arr[r] = np.nanmean(y)

        # add bin edges to list (and convert to AU)
        bin_edges_list.append(bin_edges / plasma.AU_KM)

        # plot the histogram
        axs[r, 0].step(bin_edges, np.append(hist, hist[-1]), color='C1', where='mid')

        # plot the median of the histogram, with appropriate label
        axs[r, 0].axvline(ymed_arr[r], ls='--', color='C2', label='median = %.2f' % ymed_arr[r])

        # set axis scales
        axs[r, 0].set_xscale(lax_scale[0])
        axs[r, 0].set_yscale(lax_scale[1])

        axs[r, 1].set_xscale(rax_scale[0])
        axs[r, 1].set_yscale(rax_scale[1])

        # plot the data versus DRAD
        axs[r, 1].plot(x0 / plasma.AU_KM, y0, marker='.', ls='', ms=4, label='all')
        axs[r, 1].plot(x, y, marker='.', ls='', label='sel')
        axs[r, 1].set_xscale(rax_scale[0])
        axs[r, 1].set_yscale(rax_scale[1])

        # put legends and grids
        axs[r, 0].legend()
        axs[r, 1].legend()
        # axs[r, 0].grid(which='major', axis='both')
        # axs[r, 1].grid(which='major', axis='both')

        axs[r, 1].yaxis.set_label_position("right")
        axs[r, 1].yaxis.tick_right()
        axs[r, 1].set_ylabel(hist_xlabel)
        axs[r, 0].set_ylabel('Count')

    axs[r, 0].set_xlabel(hist_xlabel)
    axs[r, 1].set_xlabel('Radial distance D [AU]')

    plt.tight_layout()

    return rad_bins, my_ymed_arr


def div_subsets(x, n_intervals, equal_len_div=True):
    """
    Function to separate input argument *x* in to a number of subsets totalling *n_intervals*. The division is done
    according to method indicated by *equidistant_division*.

    Args:
        x: [ndarray] Array to be divided.
        n_intervals: [int] Number of intervals.
        equal_len_div: [boolean] If *True* then return equal-length subsets; else the subsets are formed by
        dividing in to sets of equal distance in *x*

    Returns:
        subsets: [list] List of arrays, each array containing the indices for the specific subset

    """

    # initialise subsets to be returned
    subsets = []

    if equal_len_div:
        # sort the input array
        x_indices = np.argsort(x)

        # divide in to subsets of equal length
        x_intervals = np.linspace(0, len(x_indices) - 1, n_intervals + 1, dtype=int)

        # for each interval get the appropriate indices, append to *subsets*
        for i in range(len(x_intervals) - 1):
            subsets.append(x_indices[x_intervals[i]:x_intervals[i + 1]])
    else:
        # divide equally within the range of *x*
        x_intervals = np.linspace(np.nanmin(x), np.nanmax(x), n_intervals + 1)

        # for each interval append the appropriate indices
        for i in range(len(x_intervals) - 1):
            subsets.append(np.squeeze(np.where((x >= x_intervals[i]) & (x < x_intervals[i + 1]))))

    return subsets


def plothistbins(x, y, n_intervals=5, n_hist_bins='auto', x_plot=None, equal_len_div=True,
                 xlabel='x', ylabel='y', fig_suptitle='', ax0_xscale='linear', ax0_yscale='linear',
                 y_error_neg=None, y_error_pos=None, figWidth=env.figW, figHeight=env.figH,
                 legend_loc=1, mean_label=None, median_label=None):
    """
    Make a fancy plot showing the division of a data set along with the histogram for each subset.

    Args:
        x: [ndarray] The array containing data to sub-divide the variable *Y* by
        y: [ndarray] The data array (Y-axis of the scatterplot)
        n_intervals: [int] Number of intervals (default 5)
        n_hist_bins: [int] Number of bins in the histogram. If no argument supplied then use 'auto' from numpy.histogram.
         (default value: 'auto')
        x_plot: [ndarray] Parameter to use on the X-axis of the plots. If no argument given, then *x_plot* is set
        to *x*. (default value: None)
        equal_len_div: [boolean] If true then the subset division is done to have equal length subsets. This argument
        is forwarded directly to `div_subsets()`. (default value: True)
        xlabel: [str] Label for the x-axes of the scatter plot (axis 0)
         (default value: empty string)
        ylabel: [str] Label for the y-axis of the scatter plot and the x-axes of the histogram plots
         (axes 1 to *n_intervals*). (default value: empty string)
        fig_suptitle: [str] Label for the super title of the plot. (default value: empty string)
        ax0_xscale: [str] Scale of the x-axis to use for the scatter plot (0-th axis) (default value: 'linear')
        ax0_yscale: [str] Scale of the y-axis to use for the scatter plot (0-th axis) (default value: 'linear')

    Returns:
        y_summary: [dict] Dictionary of summary statistics for each subset / histogram.

    """

    from matplotlib import gridspec

    # initiliase the figure
    fig = plt.figure(figsize=(figWidth, figHeight))
    axs = []

    # intialise the axes using gridspec
    k = 2
    # number of gridspec axis elements to generate
    Nax = k + 2 + 2 * n_intervals
    # generate Nax axes
    gs = gridspec.GridSpec(k + 2 + 2 * n_intervals, 1)
    # append the first axis to the list *axs*
    axs.append(fig.add_subplot(gs[0:k + 1, 0], xscale=ax0_xscale, yscale=ax0_yscale))

    # set axis titles
    axs[0].set_xlabel(xlabel)
    axs[0].set_ylabel(ylabel)
    axs[0].grid(which='major')

    # set j to after the first axis (the scatterplot axis)
    j = k + 2

    # initialise the summary dictionary
    y_summary = {k: [] for k in ['mean', 'median', 'std', 'ix_sel', 'xmin', 'xmax', 'pos_err', 'neg_err']}
    x_summary = {k: [] for k in ['mean', 'median', 'std', 'ix_sel', 'xmin', 'xmax']}

    # list of colours to use ## TODO don't hardcode
    collist = ['C0', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6']

    if x_plot is None:
        x_plot = x

    # build the figure title
    suptitle_drad = ' %s = [%.2f, %.2f]' % (xlabel, np.nanmin(x_plot), np.nanmax(x_plot))
    fig.suptitle(fig_suptitle + suptitle_drad)

    # get the subsets
    ssx = div_subsets(x=x, n_intervals=n_intervals, equal_len_div=equal_len_div)

    # for each subset interval, get the histogram and make the plot
    for i in range(n_intervals):

        if i == 0:
            # make the first axis
            axs.append(fig.add_subplot(gs[j:j + k, 0]))
            j = j + k
        else:
            # make subsequent axes, sharing the x-axis with the first histogram
            axs.append(fig.add_subplot(gs[j:j + k, 0], sharex=axs[i]))
            j = j + k

        # get the selected data
        x_sel = x_plot[ssx[i]]
        y_sel = y[ssx[i]]

        if y_error_pos is not None:
            y_error_pos_sel = np.nanmean(y_error_pos[ssx[i]])
            y_error_neg_sel = np.nanmean(y_error_neg[ssx[i]])

        # number of points in this subset
        N = len(x_sel)

        # get stats of each sub-set of data
        y_summary['mean'].append(np.nanmean(y_sel))
        y_summary['median'].append(np.nanmedian(y_sel))
        y_summary['std'].append(np.std(y_sel[~np.isnan(y_sel)]))
        y_summary['xmin'].append(np.nanmin(x_sel))
        y_summary['xmax'].append(np.nanmax(x_sel))

        if y_error_pos is not None:
            y_error_pos_sel = np.nanmean(y_error_pos[ssx[i]])
            y_error_neg_sel = np.nanmean(y_error_neg[ssx[i]])
            y_summary['pos_err'].append(y_error_pos_sel)
            y_summary['neg_err'].append(y_error_neg_sel)

        # get stats of each sub-set of data
        x_summary['mean'].append(np.nanmean(x_sel))
        x_summary['median'].append(np.nanmedian(x_sel))
        x_summary['std'].append(np.std(x_sel[~np.isnan(x_sel)]))
        x_summary['xmin'].append(np.nanmin(x_sel))
        x_summary['xmax'].append(np.nanmax(x_sel))

        # calculate the histogram, returning bins and edges
        hist, bin_edges = np.histogram(y_sel[~np.isnan(y_sel)], bins=n_hist_bins)

        # get the colour from *collist*
        curr_col = collist[np.mod(i, len(collist))]
        # plot the x vs y scatter on top axis
        axs[0].plot(x_sel, y_sel, marker='o', ls='', color=curr_col, alpha=0.5)

        # plot the histogram on the i-th axis
        axs[i + 1].step(bin_edges, np.append(hist, hist[-1]),
                        where='mid',
                        color=curr_col,
                        label='N=%d\n%s = (%.2f, %.2f)' % (N, xlabel, np.nanmin(x_sel), np.nanmax(x_sel)))
        axs[i + 1].grid(which='major')

        # plot the mean and median
        if mean_label is None:
            mean_label = 'avg = %.2e' % y_summary['mean'][-1]

        if median_label is None:
            median_label = 'med = %.2e' % y_summary['median'][-1]
        axs[i + 1].axvline(y_summary['mean'][-1], ls='--', color=curr_col, label=mean_label)
        axs[i + 1].axvline(y_summary['median'][-1], ls=':', color=curr_col, label=median_label)

        axs[i + 1].legend(loc=legend_loc)
        # add legend

    # put the common histogram y-axis label # TODO deal with hardcoding of values
    fig.text(0.04, 0.40, 'Count [-]', va='center', rotation='vertical', fontsize=12)
    # add x-axis label to the last histogram
    axs[-1].set_xlabel(ylabel)

    # adjust subplot spacing # TODO deal with hardcoding of values
    plt.subplots_adjust(top=0.95,
                        bottom=0.07,
                        left=0.1,
                        right=0.97,
                        hspace=0.0,
                        wspace=0.2)

    return x_summary, y_summary


def plot_figure01(dtime='2020060700', chunk_plot='r', savefig=True):
    """
    First figure of the paper. It's just a depiction of how the break freq is derived from a power spectral density
    versus freq curve. It shows the PSD-FREQ curve, the multiple linear fits, the inertial and steep regions,
    the estimated break freq and error estimates.

    It's basically the same process employed by WICKS etal (YYYY).

    - First the MAG data from the FIELDS instrument is loaded for the appropriate date (note the correct date format).
    - Then the break frequency is estimated by the `breakfreq_lines()` function. This is the implementation of RWICKS
    method. Called by `mag_break_freq_calc()` in the `pspbreak.py` module.
    - Only the estimate of a single interval (or "chunk") is plotted. Set `chunk_plot = N` to plot chunk number N.
    - A new figure window is opened and figure is plotted, and saved if `savefig = True`.

    Args:
        dtime: [str] The date/time to be loaded. Has to be in format YYYYMMDD to correspond to FIELDS data file name.
        chunk_plot: [str / int] Which chunk should be plotted.  Set `chunk_plot=N` to plot chunk number N. Set
        `chunk_plot='r'` to plot a random one. Set `chunk_plot` to some illogical number like -1 then no plot is made.
        savefig: [bool] Option to save figure with generic file name.

    Returns:
        Nothing

    """

    # set timestamp YYYYMMDD
    dt_user = pd.datetime.strptime(dtime, env.input_time_format)
    dt_user_floor = pd.Timestamp(dt_user).floor(env.mag_time_delta)

    # read data
    mag_df = ppp.gettingmagdata(dt_user_floor, fixdatarate=True, savepoint=False)

    # run break freq calculation, including the plot
    break_freq_vars = ppp.mag_break_freq_calc(mag_df, chunk_plot=chunk_plot)

    # set axis labels
    plt.xlabel('Freq [Hz]')
    plt.ylabel('Power [$\mathrm{nT^2.Hz^{-1}}$]')
    plt.legend()
    plt.grid(which='major')

    # save figure with generic file name
    if savefig:
        figname = 'figure01_%s_chunk_%s' % (dtime, str(chunk_plot)) + env.figfile_ext
        figpath = os.path.join(env.image_dir, figname)
        print('Saving figure to ', str(figpath))
        plt.savefig(figpath)

    return


def plot_figure02(data_filename='./outputDF2_2020050900_2020060700.pkl', npoints=2, ix_sel=None):
    """
    Figure 2 of the paper is supposed to be something similar to FIGURE 2 from the paper [@DUAN2020].
    In short, plot the estimated break frequency at various radial distances DRAD. To do this is tedious.

    - Firstly, the output PKL file from the main function `new_work3()` (this module) is loaded. Change the file name
    to load the correct file. The default corresponds to my path and file. The resulting dataframe `delta_t` contains a
    number of variables, including the break frequency `breakfreq` and mean radial distance `mean_rad`.
    - Second step is to throw out all instances where range error or interval error occurs. Range error is where the
    estimated break frequency is beyond the frequencies observed in that interval of MAG data (obviously wrong).
    - Interval error is where estimated break frequency is beyond the inertial range max or dissipation range minimum.
    - Third step. Interpolate radial distance. We can safely interpolate (not extrapolate) because s/c movement is
    regular and predictable.
    - If `ix_sel=None` then make a plot of BREAKFREQ vs DRAD and allow the user to pick `npoints` points. After picking
    the desired number of points, press enter. Be sure to zoom in in order to pick a single point, otherwise more than
    one surrounding point will be selected.
    - The break freq is estimated for every selected point and the estimate plot (like FIG01) is made for each.
    - The final product is a figure with the raw MAG data and the BREAKFREQ estimate for each selected point on the
    same axis.


    Args:
        data_filename: [str] File to be loaded.
        npoints: [int] Number of points to select / plot.
        ix_sel: [list] or `None`. If `None` the user must select points by clicking. If you know the index number of the
        point then input the list of points.

    Returns:
        fb_out: [list] List of dictionaries, one for each selected point. Each dictionary contains the trace of the
        MAG vector (used to get the PSD), the frequencies, timestamp of centre of interval, and all estimated
        quantities (breakfreq, breakfreq error estimates) and error indices (range and interval errors).

    """

    # load previous output DF
    print('loading outputDF')
    df = pd.read_pickle(data_filename)

    # set range an interval errors to NAN
    # df[df.breakfreq_er == 1] = np.nan
    # df[df.breakfreq_ei == 1] = np.nan
    # df.mask(df.breakfreq_eb > 0, inplace=True)
    df.mask(df.breakfreq_ei > 0, inplace=True)
    df.mask(df.breakfreq_er > 0, inplace=True)
    df.mask(df.breakfreq_eb > 0, inplace=True)

    # df.mask((df.breakfreq_m-df.breakfreq_p).abs() > 10, inplace=True)
    # df.mask(df.breakfreq_m.abs() > 5, inplace=True)
    # df.mask(df.breakfreq_p.abs() > 5, inplace=True)

    # interpolate missing values in DRAD
    print('interpolating mean_rad')
    # drad = df.mean_rad.interpolate(limit=10, limit_area='inside').values / plasma.AU_KM
    df['drad_int_au'] = df.mean_rad.interpolate(limit=10, limit_area='inside').values / plasma.AU_KM

    df.dropna(inplace=True)

    drad = df.drad_int_au.values

    # plot the BREAKFREQ estimate versus DRAD using the `ploterrorbar()` function
    if ix_sel is None:
        print('plot FBREAK vs DRAD')
        fig = ploterrorbar(df.breakfreq.values,
                           df.breakfreq_p.values,
                           df.breakfreq_m.values,
                           ix=df.drad_int_au.values,
                           errlimit=5,
                           pickpoints=True)

        # fig, ax = plt.subplots()
        # ax = fig.gca()
        # ax.plot(drad, df.breakfreq_p.values, ls='', marker='.', color='C1', picker=True, pickradius=5)
        # ax.plot(drad, df.breakfreq_m.values, ls='', marker='.', color='C1', picker=True, pickradius=5)
        # ax.fill_between(drad, df.breakfreq_p.values, df.breakfreq_m.values)
        # ax.plot(drad, df.breakfreq.values, ls='', marker='o', color='C2', picker=True, pickradius=5)

        # pick points live
        print('pick %d points' % npoints)

        # pick points off a graph
        ix_sel = test_pickpoints(fig, n=npoints)
        # ix_sel = test_pick2(fig, n=npoints)

        # prompt user to continue
        name = input('Done picking %d points? Press ENTER to continue\n' % npoints)
        print('Indices of selected points: ', ix_sel)

    # initialise FBOUT
    fb_out = list(np.zeros(len(ix_sel)))

    # initialise new figure window
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(env.figW, env.figH))
    ax.set_xscale('log')
    ax.set_yscale('log')

    # initialise list of colors to use in the for-loop
    colslist = ['C0', 'C1', 'C2', 'C3', 'C4']

    for i in range(len(ix_sel)):
        ix = ix_sel[i]

        # get correct timestamp, based on index number
        ix_ts = pd.Timestamp(df.index[ix]).floor(env.mag_time_delta)
        print(ix_ts)

        # load the correct MAG data file
        magdf = ppp.gettingmagdata(dt_user=ix_ts, fixdatarate=True)

        # calculate the time interval corresponding to the selected index
        t0 = df.index[ix] - pd.Timedelta(int(env.chunktime / 2), 'S')
        t1 = df.index[ix + 1] + pd.Timedelta(int(env.chunktime / 2), 'S')

        # get estimate of the break frequency
        fb_out[i] = ppp.mag_break_freq_calc(magdf[t0:t1],
                                            chunk_plot=0)

        # build label for the BREAKFREQ estimate
        fbreak_label = 'R = %.2f AU' % drad[ix]

        # plot the trace of MAG vector versus frequency
        ax.plot(fb_out[i]['freq'][0],
                fb_out[i]['btrace'][0],
                color=colslist[np.mod(i, len(colslist))],
                label=fbreak_label)

        # plot the log-spaced frequency and PSD
        # ax.plot(fb_out[i]['freq_log'][0],
        #         fb_out[i]['Psd_log'][0],
        #         marker='.', ls='-', color='k',
        #         label='log-spaced')

        # plot the BREAKFREQ estimate
        # ax.axvline(fb_out[i]['breakfreq'][0],
        #            ls='--',
        #            lw=2,
        #            # color=colslist[np.mod(i, len(colslist))],
        #            color='k',
        #            label=fbreak_label)

        ax.axvline(fb_out[i]['breakfreq'][0],
                   ls='--',
                   lw=2,
                   # color=colslist[np.mod(i, len(colslist))],
                   color='k')

        # plot the positive (+/p) and negative (-/m) error rangees of the BREAKFREQ estimate
        # ax.axvline(fb_out[i]['breakfreq_em'][0], ls='--', lw=1, color='gray')
        # ax.axvline(fb_out[i]['breakfreq_ep'][0], ls='--', lw=1, color='gray')
        ax.axvspan(fb_out[i]['breakfreq_em'][0],
                   fb_out[i]['breakfreq_ep'][0],
                   color=colslist[np.mod(i, len(colslist))],
                   alpha=0.3)

    # put legend and grid
    ax.set_xlabel('Frequency [Hz]')
    ax.set_ylabel('PSD [$\mathrm{nT^2.Hz^{-1}}$]')
    ax.legend()
    ax.grid()

    return fb_out


def plot_figure03(data_filename='./outputDF2_2020050900_2020060700.pkl', n_radbins=7,
                  figWidth=env.figW, figHeight=env.figH):
    """
    Figure 3 of the paper. Plot the inertial range index and the dissipation range index versus radial distance DRAD.

    - Firstly, the output PKL file from the main function `new_work3()` (this module) is loaded. Change the file name
    to load the correct file. The default corresponds to my path and file. The resulting dataframe `delta_t` contains a
    number of variables, including the break frequency inertial index `inert_ind`, dissipation range index (`steep_ind`)
    and mean radial distance `mean_rad`.
    - Set interval and range errors to NAN (we don't want to plot these)
    - Use the nifty `binbyrad` function to plot the inertial and dissipation range indices versus DRAD. This will
    generate two figures. They are not saved to file - DIY.
    - A summary plot is made with the median of the inertial and dissipation index values for every DRAD bin. Also
    not saved to file.


    Args:
        data_filename: [str] File to be loaded.

    Returns:

    """

    # load data
    df = pd.read_pickle(data_filename)

    # set range an interval errors to NAN
    df.mask(df.breakfreq_er > 0, inplace=True)
    df.mask(df.breakfreq_ei > 0, inplace=True)
    df.mask(df.breakfreq_eb > 0, inplace=True)

    df.dropna(inplace=True)

    # use PLOTHISTBINS to divide the breakscale along mean_rad divisions
    xdat = df.mean_rad.interpolate(limit=5, limit_area='inside').values/plasma.AU_KM
    ydat = df.inert_ind.values
    y_err = df.inert_ind_e.values

    print(xdat.shape)
    print(ydat.shape)

    xsumm, ysumm = plothistbins(y=ydat,
                                x=xdat,
                                y_error_neg=y_err,
                                y_error_pos=y_err,
                                n_intervals=n_radbins,
                                equal_len_div=False)

    inert_ix_means = ysumm['mean'][:]
    inert_ix_medians = ysumm['median'][:]
    dradbins = [i for i in xsumm['xmin']]
    dradbins.append(xsumm['xmax'][-1])
    inert_ix_pm = np.array([ysumm['pos_err'], ysumm['neg_err']])

    ydat = df.steep_ind.values
    y_err = df.steep_ind_e.values

    xsumm, ysumm = plothistbins(y=ydat,
                                x=xdat,
                                y_error_neg=y_err,
                                y_error_pos=y_err,
                                n_intervals=n_radbins,
                                equal_len_div=False)

    diss_ix_means = ysumm['mean'][:]
    diss_ix_medians = ysumm['median'][:]
    dradbins = [i for i in xsumm['xmin']]
    dradbins.append(xsumm['xmax'][-1])
    diss_ix_pm = np.array([ysumm['pos_err'], ysumm['neg_err']])


    # find the centre of the bins
    bin_centres = (dradbins[:-1] + np.diff(dradbins) / 2)

    fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(figWidth, figHeight))

    axs[0].errorbar(bin_centres, inert_ix_means,
                    yerr=inert_ix_pm,
                    fmt='o',
                    color='C0',
                    ecolor='gray',
                    capsize=3,
                    label='',
                    lw=2)

    # axs[0].errorbar(bin_centres+0.003, inert_ix_medians,
    #                 yerr=inert_ix_pm,
    #                 fmt='o',
    #                 color='C1',
    #                 ecolor='C1',
    #                 capsize=3,
    #                 label='',
    #                 lw=2)


    axs[1].errorbar(bin_centres, diss_ix_means,
                    yerr=diss_ix_pm,
                    fmt='o',
                    color='C0',
                    ecolor='gray',
                    capsize=3,
                    label='',
                    lw=2)
    #
    # axs[1].errorbar(bin_centres+0.003, diss_ix_medians,
    #                 yerr=diss_ix_pm,
    #                 fmt='o',
    #                 ecolor='gray',
    #                 capsize=3,
    #                 label='',
    #                 lw=2)

    axs[1].set_xlabel('Radial distance [AU]')
    axs[0].set_ylabel('Inertial range index')
    axs[1].set_ylabel('Dissipation range index')

    axs[0].axhline(-5 / 3, color='k', ls='--', label='Kolmogorov')
    axs[0].axhline(-3 / 2, color='r', ls='--', label='Iroshnikov-Kraichnan')

    # put legend and grid
    axs[0].legend()
    axs[0].grid()
    # axs[1].legend()
    axs[1].grid()

    plt.subplots_adjust(hspace=0.01)
    plt.tight_layout()

    return dradbins, inert_ix_means, diss_ix_means


def plot_figure03a(data_filename='./outputDF2_2020050900_2020060700.pkl', n_radbins=7,
                  figWidth=env.figW, figHeight=env.figH):
    """
    THIS IS SAME AS PLOT_FIGURE03 BUT NOW WITH TURBULENCE AGE ON THE X-AXIS

    Figure 3 of the paper. Plot the inertial range index and the dissipation range index versus radial distance DRAD.

    - Firstly, the output PKL file from the main function `new_work3()` (this module) is loaded. Change the file name
    to load the correct file. The default corresponds to my path and file. The resulting dataframe `delta_t` contains a
    number of variables, including the break frequency inertial index `inert_ind`, dissipation range index (`steep_ind`)
    and mean radial distance `mean_rad`.
    - Set interval and range errors to NAN (we don't want to plot these)
    - Use the nifty `binbyrad` function to plot the inertial and dissipation range indices versus DRAD. This will
    generate two figures. They are not saved to file - DIY.
    - A summary plot is made with the median of the inertial and dissipation index values for every DRAD bin. Also
    not saved to file.


    Args:
        data_filename: [str] File to be loaded.

    Returns:

    """

    # load data
    df = pd.read_pickle(data_filename)

    # set range an interval errors to NAN
    df.mask(df.breakfreq_er > 0, inplace=True)
    df.mask(df.breakfreq_ei > 0, inplace=True)
    df.mask(df.breakfreq_eb > 0, inplace=True)

    df.dropna(inplace=True)

    # use PLOTHISTBINS to divide the breakscale along mean_rad divisions
    # xdat = df.mean_rad.interpolate(limit=5, limit_area='inside').values/plasma.AU_KM
    # xdat = df.mean_rad.interpolate(limit=5, limit_area='inside').values/plasma.AU_KM

    df['mean_rad_int'] = df.mean_rad.interpolate(limit=5, limit_area='inside').values
    print('mean R : %f' % df.mean_rad_int.mean())

    # define XDAT as the AGE
    xdat = (df.mean_rad_int*1000 / df.vsw_mean)/(24*60*60)

    # define YDAT as INERTIAL
    ydat = df.inert_ind.values
    y_err = df.inert_ind_e.values

    print(xdat.shape)
    print(ydat.shape)

    xsumm, ysumm = plothistbins(y=ydat,
                                x=xdat,
                                y_error_neg=y_err,
                                y_error_pos=y_err,
                                n_intervals=n_radbins,
                                equal_len_div=False)

    inert_ix_means = ysumm['mean'][:]
    inert_ix_medians = ysumm['median'][:]
    dradbins = [i for i in xsumm['xmin']]
    dradbins.append(xsumm['xmax'][-1])
    inert_ix_pm = np.array([ysumm['pos_err'], ysumm['neg_err']])

    ydat = df.steep_ind.values
    y_err = df.steep_ind_e.values

    xsumm, ysumm = plothistbins(y=ydat,
                                x=xdat,
                                y_error_neg=y_err,
                                y_error_pos=y_err,
                                n_intervals=n_radbins,
                                equal_len_div=False)

    diss_ix_means = ysumm['mean'][:]
    diss_ix_medians = ysumm['median'][:]
    dradbins = [i for i in xsumm['xmin']]
    dradbins.append(xsumm['xmax'][-1])
    diss_ix_pm = np.array([ysumm['pos_err'], ysumm['neg_err']])


    # find the centre of the bins
    bin_centres = (dradbins[:-1] + np.diff(dradbins) / 2)

    fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(figWidth, figHeight))

    axs[0].errorbar(bin_centres, inert_ix_means,
                    yerr=inert_ix_pm,
                    fmt='o',
                    color='C0',
                    ecolor='gray',
                    capsize=3,
                    label='',
                    lw=2)

    # axs[0].errorbar(bin_centres+0.003, inert_ix_medians,
    #                 yerr=inert_ix_pm,
    #                 fmt='o',
    #                 color='C1',
    #                 ecolor='C1',
    #                 capsize=3,
    #                 label='',
    #                 lw=2)


    axs[1].errorbar(bin_centres, diss_ix_means,
                    yerr=diss_ix_pm,
                    fmt='o',
                    color='C0',
                    ecolor='gray',
                    capsize=3,
                    label='',
                    lw=2)
    #
    # axs[1].errorbar(bin_centres+0.003, diss_ix_medians,
    #                 yerr=diss_ix_pm,
    #                 fmt='o',
    #                 ecolor='gray',
    #                 capsize=3,
    #                 label='',
    #                 lw=2)

    axs[1].set_xlabel('Turbulence Age [day]')
    axs[0].set_ylabel('Inertial range index')
    axs[1].set_ylabel('Dissipation range index')

    axs[0].axhline(-5 / 3, color='k', ls='--', label='Kolmogorov')
    axs[0].axhline(-3 / 2, color='r', ls='--', label='Iroshnikov-Kraichnan')

    # put legend and grid
    axs[0].legend()
    axs[0].grid()
    # axs[1].legend()
    axs[1].grid()

    plt.subplots_adjust(hspace=0.01)
    plt.tight_layout()

    return dradbins, inert_ix_means, diss_ix_means


def plot_figure04(df=None, num_points=5, data_filename='./outputDF2_2020050900_2020060700.pkl', inset_axis=None, legend_location=1):
    """
    Figure 4 of the paper.

    This function creates a plot with estimated breakscale wavenumber versus mean radial distance DRAD.
    The figure is supposed to be similar in format to FIGURE 3A of BRUNO & TRENCHI (2014) paper.

    - load PKL file from the specified file name `data_filename`. This file points to the dataframe produced by the
    function `new_work3()` (this module).
    - Use the nifty `binbyrad` function to plot the breakscale versus DRAD bins. This will generate one figure which
    will not be saved to file - DIY.
    - load the data from the [@BRUNO2014] paper.

    Args:
        df: [pandas.DataFrame or None] Data frame to use. If NONE then file will be loaded
        num_points: [int] Number of radial bins to use
        data_filename: [str] File to be loaded.

    Returns:
        bt3a: [numpy.ndarray] The BRUNO & TRENCHI data.

    """

    from matplotlib.ticker import FormatStrFormatter

    if df is None:
        df = pd.read_pickle(data_filename)
        df[df.breakfreq_er == 1] = np.nan
        df[df.breakfreq_ei == 1] = np.nan

    df.mask(df.breakfreq_eb > 0, inplace=True)
    df.mask(df.breakfreq_er > 0, inplace=True)
    df.mask(df.breakfreq_ei > 0, inplace=True)

    # get BREAKSCALE wavenumber in units of [1/km] to match [@BRUNO2014]
    df['breakscale_k_km'] = df.breakscale_k.values * 1000
    df['breakscale_k_ep_km'] = df.breakscale_k_p.values * 1000
    df['breakscale_k_em_km'] = df.breakscale_k_m.values * 1000

    df.dropna(inplace=True)

    # # use BINBYRAD function to make binned histogram plots of BREAKSCALE versus DRAD
    # dradbins, ymeds, yerr_pm = binbyrad(df,
    #                                     n_radbins=num_points + 1,
    #                                     yvar='breakscale_k_km',
    #                                     yerr_p_var='breakscale_k_ep_km',
    #                                     yerr_m_var='breakscale_k_em_km',
    #                                     drad='mean_rad',
    #                                     hist_xlabel='Bscl [1/km]',
    #                                     rax_scale=['linear', 'log'],
    #                                     fig_suptitle='Breakscale ')

    # use PLOTHISTBINS to divide the breakscale along mean_rad divisions
    xdat = df.mean_rad.interpolate(limit=5, limit_area='inside').values
    ydat = df.breakscale_k_km.values

    xsumm, ysumm = plothistbins(y=ydat,
                                x=xdat/plasma.AU_KM,
                                y_error_neg=df.breakscale_k_em_km.values,
                                y_error_pos=df.breakscale_k_ep_km.values,
                                n_intervals=num_points,
                                equal_len_div=True)

    ymeans = np.array(ysumm['mean'][:])
    ymeds = np.array(ysumm['median'][:])
    dradbins = [i for i in xsumm['xmin']]
    dradbins.append(xsumm['xmax'][-1])
    yerr_pm = [ysumm['pos_err'], ysumm['neg_err']]

    yerr_for_errorbar = np.zeros((2, len(ymeds)))
    yerr_for_errorbar1 = np.zeros((2, len(ymeds)))
    # yerr_for_errorbar[0, :] = np.abs(ymeds - yerr_pm[0])
    # yerr_for_errorbar[1, :] = np.abs(ymeds - yerr_pm[1])
    yerr_for_errorbar[0, :] = np.abs(ymeans - np.array(ysumm['pos_err'][:]))
    yerr_for_errorbar[1, :] = np.abs(ymeans - np.array(ysumm['neg_err'][:]))
    yerr_for_errorbar1[0, :] = np.abs(ymeds - np.array(ysumm['pos_err'][:]))
    yerr_for_errorbar1[1, :] = np.abs(ymeds - np.array(ysumm['neg_err'][:]))

    # load BRUNO & TRENCHI 2014 (FIG 3A) data
    # row 0 is DRAD [AU] and row 1 is BREAKSCALE [1/km]
    bt3a = np.array([[0.42, 0.56, 0.99, 0.99, 0.99, 1.40, 3.20, 5.28],
                     [0.00906E00, 0.00556E00, 0.00415E00, 0.00340E00, 0.00405E00, 0.00214E00, 8.25906E-4, 5.32073E-4]])

    # get bin centres
    drad_centre = dradbins[:-1] + np.diff(dradbins) / 2

    # plot [@BRUNO2014] data
    fig, ax = plt.subplots()
    ax.plot(bt3a[0, :], bt3a[1, :], marker='o', ls='-', color='C0', label='Bruno & Trenchi (2014)', lw=2)
    ax.tick_params(which='both')

    # plot medians of BREAKSCALE versus DRAD in AU with errorbars indicating DRAD bin width
    ax.errorbar(drad_centre,
                ymeds,
                xerr=np.diff(dradbins) / 2,
                yerr=yerr_for_errorbar1,
                fmt='o',
                color='C1',
                ecolor='gray',
                capsize=3,
                label='$\mathrm{k_d}$ median',
                lw=2)

    ax.errorbar(drad_centre,
                ymeans,
                xerr=np.diff(dradbins) / 2,
                yerr=yerr_for_errorbar,
                fmt='o',
                color='C2',
                ecolor='gray',
                capsize=3,
                label='$\mathrm{k_d}$ mean',
                lw=2)

    # use calc_parker_models to get the HMF
    r, B_mag_rad, dens_rad, \
    temp_rad, Omega_p, w_p, Valf, \
    Vth_proton_rad, l_d, p_g, p_ii = calc_parker_models(N=500,
                                                        vsw_rad_km=df.vsw_mean.mean() / 1000,
                                                        rad_min=0.1,
                                                        rad_max=6)

    ax.plot(r, 1 / p_g, linewidth=2, linestyle=':', color='C3', label='Proton gyroscale')
    ax.plot(r, 1 / (p_ii), linewidth=2, linestyle=':', color='C4', label='Proton inertial length')
    ax.plot(r, 1 / l_d, linewidth=2, linestyle=':', color='C5', label='Cyclotron resonance scale')

    # put labels
    plt.title(str(df.index[0]) + ' -- ' + str(df.index[-1]))
    ax.set_xlabel('Radial distance [AU]')
    ax.set_ylabel('Wavenumber [rad/km]')
    ax.legend(fontsize=legend_fontsize, loc=legend_location)
    ax.set_yscale('log')
    ax.set_xscale('log')
    ax.grid(which='major', axis='both')
    # ax.grid(which='major', axis='both')

    # plt.tick_params(axis='y', which='minor')
    # ax.yaxis.set_minor_formatter(FormatStrFormatter("%.0e"))
    # plt.ticklabel_format(axis='y', style='sci', scilimits=(0, 4))

    plt.tick_params(axis='x', which='major')
    plt.tick_params(axis='x', which='minor')
    ax.xaxis.set_major_formatter(FormatStrFormatter("%.1f"))
    ax.xaxis.set_minor_formatter(FormatStrFormatter("%.1f"))
    # plt.ticklabel_format(axis='x', scilimits=(0, 4))

    # plt.tick_params(axis='y', which='minor')
    # ax.yaxis.set_minor_formatter(FormatStrFormatter("%.1f"))

    if inset_axis is False:
        pass
    else:
        if inset_axis is None:
            inset_axis = [0.37, 0.4, 0.55, 0.58]

        axins = ax.inset_axes(inset_axis)
        axins.errorbar(drad_centre,
                       ymeds,
                       xerr=np.diff(dradbins) / 2,
                       yerr=yerr_for_errorbar1,
                       fmt='o',
                       color='C1',
                       ecolor='gray',
                       capsize=3,
                       label='',
                       lw=2)

        axins.errorbar(drad_centre,
                       ymeans,
                       xerr=np.diff(dradbins) / 2,
                       yerr=yerr_for_errorbar,
                       fmt='o',
                       color='C2',
                       ecolor='gray',
                       capsize=3,
                       label='',
                       lw=2)

        axins.plot(bt3a[0, :], bt3a[1, :], marker='o', ls='-', color='C0', lw=2)

        axins.yaxis.set_label_position('right')
        axins.yaxis.tick_right()

        axins.plot(r, 1 / p_g, linewidth=2, linestyle=':', color='green', label='Proton gyroscale')
        # axins.plot(rtemp, 1 / p_g1, linewidth=2, linestyle=':', color='green',
        #            label='Proton gyroscale (Vsw = 322 km/s)')
        axins.plot(r, 1 / p_ii, linewidth=2, linestyle=':', color='blue', label='Proton inertial length')
        # axins.plot(rtemp, 1 / p_ii1, linewidth=2, linestyle=':', color='blue',
        #            label='Proton inertial length (Vsw = 322 km/s)')
        axins.plot(r, 1 / l_d, linewidth=2, linestyle=':', color='magenta', label='Cyclotron resonance scale')
        # axins.plot(rtemp, 1 / l_dtemp, linewidth=2, linestyle=':', color='magenta',
        #            label='Cyclotron resonance scale (Vsw = 322 km/s)')

        axins.set_yscale('log')
        axins.set_xscale('log')
        axins.set_xlim([0.9 * np.min(drad_centre), 1.1 * np.max(drad_centre)])
        axins.set_ylim([0.9 * np.min(ymeds), 1.15 * np.max(ymeds)])

        axins.patch.set_color((0.9, 0.9, 0.9))
        axins.patch.set_alpha(1.0)

        # axins.set_xticklabels([])
        # axins.set_yticklabels([])
        # axins.grid()
        ax.indicate_inset_zoom(axins, edgecolor="black")

    return bt3a


def plot_breakscale_meanrad(data_filename='./outputDF2_2020050700_2020061900.pkl.4'):
    # df = pd.read_pickle('./outputDF2_2020050700_2020051000.pkl')
    df = pd.read_pickle(data_filename)

    # df.mask(df.breakfreq_ei > 0, inplace=True)
    # df.mask(df.breakfreq_er > 0, inplace=True)
    df.mask(df.breakfreq_eb > 0, inplace=True)

    # calc breakscale in KM
    breakscale_km = (1 / df.breakscale_k.values) / 1000.

    # positive error estimate
    breakscale_km_p = (1 / df.breakscale_k_p.values) / 1000.
    # negative error estimate
    breakscale_km_m = (1 / df.breakscale_k_m.values) / 1000.

    # interpolate mean_rad; it's safe to do so because the SC follows predictable path
    drad = df.mean_rad.interpolate(limit=100, limit_area='inside') / plasma.AU_KM

    # make errorbar plots
    ploterrorbar(breakscale_km,
                 xp=breakscale_km_p,
                 xm=breakscale_km_m,
                 ix=drad,
                 errlimit=None,
                 label='Breakscale',
                 datcol='C0')

    r, B_mag, Dens, Temp, Omega_p, w_p, \
    V_A, V_therm_proton, l_d, p_g, p_g1, p_ii, p_ii1, \
    l_dtemp, rtemp = importComp2(rad_min=0.1, rad_max=1.0, df=df, rangeAU=5)

    plt.plot(r, p_g, linewidth=2, linestyle='--', color='C1', label='Proton gyroscale (km)', zorder=99)
    plt.plot(r, p_ii, linewidth=2, linestyle='--', color='C6', label='Proton inertial length (km)', zorder=99)
    plt.plot(r, l_d, linewidth=2, linestyle='--', color='C3', label='Cyclotron resonance scale (km)', zorder=99)

    plt.title(str(df.index[0]) + ' $--$ ' + str(df.index[-1]))
    plt.yscale('log')
    plt.ylabel('Breakscale [km]')
    plt.xlabel('Radial distance [AU]')

    plt.legend(fontsize=legend_fontsize)
    plt.grid(which='major', axis='both')
    plt.xlim([0, 1])

    return 999


def plot_breakwavenumber_meanrad(data_filename='./outputDF2_2020050700_2020061900.pkl.4'):
    # df = pd.read_pickle('./outputDF2_2020050700_2020051000.pkl')

    # load data
    df = pd.read_pickle(data_filename)

    # mask errors in break freq
    df.mask(df.breakfreq_ei > 0, inplace=True)
    df.mask(df.breakfreq_er > 0, inplace=True)
    df.mask(df.breakfreq_eb > 0, inplace=True)

    # interpolate mean_rad; it's safe to do so because the SC follows predictable path
    drad = df.mean_rad.interpolate(limit=100, limit_area='inside') / plasma.AU_KM

    # make errorbar plots
    ploterrorbar(df.breakscale_k.values * 1000,
                 xp=df.breakscale_k_p * 1000,
                 xm=df.breakscale_k_m * 1000,
                 ix=drad,
                 errlimit=5,
                 label='Breakscale wavenumber $\mathrm{k_d}$',
                 datcol='C0')

    # r, B_mag, Dens, Temp, Omega_p, w_p, \
    # V_A, V_therm_proton, l_d, p_g, p_g1, p_ii, p_ii1, \
    # l_dtemp, rtemp = importComp2(rad_min=0.1, rad_max=1.0, df=df, rangeAU=5)

    # use calc_parker_models to get the HMF
    r, B_mag_rad, dens_rad, \
    temp_rad, Omega_p, w_p, Valf, \
    Vth_proton_rad, l_d, p_g, p_ii = calc_parker_models(N=100,
                                                        vsw_rad_km=df.vsw_mean.mean() / 1000,
                                                        rad_min=0.1,
                                                        rad_max=drad.max(),
                                                        B_earth_const=3,
                                                        sol_rot=25.4,
                                                        dens_earth_const=6,
                                                        temp_earth_const=5e4)

    plt.plot(r, 1 / p_g, linewidth=2, linestyle='--', color='C1', label='Proton gyroscale', zorder=99)
    plt.plot(r, 1 / p_ii, linewidth=2, linestyle='--', color='C2', label='Proton inertial length', zorder=99)
    plt.plot(r, 1 / l_d, linewidth=2, linestyle='--', color='C3', label='Cyclotron resonance scale', zorder=99)

    plt.title(str(df.index[0]) + ' $--$ ' + str(df.index[-1]))
    plt.yscale('log')
    plt.ylabel('Wavenumber [$\mathrm{rad.km^{-1}}$]')
    plt.xlabel('Radial distance [AU]')

    plt.legend(fontsize=legend_fontsize)
    plt.grid(which='major', axis='both')
    plt.xlim([0, 1])

    return 999


def power_law(x, a, b):
    return a * np.power(x, -b)


def plot_breakfreq(figHeight=env.figH, figWidth=env.figW):
    from scipy.optimize import curve_fit
    # import matplotlib as mpl

    # font = {'family': 'normal', 'weight': 'normal', 'size': 26}
    # mpl.rc('font', **font)

    data_filename = './outputDF2_2020050700_2020061900.pkl.4'
    # load data
    df = pd.read_pickle(data_filename)

    # set range an interval errors to NAN
    # df[df.breakfreq_ei == 1] = np.nan
    # df[df.breakfreq_er == 1] = np.nan
    df.mask(df.breakfreq_eb > 0, inplace=True)

    # BINBYRAD function to plot the specified number of DRAD ranges of the inertial and dissipation range indices
    dradbins, bfreq_ix_means, stdev = binbyrad(df,
                                               n_radbins=11,
                                               yvar='breakfreq',
                                               drad='mean_rad',
                                               hist_xlabel='inertial index [-]',
                                               yerr_m_var='breakfreq_m',
                                               yerr_p_var='breakfreq_p')

    # find the centre of the bins
    x = (dradbins[:-1] + np.diff(dradbins) / 2) / plasma.AU_KM

    # plot the bin centre versus inertial and diss index
    fig = plt.figure(figsize=(figWidth, figHeight))
    ax = fig.gca()

    yerrp = stdev[0]
    yerrm = stdev[1]

    yerr_mat = np.zeros((2, len(stdev[0])))
    yerr_mat[0, :] = np.abs(bfreq_ix_means - yerrm)
    yerr_mat[1, :] = np.abs(bfreq_ix_means - yerrp)

    popt, pcov = curve_fit(power_law, x, bfreq_ix_means, p0=[1, 1], bounds=[[1e-3, 1e-3], [1e20, 50]])

    ax.errorbar(x,
                bfreq_ix_means,
                yerr=yerr_mat,
                fmt='o',
                color='C1',
                # mfc=(1, 1, 1, 0.6),
                # mfc='w',
                ecolor='gray',
                capsize=5,
                label='Estimated break frequency',
                lw=2,
                zorder=5,
                markersize=8)

    xlinspace = np.linspace(np.min(x), np.max(x), 100)
    duan_fx = popt[0] * xlinspace ** (-1.11)
    duan_fx_m = popt[0] * xlinspace ** (-1.11 - 0.01)
    duan_fx_p = popt[0] * xlinspace ** (-1.11 + 0.01)

    ax.plot(xlinspace, power_law(xlinspace, *popt),
            ls='--',
            color='black',
            # label='y = $%2.2f x^{-%2.2f}$ (fit to PSP Orbit 5)' % (popt[0], popt[1]),
            # label='Exponential fit ($f_b = %2.2f R^{-%2.2f}$)' % (popt[0], popt[1]),
            label='Power law fit',
            lw=2)

    ax.plot(xlinspace, duan_fx, lw=2)
    # ax.fill_between(xlinspace, duan_fx_m, duan_fx_p, alpha=0.5, label='Duan et al. (2020), $y \propto x^{-1.11 \pm 0.01}$')
    ax.fill_between(xlinspace, duan_fx_m, duan_fx_p, alpha=0.5, label='Duan et al. (2020)')

    # put legend and grid
    ax.set_xlabel('Radial distance [AU]')
    ax.set_ylabel('Break Frequency [Hz]')

    handles, labels = ax.get_legend_handles_labels()

    handles = [handles[1], handles[2], handles[0]]
    labels = [labels[1], labels[2], labels[0]]

    ax.legend(handles, labels, fontsize=legend_fontsize)
    ax.grid()

    return 999


def binbyrad2(peri_dat, yvar='breakfreq', drad='mean_rad', n_radbins=5, n_bins=None, fig_suptitle='', hist_xlabel='',
              lax_scale=['linear', 'linear'], rax_scale=['linear', 'linear']):
    """
    Make a fancy plot to show distribution of values (YVAR) binned by radial distance DRAD.


    Args:
        peri_dat: [pandas.DataFrame] Dataframe that contains the YVAR to be plotted and the DRAD to bin against.
        yvar: [str] The string name of the variable to be binned
        drad: [str] The string name of the mean radial distance variable
        n_radbins: [int] Number of DRAD bins to be used / plotted (`n_radbins - 1` bins will be plotted)
        n_bins: [int] Number of bins to create in each histogram. Default behaviour `n_bins=None` results in number of
         bins being determined automatically by `numpy.histogram_bin_edges()`
        fig_suptitle: [str] Title of the main figure
        hist_xlabel: [str] X-axis label of the histograms; this is the full name of YVAR
        lax_scale: [list of str] Scale descriptors of left-hand set of axes, for x- and y-axis. E.g.
         `lax_scale=['linear', 'log'] will have linear scale x-axis and log-scale y-axis.
        rax_scale: [list of str] Scale descriptors of right-hand set of axes, for x- and y-axis. E.g.
         `lax_scale=['linear', 'log'] will have linear scale x-axis and log-scale y-axis.

    Returns:
        rad_bins: [numpy.ndarray] The DRAD bin edges
        ymed_arr: [numpy.ndarray] Array of the median value of each histogram

    """

    # generate the DRAD bins
    rad_bins = np.round(np.linspace(0, len(peri_dat), n_radbins).astype(int))

    # initialise histogram and bin edges lists
    hist_list = []
    bin_edges_list = []

    # initialise YMED_ARR and stdev with NANs
    ymed_arr = np.full_like(rad_bins[1:], fill_value=np.nan)
    stdev = np.full_like(rad_bins[1:], fill_value=np.nan)

    # set up the x and y data values
    x0 = peri_dat[drad].values
    y0 = peri_dat[yvar].values

    # set up the histogram bins
    if n_bins is None:
        allbins = np.histogram_bin_edges(peri_dat[yvar].dropna().values, bins='auto')
    else:
        allbins = np.linspace(peri_dat[yvar].min(), peri_dat[yvar].max(), n_bins)

    # initiliase the figure
    fig, axs = plt.subplots(nrows=n_radbins - 1, ncols=2, sharex='col')

    # build the figure title (adding the DRAD range to user-supplied FIG_SUPTITLE)
    suptitle_drad = ' D = [%.2f, %.2f] AU' % (peri_dat[drad].min() / plasma.AU_KM, peri_dat[drad].max() / plasma.AU_KM)
    fig.suptitle(fig_suptitle + suptitle_drad)

    # add blank axis titles to fix spacing
    axs[0, 0].set_title(' ')
    axs[0, 1].set_title(' ')

    fig1, axs1 = plt.subplots()

    for r in range(len(rad_bins[1:])):
        # get the data to be used on the Y axis from the PERI_DAT dataframe, for this bin
        x = peri_dat[drad].iloc[rad_bins[r]:rad_bins[r + 1]].values
        y = peri_dat[yvar].iloc[rad_bins[r]:rad_bins[r + 1]].values

        # x = x[rad_bins[r]:rad_bins[r + 1]]
        # y = y[rad_bins[r]:rad_bins[r + 1]]

        print('x len: %d' % len(x))
        print('y len: %d' % len(y))

        axs1.plot(y, '.')
        print('y mean %f' % np.mean(y))

        # convert from KM to AU
        x = x / plasma.AU_KM

        # execute the histogram, after dropping missing values
        hist, bin_edges = np.histogram(y, bins=allbins)

        # get the median of the current histogram
        ymed_arr[r] = np.nanmean(y)
        print(ymed_arr[r])

        # calculate standard deviation for each bin
        stdev[r] = np.nanstd(y)

        # add current histogram to list
        hist_list.append(hist)

        # add bin edges to list (and convert to AU)
        bin_edges_list.append(bin_edges / plasma.AU_KM)

        # plot the histogram
        axs[r, 0].step(bin_edges, np.append(hist, hist[-1]), color='C1', where='mid',
                       label='D = (%.2f, %.2f) AU' % (rad_bins[r] / plasma.AU_KM, rad_bins[r + 1] / plasma.AU_KM))

        # plot the median of the histogram, with appropriate label
        axs[r, 0].axvline(ymed_arr[r], ls='--', color='C2', label='median = %.2e' % ymed_arr[r])

        # set axis scales
        axs[r, 0].set_xscale(lax_scale[0])
        axs[r, 0].set_yscale(lax_scale[1])

        axs[r, 1].set_xscale(rax_scale[0])
        axs[r, 1].set_yscale(rax_scale[1])

        # plot the data versus DRAD
        axs[r, 1].plot(x0 / plasma.AU_KM, y0, marker='.', ls='', ms=4, label='all')
        axs[r, 1].plot(x, y, marker='.', ls='', label='sel')
        axs[r, 1].set_xscale(rax_scale[0])
        axs[r, 1].set_yscale(rax_scale[1])

        # put legends and grids
        axs[r, 0].legend()
        axs[r, 1].legend()
        # axs[r, 0].grid(which='major', axis='both')
        # axs[r, 1].grid(which='major', axis='both')

        axs[r, 1].yaxis.set_label_position("right")
        axs[r, 1].yaxis.tick_right()
        axs[r, 1].set_ylabel(hist_xlabel)
        axs[r, 0].set_ylabel('Count')

    axs[r, 0].set_xlabel(hist_xlabel)
    axs[r, 1].set_xlabel('Radial distance D [AU]')

    plt.tight_layout()

    # return rad_bins, ymed_arr, stdev
    return rad_bins, ymed_arr


def read_fld_pklx():
    # dates in YYYYMMDDHH format
    date_start = '2020050700'
    date_end = '2020061900'

    # interpret user-selected start time
    dt_user = pd.datetime.strptime(date_start, env.input_time_format)

    # floor to the applicable time-delta of the data files
    dt_user_floor = pd.Timestamp(dt_user).floor(env.mag_time_delta)

    # interpret user-selected end time
    endtime = pd.datetime.strptime(date_end, env.input_time_format)
    enddate = pd.Timestamp(endtime).floor(env.mag_time_delta)

    # generate a list of dates, based on the time-delta of the data files
    datelist = pd.date_range(dt_user_floor, enddate, freq=env.mag_time_delta)

    # print datelist
    print(str(datelist[0]) + ' -- ' + str(datelist[-1]))

    # maglist = []
    ## make the index
    # new_ix = pd.date_range(dt_user_floor, enddate, freq=env.chunktime_str)
    df_stitched = pd.DataFrame()

    for i in datelist:
        print('user selected time: %s' % i)

        # round to nearest timestamp corresponding to file frequency
        dt_user_floor = pd.Timestamp(i).floor(env.mag_time_delta)

        mag_df = ppp.gettingmagdata(dt_user_floor, fixdatarate=env.mag_fixdatarate)

        # if len(mag_df) == 0:
        if isinstance(mag_df, float):
            print('empty MAG_DF')
            continue
        elif isinstance(mag_df, pd.DataFrame) & len(mag_df) == 0:
            print('empty MAG_DF')
            continue

        # resample
        mag_df_rs = mag_df.resample(env.chunktime_str).mean()
        # print(mag_df)
        # mag_df_rs = ptools.newindex(df=mag_df, ix_new=new_ix)

        # maglist.append(mag_df_rs)
        df_stitched = df_stitched.append(mag_df_rs)

    return df_stitched


def get_spi_daterange():
    # dates in YYYYMMDDHH format
    date_start = '2020050700'
    date_end = '2020061900'
    # date_end = '2020060700'

    # interpret user-selected start time
    dt_user = pd.datetime.strptime(date_start, env.input_time_format)

    # floor to the applicable time-delta of the data files
    dt_user_floor = pd.Timestamp(dt_user).floor(env.mag_time_delta)

    # interpret user-selected end time
    endtime = pd.datetime.strptime(date_end, env.input_time_format)
    enddate = pd.Timestamp(endtime).floor(env.mag_time_delta)

    # generate a list of dates, based on the time-delta of the data files
    datelist = pd.date_range(dt_user_floor, enddate, freq=env.mag_time_delta)

    # print datelist
    print(str(datelist[0]) + ' -- ' + str(datelist[-1]))

    # maglist = []
    df_stitched = pd.DataFrame()

    for i in datelist:
        print('user selected time: %s' % i)

        # round to nearest timestamp corresponding to file frequency
        dt_user_floor = pd.Timestamp(i).floor(env.mag_time_delta)

        spi_df, cdf_path, pkl_path = ppp.gettingdata(dt_user_floor, instr='spc')

        # print(spi_df)

        # if isinstance(spi_df, float):
        #     continue

        # mag_df = ppp.gettingmagdata(dt_user_floor, fixdatarate=env.mag_fixdatarate)
        if len(spi_df) == 0:
            print('empty SPI_DF')
            continue

        # resample
        # mag_df_rs = mag_df.resample(env.chunktime_str).mean()
        spi_df_rs = spi_df.resample(env.chunktime_str).mean()

        # maglist.append(mag_df_rs)
        df_stitched = df_stitched.append(spi_df_rs)

    return df_stitched


def stitch_dataframes(df_list):
    df_stitched = pd.DataFrame()

    for df_i in df_list:
        print('asf')
        df_stitched = df_stitched.append(df_i)

    return df_stitched


def calc_parker_models(N,
                       vsw_rad_km,
                       rad_min,
                       rad_max,
                       B_earth_const=plasma.B_const_earth,
                       sol_rot=plasma.SOLAR_ROTATION_DAYS,
                       dens_earth_const=plasma.N_const_earth,
                       temp_earth_const=plasma.T_const_earth):
    """

    Args:
        N:
        vsw_rad_km:
        rad_min:
        rad_max:
        B_earth_const:
        sol_rot:
        dens_earth_const:
        temp_earth_const:

    Returns:

    """
    import plasma_tools as pt

    print('------------------- XXXXXXXXXXXXXXXXXXX in calc_parker_models --------------------- ')

    # length of VSW array
    # N = len(vsw_rad_km)

    # create radial distance array for VSW
    d_rad = np.linspace(rad_min, rad_max, num=N)

    # convert VSW from km/s to AU / hr
    # vsw_au_hr = vsw_rad_km / pt.AU_KM * (60 * 60)
    ### TODO correctly calc VSW in AU/hr
    vsw_au_hr = 0.001 * vsw_rad_km / pt.AU_KM * (60 * 60)

    # solar rotation rate in /AU
    sol_omega = 2 * np.pi / (sol_rot * 24)

    # Only valid for the equatorial plane # TODO what is sunbeta?
    sun_beta = sol_omega / vsw_au_hr

    # reference value B_0 in nT
    B_0 = B_earth_const / np.sqrt(1 + sun_beta ** 2)
    # B_mag = B_0 / r ** 2 * np.sqrt(1. + r * SunBeta * r * SunBeta)

    # Parker HMF with source surface at 0 AU, convert to Tesla
    # B_mag_rad = B_0 * np.sqrt(1 + (d_rad * sun_beta ** 2)) * d_rad ** (-2)
    B_mag_rad = B_0 * np.sqrt(1 + (d_rad * sun_beta) ** 2) * d_rad ** (-2)
    B_mag_rad_T = B_mag_rad * 1e-9

    # 1/r^2 density decrease, convert from #/cm^3 to #/m^3
    dens_rad = dens_earth_const / d_rad ** 2
    dens_rad_m = dens_rad * 1e6

    # Adiabatic decrease for proton temperature
    temp_rad = temp_earth_const * d_rad ** (-4 / 3)

    # radial proton thermal speed [m/s]
    Vth_proton_rad = pt.thermal_speed(temp=temp_rad, mass=pt.MP_KG)

    # proton gyro frequency [rad/s]
    Omega_p = pt.cyclotron_freq(B=B_mag_rad_T, mass=pt.MP_KG)

    # proton plasma frequency [rad/s]
    w_p = pt.plasma_freq(dens=dens_rad_m, mass=pt.MP_KG)

    # Alven speed [m/s]
    Valf = pt.alfven_speed(B=B_mag_rad_T, mass=pt.MP_KG, dens=dens_rad_m)

    # proton cyclotron resonance wavelength [m]
    print('temp_const = %e' % temp_earth_const)
    kd = pt.cycl_res_wavelen(B=B_mag_rad_T, mass=pt.MP_KG, dens=dens_rad_m, temp=temp_rad)

    # cyclotron resonance scale [1/m]
    l_d = 1 / kd

    # proton gyroscale [m]
    p_g = pt.gyroscale(temp=temp_rad, mass=pt.MP_KG, B=B_mag_rad_T)

    # proton inertial lentgh [m]
    p_ii = pt.inertialscale(B=B_mag_rad_T, mass=pt.MP_KG, dens=dens_rad_m)

    Valf = Valf / 1000
    Vth_proton_rad = Vth_proton_rad / 1000

    l_d = l_d / 1000
    p_g = p_g / 1000
    p_ii = p_ii / 1000

    return d_rad, B_mag_rad, dens_rad, temp_rad, Omega_p, w_p, Valf, Vth_proton_rad, l_d, p_g, p_ii
    # return d_rad, B_mag_rad, dens_rad, temp_rad, Omega_p, w_p, Valf, Vth_proton_rad, l_d, p_g, vsw_au_hr


def importComp2(rad_min, rad_max, df, rangeAU):
    """
    Calculate the modeled values of XXX from PSP measurements

    Args:
        rad_min:
        rad_max:
        df:
        rangeAU:

    Returns:

    """

    # constants

    # avg solar rotation period [days]
    sol_rot = 25.4
    # magnetic field magnitude at Earth in nT
    B_earth_const = 3
    # proton density at earth [#/cc]
    dens_earth_const = 6
    # Temperature at Earth; K
    temp_earth_const = 5.e4

    # calculate the modelled values for certain radial distance extent

    # arrange VSW by MEAN_RAD and get mean per MEAN_RAD bin
    df_rad_vsw = df.groupby('mean_rad', as_index=False)['vsw_mean'].mean().values
    df_drad = df_rad_vsw[:, 0]
    vsw_rad = df_rad_vsw[:, 1]
    # vervang vsw_rad met gemiddelde VSW vir die orbit
    #

    d_rad, B_mag_rad, \
    dens_rad, temp_rad, \
    Omega_p, w_p, Valf, \
    Vth_proton_rad, \
    l_d, p_g, p_ii = calc_parker_models(N=len(vsw_rad),
                                        vsw_rad_km=vsw_rad,
                                        rad_min=rad_min,
                                        rad_max=rad_max,
                                        B_earth_const=B_earth_const,
                                        sol_rot=sol_rot,
                                        dens_earth_const=dens_earth_const,
                                        temp_earth_const=temp_earth_const)

    meannewsw = np.nanmean(vsw_rad)
    N1 = 100

    rtemp, B_mag_rad1, \
    dens_rad1, temp_rad1, \
    Omega_p1, w_p1, Valf1, \
    Vth_proton_rad1, \
    l_dtemp, p_g1, p_ii1 = calc_parker_models(N=N1,
                                              vsw_rad_km=meannewsw,
                                              rad_min=rad_max,
                                              rad_max=rangeAU,
                                              B_earth_const=B_earth_const,
                                              sol_rot=sol_rot,
                                              dens_earth_const=dens_earth_const,
                                              temp_earth_const=temp_earth_const)

    return d_rad, B_mag_rad, dens_rad, \
           temp_rad, Omega_p, w_p, Valf, \
           Vth_proton_rad, l_d, p_g, p_g1, \
           p_ii, p_ii1, l_dtemp, rtemp


def testparker(df, plotflag=False):
    from anel_parker_heliosphere import importComp as nel

    nel_out = nel(rad_min=0.2, rad_max=0.7, df=df, rangeAU=5)

    lotz_out = importComp2(rad_min=0.2, rad_max=0.7, df=df, rangeAU=5)

    if plotflag:
        for i in range(len(lotz_out)):
            plt.figure()
            plt.plot(lotz_out[i], 'o')
            plt.plot(nel_out[i], marker='x', ms=3)
            print('i = %d\tdiff = %e' % (i, np.nansum(np.abs(lotz_out[i] - nel_out[i]))))
    else:
        for i in range(len(lotz_out)):
            print('i = %d\tdiff = %e' % (i, np.nansum(np.abs(lotz_out[i] - nel_out[i]))))

    return lotz_out, nel_out


def changesign(df, start_date='20200607', end_date='20200619', date_fmt_str='%Y%m%d'):
    """
    Change sign of dataframe time index to flip around the perihelion.

    Args:
        df: [pandas dataframe] Input data frame
        date_fmt_str: [str] Start date after perihelion in `date_fmt_str` format
        end_date: [str] End date after perihelion in `date_fmt_str` format
        start_date: [str] Date format

    Returns:
        df: [pandas dataframe] Output data frame
    """

    start = df.index.searchsorted(pd.datetime.strptime(start_date, date_fmt_str))

    end = df.index.searchsorted(pd.datetime.strptime(end_date, date_fmt_str))

    df.mean_rad.iloc[start:end] *= -1

    return df


def plot_summary_components(output_pkl_file='./outputDF2_2020050700_2020061900.pkl.4',
                            fld_pkl_file='./fld_orbit5_128s_avg.pkl',
                            figHeight=env.figH,
                            figWidth=env.figW,
                            legend_loc='best'):
    """
    FOR FIGURE 1 OF THE PAPER

    Read a PKL file with output data and make plot looking like Fig. 2 of Moncuquet etal 2020.

    Args:
        output_pkl_file: [str] Path to the pickle file
        figHeight: [int] Figure height in inch
        figWidth: [int] Figure width in inch
        rlimit_min: [float] Minimum of radial distance between which models shouldn't be plotted
        rlimit_max: [float] Maximum of radial distance between which models shouldn't be plotted
        legend_loc: [str or int] Default legend location, passed to `loc` argument in `matplotlib.pyplot.plot()`

    Returns:
        axs: [matplotlib axes] Set of axes

    """

    # make new figure
    fig1, axs = plt.subplots(nrows=5,
                             ncols=1,
                             sharex=True,
                             figsize=(figWidth, figHeight))

    # read data
    df0 = pd.read_pickle(output_pkl_file)
    df0.mean_rad.interpolate(limit=5, limit_area='inside', inplace=True)

    df = changesign(df0)

    drad = df.mean_rad.mul(1 / plasma.AU_KM)

    bmag = df.modBinst.mul(1e9)

    # get mean VSW from the data
    vsw_mean_km = df.vsw_mean.mean() / 1000
    print('mean VSW = %f' % vsw_mean_km)

    # use calc_parker_models to get the HMF
    r, B_mag_rad, dens_rad, \
    temp_rad, Omega_p, w_p, Valf, \
    Vth_proton_rad, l_d, p_g, p_ii = calc_parker_models(N=100,
                                                        vsw_rad_km=df.vsw_mean.mean() / 1000,
                                                        rad_min=0.1,
                                                        rad_max=drad.max())

    # plot B-field
    axs[0].plot(drad, bmag, ls='', marker='o', color='C0', label='FLD Magnetic field', alpha=0.4)
    axs[0].plot(r, B_mag_rad, linewidth=2, linestyle='', marker='.', markersize=3, color='C3',
                label='Assumed Parker HMF')
    axs[0].plot(-r, B_mag_rad, linewidth=2, linestyle='', marker='.', markersize=3, color='C3')
    axs[0].legend(numpoints=3, loc=legend_loc)
    axs[0].set_yscale('log')
    axs[0].set_ylabel('B [nT]')
    axs[0].grid()

    # plot density
    axs[1].plot(drad, df.meanDensity / 1e6, marker='o', ls='', color='C1', alpha=0.4,
                label='SPI proton density')
    axs[1].plot(r, dens_rad, marker='.', markersize=3, linestyle='', color='C3',
                label='Modelled proton density')
    axs[1].plot(-r, dens_rad, marker='.', markersize=3, linestyle='', color='C3')
    # axs[1].plot(-r, dens_rad, linewidth=2, linestyle='--', color='C1', label='Proton number density (#/cc)')
    axs[1].legend(numpoints=3, loc=legend_loc)
    axs[1].set_yscale('log')
    axs[1].set_ylabel('Density [$\mathrm{cm^{-3}}$]')
    axs[1].grid()

    # plot temperature
    axs[2].plot(drad, df.meanT, marker='o', ls='', color='C2', alpha=0.4, label='SPI Temperature')
    axs[2].plot(r, temp_rad, marker='.', markersize=3, linewidth=2, linestyle='', color='C3',
                label='Modelled temperature')
    axs[2].plot(-r, temp_rad, marker='.', markersize=3, linewidth=2, linestyle='', color='C3')
    axs[2].set_yscale('log')
    axs[2].set_ylabel('Temperature [K]')
    axs[2].grid()
    axs[2].legend(numpoints=3, loc=legend_loc)

    axs[3].plot(drad, df.vsw_mean / 1000, marker='o', ls='', color='C4', alpha=0.4, label='SPC $\mathrm{V_{SW}}$')
    # axs[3].axhline(y=400, xmin=-0.4, xmax=1, c="blue", linewidth=0.5, zorder=0)
    axs[3].axhline(y=vsw_mean_km, color='C3', ls='--', marker='',
                   label='Mean $\mathrm{V_{SW}}$ (%.0f $\mathrm{km.s^{-1}}$)' % vsw_mean_km)
    axs[3].legend(numpoints=3, loc=legend_loc)
    axs[3].set_ylabel('Speed [$\mathrm{km.s^{-1}}$]')
    axs[3].grid()

    fld = pd.read_pickle(fld_pkl_file)
    # remove duplicate indices
    fld = fld[~fld.index.duplicated()]

    # reindex to match df
    fld1 = ptools.newindex(df=fld, ix_new=df.index)
    bx = fld1.BRTN__0
    by = fld1.BRTN__1

    ### CALCULATE SPIRAL ANGLE ###
    spiral = calc_spiral_angle(fld1.BRTN__1, fld1.BRTN__0)
    axs[4].plot(drad, spiral, marker='o', alpha=0.4, ls='')

    axs[-1].set_xlabel('Radial distance [AU]')

    axs[0].set_xlim([1.05 * drad.min(), drad.max() * 1.05])
    plt.tight_layout()
    plt.subplots_adjust(hspace=0.0)

    return axs


def calc_spiral_angle(b_t, b_r):
    """
    Calculate spiral angle

    Args:
        b_t:
        b_r:

    Returns:

    """

    s = np.arctan(-b_t / b_r)

    return s


def histbins_breakscalewavenumber(df_fname='./outputDF2_2020050700_2020061900.pkl.4'):


    df = pd.read_pickle(df_fname)

    # get BREAKSCALE wavenumber in units of [1/km] to match [@BRUNO2014]
    df['breakscale_k_km'] = df.breakscale_k.values * 1000
    df['breakscale_k_ep_km'] = df.breakscale_k_p.values * 1000
    df['breakscale_k_em_km'] = df.breakscale_k_m.values * 1000

    # set X and Y parameters to be plotted
    # X is radial distance [AU]
    xpar = df.mean_rad / plasma.AU_KM
    # Y is breakscale wavenumber [rad/km]
    ypar = df.breakscale_k_km

    # label for the Y parameter
    ypar_label = 'Wavenumber $\mathrm{k_d}$ [$\mathrm{rad.km^{-1}}$]'

    x, y = plothistbins(x=xpar,
                        y=ypar,
                        xlabel='R [AU]',
                        ylabel=ypar_label,
                        ax0_yscale='log',
                        equal_len_div=False,
                        figWidth=9,
                        figHeight=9,
                        legend_loc=1,
                        mean_label='',
                        median_label='')

    return 999


def histbins_dissipation_index(df_fname='./outputDF2_2020050700_2020061900.pkl.4'):

    df = pd.read_pickle(df_fname)

    # get BREAKSCALE wavenumber in units of [1/km] to match [@BRUNO2014]
    df['breakscale_k_km'] = df.breakscale_k.values * 1000
    df['breakscale_k_ep_km'] = df.breakscale_k_p.values * 1000
    df['breakscale_k_em_km'] = df.breakscale_k_m.values * 1000

    # df.mask(df.inert_ind_e.abs() > 1e2, inplace=True)
    df.mask(df.steep_ind_e.abs() > 1e3, inplace=True)
    df.dropna(inplace=True)

    xpar = df.mean_rad / plasma.AU_KM
    # ypar = df.breakscale_k_km
    # ypar = df.inert_ind
    ypar = df.steep_ind.values

    xpar_error = []
    # ypar_error = [df.breakscale_k_em_km, df.breakscale_k_ep_km]
    # ypar_error = [df.inert_ind_e, df.inert_ind_e]
    ypar_error = [df.steep_ind_e, df.steep_ind_e]

    # ypar_label = 'Wavenumber $\mathrm{k_d}$ [$\mathrm{rad.km^{-1}}$]'
    # ypar_label = 'Inertial Index'
    ypar_label = 'Dissipation Range Index'

    x, y = plothistbins(x=xpar,
                        y=ypar,
                        xlabel='R [AU]',
                        ylabel=ypar_label,
                        ax0_yscale='linear',
                        equal_len_div=False,
                        figWidth=9,
                        figHeight=9,
                        legend_loc=2,
                        mean_label='',
                        median_label='')

    return 999


def histbins_inertial_index(df_fname='./outputDF2_2020050700_2020061900.pkl.4'):

    df = pd.read_pickle(df_fname)

    # get BREAKSCALE wavenumber in units of [1/km] to match [@BRUNO2014]
    df['breakscale_k_km'] = df.breakscale_k.values * 1000
    df['breakscale_k_ep_km'] = df.breakscale_k_p.values * 1000
    df['breakscale_k_em_km'] = df.breakscale_k_m.values * 1000

    df.mask(df.inert_ind_e.abs() > 1e2, inplace=True)
    # df.mask(df.steep_ind_e.abs() > 1e3, inplace=True)
    # df.dropna(inplace=True)

    xpar = df.mean_rad / plasma.AU_KM
    # ypar = df.breakscale_k_km
    ypar = df.inert_ind
    # ypar = df.steep_ind.values

    xpar_error = []
    # ypar_error = [df.breakscale_k_em_km, df.breakscale_k_ep_km]
    # ypar_error = [df.inert_ind_e, df.inert_ind_e]
    # ypar_error = [df.steep_ind_e, df.steep_ind_e]

    # ypar_label = 'Wavenumber $\mathrm{k_d}$ [$\mathrm{rad.km^{-1}}$]'
    ypar_label = 'Inertial Index'
    # ypar_label = 'Dissipation Range Index'

    x, y = plothistbins(x=xpar,
                        y=ypar,
                        xlabel='R [AU]',
                        ylabel=ypar_label,
                        ax0_yscale='linear',
                        equal_len_div=False,
                        figWidth=9,
                        figHeight=9,
                        legend_loc=1,
                        mean_label='',
                        median_label='')

    return 999


def test_plothist(df_fname='./outputDF2_2020050700_2020061900.pkl.4'):
    df = pd.read_pickle(df_fname)

    # get BREAKSCALE wavenumber in units of [1/km] to match [@BRUNO2014]
    df['breakscale_k_km'] = df.breakscale_k.values * 1000
    df['breakscale_k_ep_km'] = df.breakscale_k_p.values * 1000
    df['breakscale_k_em_km'] = df.breakscale_k_m.values * 1000

    df.mask(df.inert_ind_e.abs() > 1e2, inplace=True)
    df.mask(df.steep_ind_e.abs() > 1e3, inplace=True)

    # x, y, n_intervals = 5, n_hist_bins = 'auto', x_plot = None, equal_len_div = True,
    # xlabel = 'x', ylabel = 'y', fig_suptitle = '', ax0_xscale = 'linear', ax0_yscale = 'linear',
    # y_error_neg = None, y_error_pos = None):

    xpar = df.mean_rad / plasma.AU_KM
    # ypar = df.breakscale_k_km
    # ypar = df.inert_ind
    ypar = df.steep_ind

    xpar_error = []
    # ypar_error = [df.breakscale_k_em_km, df.breakscale_k_ep_km]
    # ypar_error = [df.inert_ind_e, df.inert_ind_e]
    ypar_error = [df.steep_ind_e, df.steep_ind_e]

    # ypar_label = 'Wavenumber $\mathrm{k_d}$ [$\mathrm{rad.km^{-1}}$]'
    # ypar_label = 'Inertial Index'
    ypar_label = 'Dissipation Range Index'

    x, y = plothistbins(x=xpar,
                        y=ypar,
                        y_error_neg=ypar_error[0],
                        y_error_pos=ypar_error[1],
                        xlabel='R [AU]',
                        ylabel=ypar_label,
                        ax0_yscale='log',
                        equal_len_div=False,
                        figWidth=9,
                        figHeight=9,
                        legend_loc=1,
                        mean_label='',
                        median_label='')

    return 999
