"""
This module is for the analysis of solar wind plasma and magnetic field data recorded by instruments aboard \
the Parker Solar Probe (<http://parkersolarprobe.jhuapl.edu/>).

This file contains the main functions in the module that deal with the processing and plotting of raw data and derived \
paramaters.

Accompanying this module are:

- `env.py` contains environment variables to be set by the user
- `psp_tools.py` contain helper functions called by the functions in this file
- `plasma_tools.py` contains physical constants and calculations of plasma quantities
- `testing.py` contain higher level analysis and plotting functions and some unfinished stuff
- `plot_all_figures.py` functions to plot the figures published in the paper

Lotz, Nel, Wicks etal.

<https://bitbucket.org/stefansansa/pspanalysis/>

"""

# IMPORTS
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import env
import datetime as dt
import psp_tools as ttt
import plasma_tools as plasma


# TODO remove `main` completely?
def main():
    """
    This function reads user input from the environment file `env.py` and calls functions to download, read and
    process data from PSP.

    Returns:
        brk_frq_vars: [pandas DataFrame] Dataframe with break frequency related variables
        mag_df: [pandas DataFrame] Magnetic field data from the FIELDS instrument from `read_mag_data()` function

    """

    # interpret user-selected time
    dt_user = pd.datetime.strptime(env.time_selected, env.input_time_format)
    ttt.printmessage('user selected time: %s' % dt_user)

    # round to nearest timestamp corresponding to file frequency
    dt_user_floor = pd.Timestamp(dt_user).floor(env.mag_time_delta)
    ttt.printmessage('floored user time: %s' % dt_user_floor)

    # initialise empty dataframes for return variables
    spi_df = pd.DataFrame()
    spc_df = pd.DataFrame()
    mag_df = pd.DataFrame()

    # interpret the work to be done
    # # set local versions of flags, since they may be forced to change
    len_scale_flag = env.calc_len_scale_flag
    brk_freq_flag = env.calc_break_freq_flag

    # initialise
    lensclvars = env.ERRORVAL

    if len_scale_flag:
        # we need brk freq to calc len scale
        brk_freq_flag = True

    if brk_freq_flag:
        ttt.printmessage('Now going to calc break freq')
        mag_df, brk_frq_vars = break_freq_download_calc(dt_user_floor)
        spectraplot(brk_frq_vars)

    if len_scale_flag:
        ttt.printmessage('Now going to calc len scale')
        lensclvars = len_scale_download_calc(dt_user_floor, brk_frq_vars=brk_frq_vars)

    return brk_frq_vars, mag_df


def break_freq_download_calc(dt_user):
    """
    Download and load magnetic field data (CDF file from  FIELDS experiment) and calculate the break frequency
    in intervals by calling `mag_break_freq_calc()`.

    Args:
        dt_user: [str] Date-time string supplied by the user. Expected format is YYYYMMDDHH.

    Returns:
        brk_frq_vars: [pandas DataFrame] Dataframe with break frequency related variables
        mag_df: [pandas DataFrame] Magnetic field data from the FIELDS instrument from `read_mag_data()` function
    """

    mag_df = gettingmagdata(dt_user, fixdatarate=env.mag_fixdatarate)
    break_freq_vars = env.ERRORVAL

    try:
        if isinstance(mag_df, type(env.ERRORVAL)):
            raise ValueError('Data not loaded')
        else:
            # calculate break freq
            # print(type(mag_df))
            print('xxxx ---- mag_df shape ', mag_df.dropna().shape)
            print('xxxx ---- mag_df shape ', mag_df.index.freq)
            break_freq_vars = mag_break_freq_calc(mag_df)

    except ValueError as err:
        print(err)

    return mag_df, break_freq_vars


def fix_data_rate(df, r, L, floor_index=False, floor_dt='1s', plotflag=False, interpmethod='linear',
                  dataratevarname='rate'):
    """
    Fix the data rate of dataframe *df* by reindexing by interpolation method *interpmethod* to a constant cadence
    calculated from the data rate *r* and the sample cycle duration *L*.

    Args:
        df: [pandas DataFrame] the dataframe
        r: [pandas Series] data rate time series
        L: [float] duration of sample cycle in seconds
        floor_index: [bool] Whether or not the index start and end should be clipped (or floored)
        floor_dt: [str] Datetime offset string to specify how timeseries should be floored
        plotflag: [bool] Whether or not to make a summary plot of the difference between the original and fixed\
         dataframe
        interpmethod: [str] Interpolation string to specify the interpolation method used (e.g. 'linear', 'cubic',\
        and other strings accepted by pandas.DataFrame.interpolate() function)

        dataratevarname: [str] Name of the variable representing data rate of the input dataframe *df*

    Returns:
        df_new: the new reindexed dataframe
    """

    ttt.printmessage('Fixing the datarate... ')
    print('xxxx ---- df index freq', df.index.freq)

    # test if the DF has any data inside
    if df.dropna().shape[0] == 0:
        ttt.printmessage('WARNING: Input DF is EMPTY. Returning as-is.')
        return df

    # fill missing values in the rate by last and next valid values
    r.fillna(method='ffill', inplace=True)
    r.fillna(method='bfill', inplace=True)
    r2 = r.copy()

    # cap at maximum if the filling cause problems
    r[r < r2.max()] = r2.max()

    # find where data rate change
    jdelta = np.where(r.diff() != 0)[0]

    # add end of the dataframe to the vector
    jdelta = np.append(jdelta, np.array(r.shape[0], dtype=np.int))

    # remove any duplicates (could happen if last index has change in data rate)
    jdelta = np.unique(jdelta)

    # number of distinct datarate sections in df
    Ndelta = len(jdelta)
    print('xxxx ----- NDELTA = %d' % Ndelta)

    # intialise new empty dataframe to be returned
    df_new = pd.DataFrame()

    # for each change in datarate
    for j in range(Ndelta - 1):

        # get the relevant part of the dataframe
        df_part = df[jdelta[j]:jdelta[j + 1]]

        # get the data rate of the relevant part
        df_part_rate = r[jdelta[j]]

        # if data rate is invalid for this part, then skip this iteration of the loop
        if df_part_rate == -1:
            continue

        # assign new delta_t (i.e. new cadence for this part of the dataframe), scaling by *L*
        new_dt = L / df_part_rate

        # create frequency offset string (in microseconds)
        freq_str = str(int(new_dt * 1e6)) + 'U'
        print('xxxx ----- set freq ')

        # build new time index
        if floor_index:
            new_ix = pd.date_range(df_part.index[0].floor(floor_dt),
                                   df_part.index[-1].floor(floor_dt),
                                   freq=freq_str)
        else:
            new_ix = pd.date_range(df_part.index[0], df_part.index[-1], freq=freq_str)

        print('xxxx ---- NEW_IX FREQ ', new_ix.freq)

        # reindex: create an empty dataframe with the correct time index
        df_part_new = pd.DataFrame(index=new_ix)
        # print('freq type: ', df_part_new.index.freq)

        # use for loop to do reindexing per variable; this is quicker than reindexing entire dataframe
        for k in df_part.columns:
            # reindex only the current variable corresponding to key k
            data_k = ttt.newindex(df=df_part[k], ix_new=new_ix, interp_method=interpmethod)

            # add the reindexed variable to the initially empty dataframe
            df_part_new[k] = data_k.values

        # handle the data rate separately (i.e. don't interpolate with interpmethod, but 'nearest')
        df_part_new[dataratevarname] = ttt.newindex(df=df_part[dataratevarname],
                                                    ix_new=new_ix,
                                                    interp_method='nearest')

        # add the current part to the dataframe to be returned
        df_new = pd.concat([df_new, df_part_new])

    # get rid of NA
    df_new = df_new.fillna(limit=env.fillna_limit, method='pad')
    # get rid of NA - backfill removes NA at start of series
    df_new = df_new.fillna(limit=env.fillna_limit, method='bfill')

    # make a summary plot of the difference between the original and fixed *df*
    if plotflag:
        ttt.plot_fixrate(df, df_new, L, r)

    return df_new


def calc_lengthscale(Break_Freq, MeanDensity, MeanVx, MeanVy, MeanVz, MeanModBSC, MeanModBINST, MeanT, MeanTperp,
                     MeanTpar, MeanRad, Break_Freq_m, Break_Freq_p):
    """
    TODO: write proper docstring
    Calculate and return various length-scale related quantities from the set of inputs.

    Args:
        Break_Freq: Set of break frequency estimates
        MeanDensity: Density in [?]
        MeanVx: Mean solar wind speed (first component, typically R in RTN)
        MeanVy: Mean solar wind speed (second component, typically T in RTN)
        MeanVz: Mean solar wind speed (third component, typically N in RTN)
        MeanModBSC: Mean B-field magnitude in S/C frame
        MeanModBINST: Mean B-field magnitude in INSTRUMENT frame
        MeanT: Temperature magnitude
        MeanTperp: Perpendicular component of temperature
        MeanTpar: Parallel component of temperature
        MeanRad: Radial distance from the sun (S/C location)
        Break_Freq_m: "minus" error in the break frequency estimate
        Break_Freq_p: "plus" error in the break frequency estimate

    Returns:
        MeanRad:
        Proton_Gyrofreq:
        MeanVth:
        Alfven_Speed:
        Proton_Inertial_Length:
        Proton_Gyroscale:
        MeanModBINST:
        MeanModBSC:
        MeanDensity:
        MeanT:
        Proton_Kc:
        Break_Scale_k:
        w_p:
        MeanTperp:
        MeanTpar:
        Break_Scale_k_m:
        Break_Scale_k_p:
        MeanModV:

    """

    # Convert break frequency into an angular frequency:
    Break_Omega = 2 * np.pi * Break_Freq

    # units should now be m^(-3)
    MeanDensity = MeanDensity * 1000000

    # units should now be m/s
    MeanModV = np.sqrt(MeanVx ** 2 + MeanVy ** 2 + MeanVz ** 2) * 1000
    MeanVthPerp = np.sqrt(MeanTperp * plasma.KB / plasma.MP_KG)
    MeanVthPar = np.sqrt(MeanTpar * plasma.KB / plasma.MP_KG)

    # calculate plasma parameters
    # B field now in units of T
    MeanModBINST = MeanModBINST * 1.e-9
    MeanModBSC = MeanModBSC * 1.e-9

    Proton_Gyrofreq = plasma.E_C * MeanModBINST / plasma.MP_KG
    # w_p = np.sqrt(MeanDensity * plasma.E_C * plasma.E_C / plasma.MP_KG / plasma.E0)
    w_p = plasma.plasma_freq(dens=MeanDensity, mass=plasma.MP_KG)

    # in m/s
    Alfven_Speed = plasma.C_MS * Proton_Gyrofreq / w_p
    MeanVth = np.sqrt(3. * MeanT * plasma.KB / plasma.MP_KG)

    # calculate proton scales
    Break_Scale_L = MeanModV / Break_Freq
    Proton_Kc = Proton_Gyrofreq / (Alfven_Speed + MeanVth)

    Proton_Gyroscale = MeanVth / Proton_Gyrofreq
    Proton_Inertial_Length = Alfven_Speed / Proton_Gyrofreq

    Break_Scale_k = Break_Omega / MeanModV

    Break_Omega_m = 2 * np.pi * Break_Freq_m
    Break_Omega_p = 2 * np.pi * Break_Freq_p
    Break_Scale_k_m = Break_Omega_m / MeanModV
    Break_Scale_k_p = Break_Omega_p / MeanModV

    # TODO refine the way the return dataframe is created
    # very roundabout way to ensure that a dataframe of variables are returned instead of a list / dict
    returnlist = (MeanRad, Proton_Gyrofreq, MeanVth, Alfven_Speed, Proton_Inertial_Length, Proton_Gyroscale,
                  MeanModBINST, MeanModBSC, MeanDensity, MeanT, Proton_Kc, Break_Scale_k, w_p, MeanTperp, MeanTpar,
                  Break_Scale_k_m, Break_Scale_k_p, MeanModV)

    N = len(MeanRad)
    K = len(returnlist)

    returnarray = np.zeros((N, K))

    for i in range(K):
        returnarray[:, i] = returnlist[i]

    df = pd.DataFrame(data=returnarray,
                      columns=['MeanRad', 'Proton_Gyrofreq', 'MeanVth', 'Alfven_Speed',
                               'Proton_Inertial_Length', 'Proton_Gyroscale', 'MeanModBINST',
                               'MeanModBSC', 'MeanDensity', 'MeanT', 'Proton_Kc', 'Break_Scale_k',
                               'w_p', 'MeanTperp', 'MeanTpar', 'Break_Scale_k_m', 'Break_Scale_k_p', 'MeanModV'])

    return df


def len_scale_download_calc(dt_user, brk_frq_vars):
    """
    Download all data and make calculations to estimate length scale for the
    given time range.

    Args:
        dt_user: [pandas datetime] User specified date
        brk_frq_vars: [pandas DataFrame] Break frequency timeseries calculated
        by `mag_break_freq_calc()`.

    Returns:

    """

    # download and/or load data
    spi_df, spi_cdf_path, spi_pkl_path = gettingdata(dt_user=dt_user, instr='spi')
    spc_df, spc_cdf_path, spc_pkl_path = gettingdata(dt_user=dt_user, instr='spc')

    # if (spi_df == env.ERRORVAL) | (spc_df == env.ERRORVAL):
    #     print(' ------- SPC OR SPI IS EMPTY RETURNING ERROVAL')
    #     return env.ERRORVAL

    if spi_df.empty | spc_df.empty:
        print(' ------- SPC OR SPI IS EMPTY RETURNING ERROVAL')
        return env.ERRORVAL

    # save data to file at this point
    ttt.printmessage('---> SPC SAVEPOINT <---- ')
    ttt.savepickle(spc_pkl_path, varslist=spc_df)

    # calculate magnitude of B-field
    spi_df['ModBSC'] = (spi_df['modbSC__0'] ** 2 + spi_df['modbSC__1'] ** 2 + spi_df['modbSC__2'] ** 2) ** 0.5
    spi_df['ModBINST'] = (spi_df['modbINST__0'] ** 2 + spi_df['modbINST__1'] ** 2 + spi_df['modbINST__2'] ** 2) ** 0.5

    spi_mag_sc = [spi_df['modbSC__0'].values, spi_df['modbSC__1'].values, spi_df['modbSC__2'].values]
    spi_mag_inst = [spi_df['modbINST__0'].values, spi_df['modbINST__1'].values, spi_df['modbINST__2'].values]

    # TODO wouldn't TEMP as an array be better option instead of three separate components? [@SLOTZ to do if nec.]
    #  note that newer SPI data files have 3-dimensional temp, while older files (<= perihelion 5) have 1-d temp
    if env.spi_temp_varname + '__0' in list(spi_df.columns):
        spi_df[env.spi_temp_varname] = (spi_df[env.spi_temp_varname + '__0'] ** 2 +
                                        spi_df[env.spi_temp_varname + '__1'] ** 2 +
                                        spi_df[env.spi_temp_varname + '__2'] ** 2) ** 0.5

    # save SPI data to file at this point
    ttt.printmessage('---> SPI SAVEPOINT <----')
    ttt.savepickle(spi_pkl_path, varslist=spi_df)

    # get variables in order to calc lengthscale
    # make new dataframe for breakfreq data
    breakfreq_df = pd.DataFrame(index=brk_frq_vars['spectime'][:], data=brk_frq_vars['breakfreq'][:], columns=['fb'])

    # incorporate the erros in breakfreq dataframe
    breakfreq_df['fbp'] = brk_frq_vars['breakfreq_ep']
    breakfreq_df['fbm'] = brk_frq_vars['breakfreq_em']
    breakfreq_df['fbr'] = brk_frq_vars['breakfreq_er']

    # get overlapping time range by finding maximum overlap between two timeseries
    t_overlap_start = np.max([breakfreq_df.index[0], spc_df.index[0], spi_df.index[0]]).ceil('1s')
    t_overlap_end = np.min([breakfreq_df.index[-1], spc_df.index[-1], spi_df.index[-1]]).floor('1s')

    ttt.printmessage('Maximum overlap between MAG and SPI / SPC data:')
    ttt.printmessage('From \t' + str(t_overlap_start))
    ttt.printmessage('Until\t' + str(t_overlap_end))

    # build new index covering the overlap interval at *chunktime* cadence
    ix_new = pd.date_range(t_overlap_start, t_overlap_end, freq=env.chunktime_str)

    # re-index each dataframe (spi, spc, breakfreq)
    spi_df_res = ttt.newindex(spi_df, ix_new)
    spc_df_res = ttt.newindex(spc_df, ix_new)
    breakfreq_df_res = ttt.newindex(breakfreq_df, ix_new)

    # Break_Freq = breakfreq_df_res.values
    Break_Freq = breakfreq_df_res.fb.values

    # incorporate break freq errors here
    Break_Freq_p = breakfreq_df_res.fbp.values
    Break_Freq_m = breakfreq_df_res.fbm.values
    Break_Freq_r = breakfreq_df_res.fbr.values

    # out of range error means break freq estimate is beyond frequencies observed in the signal
    ix_bf_outofrange = np.squeeze(np.where(Break_Freq_r > 0))
    Break_Freq[ix_bf_outofrange] = np.nan
    Break_Freq_p[ix_bf_outofrange] = np.nan
    Break_Freq_m[ix_bf_outofrange] = np.nan

    # Primary proton population density, from 1-dimensional Maxwellian fitting. cm^(-3)
    MeanDensity = spc_df_res.np1f.values
    MeanVx = spc_df_res.vp1f_rtn__0.values
    MeanVy = spc_df_res.vp1f_rtn__1.values
    MeanVz = spc_df_res.vp1f_rtn__2.values
    MeanModBSC = spi_df_res.ModBSC.values
    MeanModBINST = spi_df_res.ModBINST.values

    MeanT = spi_df_res.temp.values
    # convert from eV to Kelvin
    MeanT = MeanT / plasma.EV_PER_KELVIN

    PosX = spc_df_res.sat_pos__0.values
    PosY = spc_df_res.sat_pos__1.values
    PosZ = spc_df_res.sat_pos__2.values

    MeanRad = (PosX ** 2 + PosY ** 2 + PosZ ** 2) ** 0.5

    # TODO use same values for Tperp and Tpar since this part not done yet
    MeanTpar = spi_df_res.temp.values
    # convert from eV to Kelvin
    MeanTpar = MeanTpar / plasma.EV_PER_KELVIN

    MeanTperp = spi_df_res.temp.values
    # convert from eV to Kelvin
    MeanTperp = MeanTperp / plasma.EV_PER_KELVIN

    # run calc_lengthscale, returning a dataframe with the necessary parameters
    df = calc_lengthscale(Break_Freq, MeanDensity, MeanVx, MeanVy, MeanVz, MeanModBSC,
                          MeanModBINST, MeanT, MeanTpar, MeanTperp, MeanRad, Break_Freq_m, Break_Freq_p)

    # add paramaters to the DF that are external to calc_lengthscale()
    df['Spectime'] = ix_new
    df['Break_Freq'] = Break_Freq
    df['Break_Freq_m'] = Break_Freq_m
    df['Break_Freq_p'] = Break_Freq_p

    return df


def read_mag_data(mag_data_cdf, varnames_list=None):
    """
    Read magnetometer instrument data from PSP CDF data format in to PANDAS DataFrame.
    Only certain parameters are selected and renamed according to the parameters in ENV file. Make your changes there.

    Args:
        mag_data_cdf: path to file name [str]
        N0: starting index [int]
        N1: end index [int]
        varnames_list: [list of strings] List of strings to specify the names of the MAG variables. If default (`None`)
         then the list is read from `env.py`

    Returns:
        df: Pandas dataframe with data inside
    """

    if varnames_list is None:
        print('setting varnames_list to *env.mag_cdf_varnames*')
        varnames_list = env.mag_cdf_varnames

    df = ttt.callingcdf(mag_data_cdf,
                        index_col=env.mag_data_index_col,
                        cdf_vars=env.mag_cdf_vars,
                        varnames_list=varnames_list,
                        missing_value=env.mag_errorval)

    try:
        if df.size == 0:
            raise RuntimeError('ERROR: empty DF')
    except RuntimeError as err:
        print(err)
        return df

    # read and apply the quality flag to the current dataframe
    # # ... because the MAG data quality flag is not at same time cadence as the rest of the data
    df = mag_qualflag(df,
                      mag_data_cdf,
                      qflag_ix=env.mag_qflag_index,
                      qflag_var=env.mag_qflag_var,
                      qflagname=env.mag_qflag_name)

    # mask bad quality data with NAN
    df = ttt.mask_bad_quality(df, qual_flag=env.mag_qflag_name, qual_flag_max=env.mag_qflag_max)

    try:
        if df.dropna().shape[0] == 0:
            raise RuntimeError('ERROR: empty DF')
    except RuntimeError as err:
        print(err)
        return env.ERRORVAL

    ttt.printmessage('Shape of MAG dataframe: [%d, %d]' % (df.shape[0],
                                                           df.shape[1]))

    return df


def mag_qualflag(df, cdfdat, qflag_ix, qflag_var, qflagname='qualflag'):
    """

    Args:
        df: data frame
        cdfdat: the cdf data
        qflag_ix: CDF variable to use for qflag index
        qflag_var: CDF variable to use for qflag
        qflagname:

    Returns:
        df2: adjusted *df* that now has quality flag

    """
    # make dataframe specifically for quality flag since it has different
    # sampling time (only applies to MAG instrument)s
    df2 = ttt.callingcdf(cdfdat,
                         index_col=qflag_ix,
                         varnames_list=[qflagname],
                         cdf_vars=[qflag_var])

    # populate the MAG dataframe with quality flag
    # initialise
    df[qflagname] = np.nan

    # for every quality flag value
    for i in range(1, df2[qflagname].size):
        # repeat the values from df2
        df.loc[df2.index[i - 1]:df2.index[i], qflagname] = df2[qflagname].iloc[i - 1]

        if i == df2[qflagname].size:
            # last interval (not covered by for-loop indexing)
            df.loc[df2.index[i]:, qflagname] = df2[qflagname].iloc[i]

    print('Shape of MAG dataframe: [%d, %d]' % (df.shape[0], df.shape[1]))

    return df


def read_sp_data(spi_cdf, instr='spi'):
    """
    Read SPI / SPC instrument data from PSP CDF data format in to PANDAS
    DataFrame. Only certain parameters are selected and renamed according to
    the parameters in ENV file. Make your changes there.

    Args:
        spi_cdf: path to file name [str]
        instr: instrument to load data from ('spi' or 'spc') [str]

    Returns:
        df: Pandas dataframe with data inside
    """

    # cast to lowercase
    instr = instr.lower()

    if instr == 'spi':
        print('instr = %s' % instr)
        index_col = env.spi_data_index_col
        cdf_vars = env.spi_cdf_vars
        varnames_list = env.spi_cdf_varnames
        missing_value = env.spi_errorval
        qual_flag = env.spi_quality_flag
        qual_flag_max = env.spi_qflag_max
    elif instr == 'spc':
        print('instr = %s' % instr)
        index_col = env.spc_data_index_col
        cdf_vars = env.spc_cdf_vars
        varnames_list = env.spc_cdf_varnames
        missing_value = env.spc_errorval
        qual_flag = env.spc_quality_flag
        qual_flag_max = env.spc_qflag_max
    else:
        raise ValueError('Invalid value for input argument *instr*: \'%s\'' % str(instr))
    # ---

    df = ttt.callingcdf(spi_cdf,
                        index_col=index_col,
                        cdf_vars=cdf_vars,
                        varnames_list=varnames_list,
                        missing_value=missing_value)

    try:
        if df.size == 0:
            raise RuntimeError('ERROR: empty DF')
    except RuntimeError as err:
        print(err)
        return df

    # mask errors with NAN
    print('xxx DF.COUNT() in read_spi_data: ', df.count())
    df.mask(df == env.spi_errorval, inplace=True)
    print('xxx DF.COUNT() in read_spi_data: ', df.count())

    # mask bad quality data with NAN
    spi_df = ttt.mask_bad_quality(df,
                                  qual_flag=qual_flag,
                                  qual_flag_max=qual_flag_max)
    print('xxx DF.COUNT() in read_spi_data: ', df.count())

    return spi_df


def mag_break_freq_calc(mag, magvars=None, windowStr='boxcar', chunk_plot=-1):
    """
    Args:
        fn:
        mag: Dataframe with magnetic field data
        windowStr: Type of window to use
        chunk_plot: [int] Plot the steps in calc f_break for a specific chunk. If *chunk_plot* is integer, that number
        will be used. If *chunk_plot* = 'r', choose a random chunk to plot. Pick an impossible number like
        *chunk_plot=-1* to ensure the steps are not plot.


    Returns:
        freq_log_lst: List of log-spaced frequency arrays
        P_log_lst: List of power as estimated at the frequencies *freq_log_lst*
        f_break_lst: List of break frequency estimates, one per element of *freq_log_lst*
    """

    from scipy.fft import fft, fftfreq

    # get components of magnetic field
    if magvars is None:
        magvars = ttt.listsearch(search_string=env.mag_field_varname, input_list=list(mag.columns))

    # build chunks
    chunk_duration_sec = env.chunktime
    ts_chunk = ttt.chunkify(mag.index, chunk_duration_sec)

    # get timeseries for the break freq (in-between ts_chunk)
    ts_spec = pd.Series(ts_chunk[:-1]) + pd.Timedelta(str(int(chunk_duration_sec / 2)) + 's')

    # number of chunks
    Nchunks = len(ts_chunk)
    print('Nchunks = %d' % Nchunks)

    # if random plot chunk is selected
    if chunk_plot == 'r':
        chunk_plot = np.random.randint(Nchunks)

    # sampling period
    Ts = np.round(mag.index.freq.nanos * 1e-9, decimals=6)
    Fs = 1 / Ts

    P_log_lst = []
    freq_log_lst = []
    spectral_ts_lst = []

    # output raw data to ease plotting later on
    btrace_lst = []
    freq_lst = []

    fb_arr = np.zeros(len(ts_spec))
    fb_ep_arr = np.zeros_like(fb_arr)
    fb_em_arr = np.zeros_like(fb_arr)

    # check if outside freq range
    fb_er_arr = np.zeros_like(fb_arr)

    # check if outside interval
    fb_ei_arr = np.zeros_like(fb_arr)

    # errorbar interval error check
    fb_ebr_arr = np.zeros_like(fb_arr)

    # save the steep_index and inertial_index
    steep_ix_lst = np.zeros_like(fb_arr)
    inert_ix_lst = np.zeros_like(fb_arr)
    steep_ix_e_lst = np.zeros_like(fb_arr)
    inert_ix_e_lst = np.zeros_like(fb_arr)

    # save the steep constant and inertial constant
    steep_const_lst = np.zeros_like(fb_arr)
    inert_const_lst = np.zeros_like(fb_arr)

    inert_min_lst = np.zeros_like(fb_arr)
    inert_max_lst = np.zeros_like(fb_arr)
    steep_min_lst = np.zeros_like(fb_arr)
    steep_max_lst = np.zeros_like(fb_arr)

    # create break_freq DF
    # breaf freq dataframe constisting of freak freq (fb), pos error bar (err_p), neg error bar (err_n),
    # range error indicator (err_r): 1 if fb is outside allowed freq range
    # fbreak_df = pd.DataFrame(index=ts_spec, columns=['fb', 'err_p', 'err_m', 'err_r'])
    # fbreak_df['err_r'] = 0

    for ti in range(Nchunks - 1):
        # for ti in chunk_plot:

        if ti == chunk_plot:
            plotsteps = True
        else:
            plotsteps = False

        # print('ti = %d' % ti)

        t0str = ts_chunk[ti]
        tNstr = ts_chunk[ti + 1]

        # use strings to get chunk data
        dat = mag[t0str:tNstr][magvars]

        # get chunk size
        N = dat.index.size

        # get the frequencies
        freq0 = fftfreq(N, d=Ts)

        # first half of the vector (for positive frequencies)
        k = np.arange(0, N)
        freq0[k > N / 2] = freq0[k > N / 2] - np.max(freq0)

        # i_half = range(0, int(N / 2))
        # freq = freq0[i_half]
        freq = freq0[freq0 > 0]
        freq_nyq = Fs / 2

        # set up trace matrix
        Bf_tr = np.zeros_like(dat, dtype=complex)

        # for each component of the B field
        for i in range(np.min(dat.shape)):
            # set window
            # ft_window = window_selector(N, win_name=windowStr)
            ft_window = ttt.window_selector(N, win_name=windowStr)

            # get the current component
            Bi = dat[dat.columns[i]].values

            # detrend and apply window
            ft_input_signal = mpl.mlab.detrend(Bi) * ft_window

            # get the FFT of the detrended and windowed B-field component, scale by freq
            Bf = fft(ft_input_signal, N) / np.sqrt(N / Ts)

            # get the transpose
            Bf_tr[:, i] = Bf.transpose()

        # take sum along the diagonal
        Btr = np.sum(np.squeeze(Bf_tr * np.conj(Bf_tr)), axis=1)
        # only use positive freq
        Btr = Btr[freq0 > 0]

        # smooth the trace
        Btr_smooth = ttt.smooth(np.real(Btr), n=5)

        # number of frequencies to use in logspace
        numfreqs = np.floor((np.log10(np.max(freq) / np.min(freq))) / np.log10(env.freqratio))

        # set up log-spaced frequency array
        freq_log = np.logspace(np.log(np.min(freq)) / np.log(env.freqratio),
                               np.log(freq_nyq) / np.log(env.freqratio),
                               base=env.freqratio, num=int(numfreqs))

        # interpolate smoothed trace to log-spaced freqs
        Plog = np.interp(freq_log, freq, Btr_smooth)

        # restrict to higher freqs (hard-coded limit at 0.5Hz)x
        # iii = np.argmin(np.abs(freq_log-0.01))
        # iii = np.argmin(np.abs(freq-0.5))
        iii = 0 # TODO remove this part

        # get the frequency breaks from the log-spaced power and frequency
        C = breakfreq_lines(freq_log[iii:], Plog[iii:], plot_steps=plotsteps, plot_title=str(t0str) + ' -- ' + str(tNstr))

        f_break, fb_em, fb_ep, inert_min, inert_max, steep_min, steep_max, inert_const, inert_ix, \
        steep_const, steep_ix, spectral_ts, inert_ix_e, steep_ix_e = C

        freq_log_lst.append(freq_log)
        P_log_lst.append(Plog)
        spectral_ts_lst.append(spectral_ts)
        # f_break_lst.append(f_break)
        fb_arr[ti] = f_break
        fb_em_arr[ti] = fb_em
        fb_ep_arr[ti] = fb_ep

        # save the steep_index and inertial_index
        steep_ix_lst[ti] = steep_ix
        inert_ix_lst[ti] = inert_ix

        # save the steep and inertial constants
        steep_const_lst[ti] = steep_const
        inert_const_lst[ti] = inert_const

        # Error of inert_ix and steep_ix
        steep_ix_e_lst[ti] = steep_ix_e
        inert_ix_e_lst[ti] = inert_ix_e

        inert_min_lst[ti] = inert_min
        inert_max_lst[ti] = inert_max

        steep_min_lst[ti] = steep_min
        steep_max_lst[ti] = steep_max

        # output raw data to ease plotting later on
        btrace_lst.append(Btr)
        freq_lst.append(freq)

        # append set of values to fbreak_df
        # fbreak_df.fb[ti] = f_break

        # do error check such as this; mark f_break if outside freq range
        if (f_break < np.nanmin(freq)) | (f_break > np.nanmax(freq)):
            # fbreak_df.err_r[ti] = 1
            fb_er_arr[ti] = 1

        # do error check such as this; mark f_break if outside freq range
        if (f_break < inert_max) | (f_break > steep_min):
            # fbreak_df.err_r[ti] = 1
            fb_ei_arr[ti] = 1

        # Error check for the errorbars
        # mark where the error bars are out of range
        if (fb_em < inert_max) | (fb_ep > steep_min):
            fb_ebr_arr[ti] = 1

        if (fb_ep < inert_max) | (fb_em > steep_min):
            fb_ebr_arr[ti] = 1

    output_dict = {
        'spectime': ts_spec,
        # 'breakfreq': np.array(f_break_lst),
        'breakfreq': fb_arr,
        'freq_log': freq_log_lst,
        'Psd_log': P_log_lst,
        'breakfreq_ep': fb_ep_arr,
        'breakfreq_em': fb_em_arr,
        'breakfreq_er': fb_er_arr,
        'breakfreq_ei': fb_ei_arr,
        'breakfreq_eb': fb_ebr_arr,
        'spectral_ts': spectral_ts_lst,
        'btrace': btrace_lst,
        'freq': freq_lst,
        'inert_const': inert_const_lst,
        'steep_const': steep_const_lst,
        'inert_ind': inert_ix_lst,
        'steep_ind': steep_ix_lst,
        'inert_ind_e': inert_ix_e_lst,
        'steep_ind_e': steep_ix_e_lst,
        'inert_min': inert_min_lst,
        'inert_max': inert_max_lst,
        'steep_min': steep_min_lst,
        'steep_max': steep_max_lst
    }

    return output_dict


def breakfreq_lines(freq, Psd, plot_steps=True, plot_title=''):
    """
    Calculate the break in the power-frequency spectrum according to RWICKS method. This
    is called on every chunk of data (length determined by `env.chunktime`).

    Args:
        freq: Set of frequencies [np.ndarray]
        Psd: Set of powers [np.ndarray]
        plot_steps: Flag to indicate if illustrative plot should be made [boolean]
        plot_title: If *plot_steps* is True, *plot_title* is the title of the figure

    Returns:
        breakfreq:
        breakfreq_em:
        breakfreq_ep:
        inert_minw:
        inert_maxw:
        steep_minw:
        steep_maxw:
        inert_const:
        inert_ix:
        steep_const:
        steep_ix:
        spectral_ts:
        inert_ix_e:
        steep_ix_e

    """

    fitrange = int(np.ceil(np.log(2.5) / np.log(env.freqratio)))

    interpw = freq
    trace_B_fit = Psd

    spectral_ts = np.zeros_like(trace_B_fit)
    spectralconst_ts = np.zeros_like(trace_B_fit)

    cov_slope = np.zeros_like(spectralconst_ts)
    cov_inter = np.zeros_like(spectralconst_ts)

    if plot_steps:
        plt.figure()

    # --- it's just a piecewise linear fit with equal-length segments ---
    # spectral exponent calculation
    for j in range(1, np.max(interpw.shape) - fitrange + 2):
        expfit_x = np.log10(interpw[j:j + fitrange])  # xxx move outside of for loop
        expfit_y = np.log10(trace_B_fit[j:j + fitrange])  # xxx move outside of for loop

        # call generic fit function of given order (typically 1, i.e. linear)
        exponentfit, cfit = ttt.fit(expfit_x, expfit_y, deg=env.fitfunction_deg, fullyes=True)

        # slope
        spectral_ts[j + int((fitrange + 1) / 2)] = exponentfit[0]

        # intercept
        spectralconst_ts[j + int((fitrange + 1) / 2)] = exponentfit[1]

        cov_slope[[j + int((fitrange + 1) / 2)]] = cfit[0]
        cov_inter[[j + int((fitrange + 1) / 2)]] = cfit[1]

        if plot_steps:
            plt.plot(10 ** expfit_x, 10 ** (exponentfit[0] * expfit_x + exponentfit[1]), color='gray')

    # --- it's just a piecewise linear fit with equal-length segments ---

    # get spectral ix and ts for this loop
    spectral_ix_i = np.squeeze(spectral_ts)
    spectralconst_ts_i = np.squeeze(spectralconst_ts)

    # define the high freq range
    highwrange = interpw[interpw > 0.5]
    psd_highwrange = Psd[interpw > 0.5]

    # ### fit staight line here
    # fitpars = np.polyfit(np.log10(highwrange), np.log10(psd_highwrange), deg=1, full=False)
    # plt.figure()
    # plt.plot(np.log10(highwrange), np.log10(psd_highwrange))
    # plt.plot(np.log10(highwrange), fitpars[0]*np.log10(highwrange) + fitpars[1], 'm--')

    # steep part of spec
    steepfreq = np.argmin(spectral_ix_i[interpw > 0.5])

    # why *, / and why 1.3 ?
    steepmaxw = highwrange[steepfreq] * env.freqConst
    steepminw = highwrange[steepfreq] / env.freqConst

    midw_ix = np.argmin(np.abs(interpw - highwrange[steepfreq]))
    maxw_ix = np.argmin(np.abs(highwrange - steepmaxw))
    minw_ix = np.argmin(np.abs(highwrange - steepminw))

    maxw_ix_steep = maxw_ix.copy()
    minw_ix_steep = minw_ix.copy()

    steepspec_sel = spectral_ix_i[interpw > 0.5]  # XXX why 0.5?
    steepconst_sel = spectralconst_ts_i[interpw > 0.5]  # XXX why 0.5?

    cov_slope_steep = cov_slope[interpw > 0.5]
    cov_inter_steep = cov_inter[interpw > 0.5]

    steep_ix = np.mean(steepspec_sel[minw_ix:maxw_ix + 1])
    steep_ix_e = np.mean(cov_slope_steep[minw_ix:maxw_ix + 1])
    steep_const = np.mean(steepconst_sel[minw_ix:maxw_ix + 1])
    steep_const_e = np.mean(cov_inter_steep[minw_ix:maxw_ix + 1])

    steep_minw = highwrange[minw_ix]
    steep_maxw = highwrange[maxw_ix]

    inertial_w_mid = highwrange[steepfreq] / env.inertial_mid_const
    inertial_w_max = 2 * highwrange[steepfreq] / env.inertial_mid_const
    inertial_w_min = highwrange[steepfreq] / env.inertial_min_const

    midw_ix = np.argmin(np.abs(interpw - inertial_w_mid))
    maxw_ix = np.argmin(np.abs(interpw - inertial_w_max))
    minw_ix = np.argmin(np.abs(interpw - inertial_w_min))

    maxw_ix_inert = maxw_ix.copy()
    minw_ix_inert = minw_ix.copy()

    inert_ix = np.mean(spectral_ix_i[minw_ix:maxw_ix + 1])
    inert_ix_e = np.mean(cov_slope[minw_ix:maxw_ix + 1])
    inert_const = np.mean(spectralconst_ts_i[minw_ix:maxw_ix + 1])
    inert_const_e = np.mean(cov_inter[minw_ix:maxw_ix + 1])

    inert_minw = interpw[minw_ix]
    inert_maxw = interpw[maxw_ix]

    # calculate the break frequency
    breakfreq = 10 ** ((inert_const - steep_const) / (steep_ix - inert_ix))

    # calculate break frequency error
    # reg
    breakfreq_em = 10 ** (((inert_const - inert_const_e) - (steep_const - steep_const_e)) /
                          ((steep_ix - steep_ix_e) - (inert_ix - inert_ix_e)))

    breakfreq_ep = 10 ** (((inert_const + inert_const_e) - (steep_const + steep_const_e)) /
                          ((steep_ix + steep_ix_e) - (inert_ix + inert_ix_e)))

    if plot_steps:
        plot_breakfreq_steps(freq, Psd, steepminw, steepmaxw, inert_minw, inert_maxw,
                             breakfreq, breakfreq_em, breakfreq_ep, inert_ix, inert_ix_e, inert_const, inert_const_e,
                             steep_ix, steep_ix_e, steep_const, steep_const_e, plot_title)

    return breakfreq, breakfreq_em, breakfreq_ep, inert_minw, inert_maxw, steep_minw, steep_maxw, \
           inert_const, inert_ix, steep_const, steep_ix, spectral_ts, inert_ix_e, steep_ix_e


def plot_breakfreq_steps(freq, Psd,
                         steepminw, steepmaxw, inert_minw, inert_maxw,
                         breakfreq, breakfreq_em, breakfreq_ep,
                         inert_ix, inert_ix_e, inert_const, inert_const_e,
                         steep_ix, steep_ix_e, steep_const, steep_const_e, plot_title=''):
    """
    Plot the steps when estimating break frequency

    Args:
        freq:
        Psd:
        steepminw:
        steepmaxw:
        inert_minw:
        inert_maxw:
        breakfreq:
        breakfreq_em:
        breakfreq_ep:
        inert_ix:
        inert_ix_e:
        inert_const:
        inert_const_e:
        steep_ix:
        steep_ix_e:
        steep_const:
        steep_const_e:
        timestr:

    Returns:

    """

    # plt.plot(np.log10(freq), np.log10(Psd), color='C0', marker='.')
    plt.plot(freq, Psd, color='C0', marker='.')
    ax1 = plt.gca()
    ax1.set_xscale('log')
    ax1.set_yscale('log')

    # plt.axvspan(np.log10(steepminw), np.log10(steepmaxw), color='r', alpha=0.2, label='steep range')
    # plt.axvspan(np.log10(inert_minw), np.log10(inert_maxw), color='k', alpha=0.2, label='inertial range')

    plt.axvspan(steepminw, steepmaxw, color='r', alpha=0.1, label='Diss. range')
    plt.axvspan(inert_minw, inert_maxw, color='g', alpha=0.1, label='Inertial range')

    plt.axvline(breakfreq, color='k', ls='-', lw=2, label='$f_b$')
    plt.axvline(breakfreq_em, color='b', ls='--', lw=1, label='$f_b - \epsilon_-$')
    plt.axvline(breakfreq_ep, color='k', ls='--', lw=1, label='$f_b + \epsilon_+$')

    avgfitx = np.array([np.log10(inert_minw), np.log10(steepminw)])
    avgfity = inert_ix * avgfitx + inert_const
    plt.plot(10 ** avgfitx, 10 ** avgfity, lw=2, color='g', label='Inertial avg linear fit')

    plt.fill_between(10 ** avgfitx,
                     10 ** np.polyval([inert_ix + inert_ix_e, inert_const + inert_const_e], avgfitx),
                     10 ** np.polyval([inert_ix - inert_ix_e, inert_const - inert_const_e], avgfitx),
                     color='g', alpha=0.4, label='Inertial range fit error')

    avgfitx = np.array([np.log10(inert_maxw), np.log10(steepmaxw)])
    avgfity = steep_ix * avgfitx + steep_const
    plt.plot(10 ** avgfitx, 10 ** avgfity, lw=2, color='r', label='Dissipation avg linear fit')

    plt.fill_between(10 ** avgfitx,
                     10 ** np.polyval([steep_ix, steep_const] + np.array([steep_ix_e, steep_const_e]), avgfitx),
                     10 ** np.polyval([steep_ix, steep_const] - np.array([steep_ix_e, steep_const_e]), avgfitx),
                     color='r', alpha=0.4, label='Diss. range fit error')

    plt.xlabel('Freq [Hz]')
    # plt.ylabel('Power [$\mathrm{nT^2/Hz}$]')
    plt.ylabel('Power [$\mathrm{nT^2.Hz^{-1}}$]')
    plt.legend()
    # plt.grid(which='major')

    # figure title
    # title_str = env.file_flags_mag + ':  ' + plot_title
    title_str = plot_title
    plt.title(title_str)

    return


def psp_trace_fourier_power(B, delta_t):
    """
    Calculate FFT of chunk of B_RTN

    Args:
        B:
        delta_t:

    Returns:

    """
    import matplotlib as mpl

    tslen = np.max(B.shape)

    # set length
    nfft = tslen

    # set range of FT components
    k = np.arange(0, nfft)

    # set range of freqs
    w = k / (nfft * delta_t)

    # mirror freq
    w[k > nfft / 2] = w[k > nfft / 2] - np.max(w)

    # initialise FT matrix of B
    bhat = np.zeros_like(B, dtype=complex)

    # calc FFT for each component of B
    for i in range(np.min(B.shape)):
        b_comp_fft = np.fft.fft(mpl.mlab.detrend(B[:, i]), tslen) / np.sqrt(tslen / delta_t)
        bhat[:, i] = b_comp_fft.transpose()

    # calculate component-wise sum of FFT
    traceb = np.sum(np.squeeze(bhat * np.conj(bhat)), axis=1)

    # only utilise positive w
    traceb = traceb[w > 0]
    w = w[w > 0]

    return traceb, w


def spectraplot(varsdict, fig_title='', chunk_number=110):
    """
    Plot
    - spectrum of B_tr with estimated break frequency indicated, and
    - the inertial and dissipation range fits for a single chunk of B-field data

    Args:
        varsdict: [dict] Dictionary with various variables
        fig_title: [str] Figure title
        chunk_number: [int] The chunk interval to plot

    Returns:
        None

    """

    breakfreq = varsdict['breakfreq']
    interpw = varsdict['freq_log']
    psd_lst = varsdict['Psd_log']
    spec_lst = varsdict['spectral_ts']
    breakfreq_r = varsdict['breakfreq_er']
    breakfreq_p = varsdict['breakfreq_ep']
    breakfreq_m = varsdict['breakfreq_em']

    breakfreq[breakfreq_r > 0.5] = np.nan

    traceb_ts = np.zeros((len(psd_lst[0]), len(psd_lst)))
    spectralix_ts = np.zeros((len(spec_lst[0]), len(spec_lst)))

    # reshape psd
    for i in range(len(psd_lst)):
        traceb_ts[:, i] = psd_lst[i]

    for i in range(len(spec_lst)):
        spectralix_ts[:, i] = spec_lst[i]

    # make first figure
    fig1, axs = plt.subplots(nrows=2,
                             ncols=1,
                             sharex=True,
                             figsize=(env.figW, env.figH))

    # first axis, contour plot
    N = np.max(breakfreq.shape)
    ix = np.arange(1, N + 1)
    cs0 = axs[0].contourf(ix, interpw[0], np.log10(traceb_ts))

    axs[0].fill_between(ix[breakfreq_r < 0.5],
                        breakfreq_m[breakfreq_r < 0.5],
                        breakfreq_p[breakfreq_r < 0.5],
                        color='k',
                        alpha=0.4)

    axs[0].semilogy(ix, breakfreq, lw=1, color='k', ls='-')

    axs[0].set_xlabel('Spec num (%d s per spec)' % env.chunktime)
    axs[0].set_ylabel('Freq [Hz]')
    axs[0].set_title('%s\nPower spec $\log_{10}(P)$' % fig_title)
    axs[0].grid(which='major')

    cbar0 = plt.colorbar(cs0, ax=axs[0])

    # second axis, contour plot
    cs1 = axs[1].contourf(ix, interpw[0], spectralix_ts)
    axs[1].fill_between(ix[breakfreq_r < 0.5],
                        breakfreq_m[breakfreq_r < 0.5],
                        breakfreq_p[breakfreq_r < 0.5],
                        color='k',
                        alpha=0.5)

    axs[1].semilogy(ix, breakfreq, lw=1, color='k', ls='-')

    cbar1 = fig1.colorbar(cs1, ax=axs[1])
    axs[1].set_xlabel('Spec num (%d s per spec)' % env.chunktime)
    axs[1].set_ylabel('Freq [Hz]')
    axs[1].set_title('Spec index')
    axs[1].grid(which='major')

    fig1.suptitle(fig_title)
    plt.tight_layout()

    # the other figure
    fig2, axs = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(env.figW, env.figH))

    x = interpw
    y = psd_lst

    # create the best-fit lines for the inertial and dissipation ranges
    inert_fit_y = 10 ** (varsdict['inert_const'][chunk_number] +
                         varsdict['inert_ind'][chunk_number] * np.array([-2, 1]))
    inert_fit_x = [0.01, 10]

    diss_fit_y = 10 ** (varsdict['steep_const'][chunk_number] +
                        varsdict['steep_ind'][chunk_number] * np.array([-1, 1]))
    diss_fit_x = [0.1, 10]

    # plot the PSD versus frequency
    axs[0].loglog(x[chunk_number], y[chunk_number], label='Psd (FFT)')

    # plot break frequency
    axs[0].axvline(breakfreq[chunk_number], ls='--', color='r', label='Break freq')

    # plot the inertial and dissipation range fits
    axs[0].loglog(inert_fit_x, inert_fit_y, label='Inert fit line')
    axs[0].loglog(diss_fit_x, diss_fit_y, label='Diss fit line')

    # indicate the range of dissipation range and inertial range frequencies
    axs[0].axvspan(varsdict['steep_min'][chunk_number], varsdict['steep_max'][chunk_number], color='m', alpha=0.2)
    axs[0].axvspan(varsdict['inert_min'][chunk_number], varsdict['inert_max'][chunk_number], color='m', alpha=0.2)

    axs[0].set_xlabel('Freq [Hz]')
    axs[0].set_ylabel('Power')
    axs[0].legend()
    axs[0].grid()

    # Figure 2, axis 2
    x = interpw
    y = spec_lst
    dissfit = varsdict['steep_ind'][chunk_number]
    inertfit = varsdict['inert_ind'][chunk_number]

    # plot the spectral index versus frequency
    axs[1].semilogx(x[chunk_number], y[chunk_number], label='Spectral index (FFT)')

    # plot the break frequency
    axs[1].axvline(breakfreq[chunk_number], ls='--', color='r', label='Break freq')

    # plot the inertial and dissipation range fits
    axs[1].axhline(inertfit, label='Inertial fit', color='C1')
    axs[1].axhline(dissfit, label='Dissipation fit', color='C2')

    # labels etc
    axs[1].set_xlabel('Freq [Hz]')
    axs[1].set_ylabel('Spectral index')
    axs[1].legend()
    axs[1].grid()
    fig2.suptitle(fig_title)
    plt.tight_layout()
    fig2.subplots_adjust(hspace=0.0)

    return


def gettingdata(dt_user, instr='spc'):
    """
    Get SPC or SPI data. Download only if necessary, and apply `fix_data_rate()`.

    Args:
        dt_user: Datetime string
        instr: Instrument code 'spi' or 'spc'

    Returns:
        out_df: [pandas Dataframe] Output data frame
        cdf_filepath: [str] Path to the CDF source file
        pkl_filepath: [str] Path to the PKL file

    """

    if instr == 'spc':
        full_url = ttt.psp_urlbuilder(dt_user, dt_fmt=env.file_datepart_format_spc, baseurl=env.baseurl_spc,
                                      file_hdr=env.file_header, file_flags=env.file_flags_spc,
                                      data_version=env.spc_data_version, file_ext=env.spc_file_ext)
    elif instr == 'spi':
        full_url = ttt.psp_urlbuilder(dt_user, dt_fmt=env.file_datepart_format_spi, baseurl=env.baseurl_spi,
                                      file_hdr=env.file_header, file_flags=env.file_flags_spi,
                                      data_version=env.spi_data_version, file_ext=env.spi_file_ext)
    else:
        raise ValueError('Invalid value for input argument *instr*: \'%s\'' % str(instr))

    # initialise returned variable to error value
    # out_df = env.ERRORVAL
    out_df = pd.DataFrame([])

    # get filename
    cdf_filename = ttt.print_fn_only(full_url, dir_del='/')

    pkl_filename = ttt.replace_filename_extension(cdf_filename, newextension='pkl')

    cdf_filepath = os.path.join(env.data_dir, cdf_filename)
    pkl_filepath = os.path.join(env.data_dir, pkl_filename)

    # if pkl filepath exists, then just read it
    if os.path.exists(pkl_filepath):
        print('PKL file exist')
        out_df = pd.read_pickle(pkl_filepath)
    else:
        print('PKL file DONT exist')
        # download the file
        dlres = ttt.download_data(full_url, outpath=env.data_dir)

        # first load the spc CDF file
        out_cdf = ttt.load_cdf_file(os.path.join(env.data_dir, cdf_filename))

        if out_cdf == env.ERRORVAL:
            pass
        else:
            # convert SPI CDF to pandas DF
            out_df = read_sp_data(out_cdf, instr=instr)
            # if instr == 'spc':
            #     out_df = read_spi_data(out_cdf, instr=instr)
            # elif instr == 'spi':
            #     out_df = read_spi_data(out_cdf, instr=instr)

    if isinstance(out_df, pd.DataFrame):
        if out_df.dropna().shape[0] == 0:
            out_df = pd.DataFrame([])

    return out_df, cdf_filepath, pkl_filepath


def gettingmagdata(dt_user, fixdatarate=True, savepoint=True):
    """
    Get MAG instrument data. Download only if necessary, and apply `fix_data_rate()`.

    Args:
        dt_user: Datetime string
        fixdatarate: [bool] Whether or not to fix the datarate
        savepoint: [bool] Whether or not to save data to file

    Returns:
        out_df: [pandas.DataFrame] Loaded and processed data

    """

    # build the URL
    # use user datetime to build filename to download
    full_url = ttt.psp_urlbuilder(dt_user,
                                  dt_fmt=env.file_datepart_format_mag,
                                  baseurl=env.baseurl_mag,
                                  file_hdr=env.file_header,
                                  file_flags=env.file_flags_mag,
                                  data_version=env.mag_data_version,
                                  file_ext=env.mag_file_ext)

    # get the filename without the full path
    cdf_filename = ttt.print_fn_only(full_url, dir_del='/')
    pkl_filename = ttt.replace_filename_extension(cdf_filename, newextension='pkl')
    x_pkl_filename = ttt.replace_filename_extension(cdf_filename, newextension='x.pkl')

    cdf_filepath = os.path.join(env.data_dir, cdf_filename)
    pkl_filepath = os.path.join(env.data_dir, pkl_filename)
    x_pkl_filepath = os.path.join(env.data_dir, x_pkl_filename)

    # initialise output DF here
    out_df0 = env.ERRORVAL

    # if pkl filepath exists, then just read it
    if os.path.exists(x_pkl_filepath) & fixdatarate:
        print('X_PKL file exist')
        out_df0 = pd.read_pickle(x_pkl_filepath)
        fixdatarate = False
    elif os.path.exists(pkl_filepath):
        print('PKL file exist')
        out_df0 = pd.read_pickle(pkl_filepath)
    else:
        print('PKL file DONT exist')
        # download the file
        dlres = ttt.download_data(full_url, outpath=env.data_dir)

        if dlres == env.ERRORVAL:
            return env.ERRORVAL

        # first load the CDF file
        mag_cdf = ttt.load_cdf_file(cdf_filepath)

        if mag_cdf != env.ERRORVAL:
            # convert CDF to pandas DF
            out_df0 = read_mag_data(mag_cdf)

            if isinstance(out_df0, type(env.ERRORVAL)):
                return env.ERRORVAL

            # file path to save to
            if savepoint:
                ttt.printmessage('---> SAVEPOINT <----')
                ttt.savepickle(pkl_filepath, varslist=out_df0)

    if fixdatarate:
        out_df = fix_data_rate(df=out_df0,
                               r=out_df0[env.mag_data_rate_var_name],
                               L=env.cycle_dur,
                               floor_index=False,
                               interpmethod=env.interpolation_method,
                               dataratevarname=env.mag_data_rate_var_name,
                               plotflag=False)
        # file path to save to
        if savepoint:
            ttt.printmessage('---> SAVEPOINT <----')
            ttt.savepickle(x_pkl_filepath, varslist=out_df)

    else:
        out_df = out_df0

    print('xxxx ----- OUT_DF INDEX FREQ: ', out_df.index.freq)
    # out_df = out_df.dropna()
    # out_df.dropna(inplace=True)
    print('xxxx ----- OUT_DF INDEX FREQ: ', out_df.index.freq)

    if out_df.shape[0] == 0:
        out_df = env.ERRORVAL

    return out_df
