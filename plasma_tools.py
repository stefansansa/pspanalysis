"""
PLASMA tools for PSP analysis code


"""


# --- PHYSICAL CONSTANTS ---
# --- use CAPS for physical constants, makes code easier to read --

EV_PER_KELVIN = 8.61733e-5
"""float: Conversion factor from eV to Kelvin"""

AU_KM = 149597871
"""int: 1 Astronomical Unit (AU) in kilometre"""

MP_KG = 1.6726219e-27
"""float: Mass of a proton in kg"""

# Boltzmann’s constant, m2 kg s-2 K-1
KB = 1.3806485e-23
"""float: Boltzmann's constant in J/K"""

# Elementary charge, C
E_C = 1.602176634e-19
"""float: Elementary charge in Columb C"""

C_MS = 2.99792458e8
"""float: Speed of light in m/s"""

E0 = 8.85418782e-12
"""float: Permittivity of free space, F/m"""

SOLAR_ROTATION_DAYS = 25.4
"""float: Solar rotation period in days"""

# --- AVERAGE SOLAR WIND CONDITIONS NEAR EARTH ---
B_const_earth = 4.75
T_const_earth = 3.9e4
N_const_earth = 7

# --- PLASMA QUANTITIES ---

def thermal_speed(temp, mass):
    """
    Calculate thermal speed

    Args:
        temp:
        mass:

    Returns:
        vth: thermal speed in [m/s], i.e. v_th = \sqrt{3 K_b  T / m}

    """
    vth = (3 * KB * temp / mass) ** 0.5

    return vth


def alfven_speed(B, mass, dens):
    """
    Calculate Alfven speed

    Args:
        B:
        mass:
        dens:

    Returns:
        va: Alfven speed in [m/s]
    """

    freq_cycl = cyclotron_freq(B, mass)
    freq_plasma = plasma_freq(dens, mass)

    va = C_MS * freq_cycl / freq_plasma

    return va


def cyclotron_freq(B, mass, z=1):
    """
    Calculate cyclotron frequency

    Args:
        B: Magnetic field
        mass: Mass
        z: Charge number

    Returns:
        fc: Cyclotron frequency in rad/s
    """

    fc = z * E_C * B / mass

    return fc


def plasma_freq(dens, mass):
    """
    Calculate plasma frequency in XXX.

    Args:
        dens: [float, ndarray] Density in XXX
        mass: [float, ndarray] Mass in kg

    Returns:
        fp: [float, ndarray] Plasma frequency in 1/s (Hz)

    """
    fp = ((dens * E_C ** 2) / (mass * E0)) ** 0.5

    return fp


def gyroscale(temp, mass, B):
    """
    Calculate gyro-scale

    Args:
        temp:
        mass:
        B:

    Returns:

    """
    vth = thermal_speed(temp=temp, mass=mass)

    freq_cycl = cyclotron_freq(B=B, mass=mass)

    s_g = vth / freq_cycl

    # [m/s / 1/s = m]

    return s_g


def inertialscale(B, mass, dens):
    """
    Inertial scale

    Args:
        B:
        mass:
        dens:

    Returns:

    """
    valf = alfven_speed(B, mass, dens)

    freq_cycl = cyclotron_freq(B, mass)

    s_i = valf / freq_cycl

    # [m/s] / [1/s] = [m]

    return s_i


def cycl_res_wavelen(B, mass, dens, temp):
    """
    Wavelength of cyclotron freq
    
    Args:
        B:
        mass:
        dens:
        temp:

    Returns:

    """

    freq_cycl = cyclotron_freq(B, mass)
    vth = thermal_speed(temp, mass)
    valf = alfven_speed(B, mass, dens)

    kd = freq_cycl / (valf + vth)
    # kd = freq_cycl / (valf + 3*vth)

    return kd


# TODO conversion functions
# - wavenumbers to RAD/KM
#   - breakscale_k variables
#