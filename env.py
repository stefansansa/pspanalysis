"""
Environment file for `pspbreaks.py`

Users to edit this file to set user-defined variables

"""

import os
import matplotlib.pyplot as plt

plt.style.use('default')
# plt.style.use('seaborn')
# plt.style.use('seaborn-whitegrid')
# plt.style.use('ggplot')


def create_dir_if_not_exist(dir):
    """
    function to create a directory if it doesn't exist
    :param dir: directory to test and create if necessary
    """
    if not os.path.exists(dir):
        os.makedirs(dir)
        print('Created directory\n   %s\n' % str(dir))


# --- most basic user settings ---

# start and end dates

# time_selected = '2020060100'
# time_selected = '2020060300'
# time_selected = '2020060400'
# time_selected = '2020060700'
# time_selected = '2020053118'
# time_selected = '2020053100'
# time_selected = '2020050912'
# time_selected = '2020050800'  # start of PERI 5
# time_selected = '2020050700'  # start of PERI 5
time_selected = '2020060100'  # TESTING DONE ON 7 DEC 2021
# time_selected = '2021010100'  # TESTING DONE ON 7 DEC 2021
# time_selected = '2020050900'
# time_selected = '2020072000'
# time_selected = '2020091000'
# time_selected = '2020080100'
# time_selected = '2020070100'
# time_selected = '2020072600'
# time_selected = '2020081700'    # START OF ORBIT 6


"""str: time selection in YYYYMMDDHH format"""

# endtime_selected = '2020060200'
# endtime_selected = '2020060100'
# endtime_selected = '2020052200'
# endtime_selected = '2020051000'
endtime_selected = '2020060112' # TESTING DONE ON 7 DEC 2021
# endtime_selected = '2021010112' # TESTING DONE ON 7 DEC 2021
# endtime_selected = '2020060700'  # end of PERI 5
# endtime_selected = '2020061900'  # end of PERI 5
# endtime_selected = '2020072000'
# endtime_selected = '2020071000'
# endtime_selected = '2020080200'
# endtime_selected = '2020072700'
# endtime_selected = '2020091500'
# endtime_selected = '2020093023'    # END OF ORBIT 6

# directory structure definitions ---

pkl_default_filename = 'pickledump_%Y%m%d-%H%M.pkl'

repo_dir = os.path.expanduser('./')
"""str: directory where repo is kept. Change this to suit your needs. Default
is ./ -- i.e. the same directory where the env.py is kept."""

data_dir = os.path.expanduser(os.path.join(repo_dir, 'datafiles'))
# data_dir = os.path.expanduser(os.path.join(repo_dir, 'datafiles3'))
# data_dir = os.path.expanduser(os.path.join(repo_dir, 'datafiles2'))
"""str: directory where data files are saved. Will create if not exist."""

# pickle file name extension
pkl_ext = '.pkl'

# name of the temperature variable in SPI data files
spi_temp_varname = 'temp'
"""str: Name of the temperature variable in SPI data files"""

# image_dir = os.path.expanduser(os.path.join(repo_dir, 'images'))
image_dir = os.path.expanduser(os.path.join(repo_dir, 'images3'))
"""str: directory where images are saved. Will create if not exist."""

cdflib_dir = os.path.expanduser('~/local/cdf/lib')
"""str: location of CDF library (this is for pycdf to work)"""

# create directories
create_dir_if_not_exist(data_dir)
create_dir_if_not_exist(image_dir)


# OTHER CONSTANTS

ERRORVAL = -9999.99
"""float: A generic error value to use, mainly for function-return since NAN is tricky to test for"""

interpolation_limit = 1
"""int: Limit for the interpolation gap when reindexing a dataframe (see function *newindex*)."""

fillna_limit = 2
"""int: Limit to fill NA. Used in function *newindex*."""

cycle_dur = 0.874
"""float: Instrument cycle duration in seconds."""

breakfreq_error_check = True
# breakfreq_error_check = False
"""bool: Whether or not to check for out-of-range error in break frequency estimate."""

verbose = True
"""bool: If code should be verbose, i.e. lots of messages printed to screen."""

# --- calculation and processing ---

interpolation_method = 'quadratic'
"""str: interpolation method to use in `fix_data_rate()` function. The value of this variable is pushed to the 
scipy.interpolation method, called by pandas. Allowed values are 'linear', 'quadratic', 'cubic'."""

calc_break_freq_flag = True
"""bool: Whether or not to calculate the break freq """

calc_len_scale_flag = True
"""bool: Whether or not to calculate the length scale """

# --- PSP data file details ---


"""str: time selection in YYYYMMDDHH format"""

input_time_format = '%Y%m%d%H'
"""str: C-like date format definition for user input"""

file_header = 'psp'
"""str: first part of PSP data file"""


# --- URL AND FILE SETTINGS FOR MAG ---
file_datepart_format_mag = '%Y%m%d%H'
"""str: C-like date format for the PSP data files"""

mag_fixdatarate = True
"""boolean: Flag to fix data rate of MAG data or not. If TRUE then `fix_data_rate()` is called."""

mag_time_delta = '6H'
"""str: Expected time between file names given in `datetime` frequency 
convention units (e.g. 6H for 6 hours, 1d for 1 day, etc.)"""

mag_qflag_max = 0
# mag_qflag_max = 128
"""int: Max value that quality flag may be. This assumes higher qflag
denotes worse quality data. Instances where qflag > mag_qflag_max will be 
masked with NAN."""

mag_qflag_name = 'qualflag'
"""str: Name to use for quality flag variable."""

mag_qflag_index = 'epoch_quality_flags'
"""str: Key name for the *index* of the quality flag variable of MAG data."""

mag_qflag_var = 'psp_fld_l2_quality_flags'
"""str: Key name for the quality flag variable of MAG data."""

mag_data_version = 'v01'
# mag_data_version = 'v02'
"""str: version of MAG data. The default is \'v01\', but can be \'v02\' as well."""

mag_file_ext = '.cdf'
"""str: last part of the PSP and SPC data file"""

baseurl_mag = 'http://research.ssl.berkeley.edu/data/psp/data/sci/fields/l2/mag_RTN'
"""str: base URL of the PSP data website"""

file_flags_mag = 'fld_l2_mag_RTN'
"""str: extra flags string to be supplied by user"""

mag_errorval = -1e31
"""float: Error value for MAG instrument"""

mag_data_index_col = 0
# mag_data_index_col = None
"""int: Identify which CDF variable (zero-indexed) to use as dataframe index.
Default value of None """

mag_data_rate_var_name = 'rate'
mag_data_rate_var_num = 5

mag_cdf_vars = [1, mag_data_rate_var_num]
# mag_cdf_vars = None
"""list (int): CDF variables to use for MAG. Default value of None."""

mag_field_varname = 'BRTN'

mag_cdf_varnames = [mag_field_varname, mag_data_rate_var_name]
# mag_cdf_varnames = None
"""list (str): Variable names to assign to cdf_vars above. If default value of
None is given, the original CDF variable names will be used."""


# --- URL AND FILE SETTINGS FOR SPI ---

spi_ion_id = 'sf00'
"""str: ion type identifier for SPI"""

baseurl_spi = 'http://sweap.cfa.harvard.edu/pub/data/sci/sweap/spi/L3/spi_' + spi_ion_id
"""str: base URL of the SPI data website"""

file_flags_spi = 'swp_spi_' + spi_ion_id + '_L3_mom_INST'
"""str: file flags string """

file_datepart_format_spi = '%Y%m%d'
"""str: format of the date/time part of the filename (in C-like datetime 
format codes"""

spi_data_version = 'v03'
# spi_data_version = 'v02'
# spi_data_version = 'v01'
# spi_data_version = 'v0x'
"""str: version of data to use (\'v01\' or \'v02\')"""

spi_file_ext = '.cdf'
"""str: extension of SPI data files"""

spi_time_delta = '1D'
"""str: Expected time between file names given in `datetime` frequency 
convention units (e.g. 6H for 6 hours, 1d for 1 day, etc.)"""

spi_qflag_max = 0
# spi_qflag_max = 128
"""int: Max value that quality flag may be. This assumes higher qflag
denotes worse quality data. Instances where qflag > spi_qflag_max will be 
masked with NAN."""

spi_data_index_col = 0
# spi_cdf_vars = [29, 31, 32, 34, 35, 36]
spi_cdf_vars = ['QUALITY_FLAG', 'DENS', 'VEL', 'TEMP', 'MAGF_SC', 'MAGF_INST']
# spi_cdf_varnames = None
spi_cdf_varnames = ['quality_flag','dens','vel','temp', 'modbSC', 'modbINST']
spi_quality_flag = 'quality_flag'
spi_errorval = -1e31

# ---X----------------------------X---


# --- URL AND FILE SETTINGS FOR SPC ---
spc_data_version = 'v02'
# spc_data_version = 'v01'
"""str: Data version to use for SPC"""

spc_qflag_name = 'general_flag'
spc_quality_flag = 'general_flag'
"""str: Name of the quality flag in the CDF variable"""

# spc_qflag_max = 128
spc_qflag_max = 0
"""int: Max value that quality flag may be. This assumes higher qflag
denotes worse quality data. Instances where qflag > spc_qflag_max will be 
masked with NAN."""

baseurl_spc = 'http://sweap.cfa.harvard.edu/pub/data/sci/sweap/spc/L3'
"""str: base URL of the SPC data website"""

file_flags_spc = 'swp_spc_l3i'

file_datepart_format_spc = '%Y%m%d'

spc_file_ext = '.cdf'
"""str: extension of SPC data files"""

spc_data_index_col = 0
spc_cdf_vars = [10, 11, 12, 13, 16, 17, 47, 81]
spc_cdf_varnames = ['np1f','np1f_u','wp1f','wp1f_u','vp1f_rtn','vp1f_rtn_u','sat_pos','general_flag']
# spc_cdf_varnames = None
spc_errorval = -1e31

# ---X----------------------------X---

# --- figure settings ---

# default fig height and width in inches
figH = 10
figW = 13

# image file extension to use [png, jpg, svg, pdf, eps]
figfile_ext = '.png'

# TODO change case, change place, to make sense
RANGEAU = 5

# --- settings for calculating the break scale ---

# time per chunk in seconds
chunktime = 128
chunktime_str = str(int(chunktime)) + 's'

# minimum data rate
min_rate = 64

# log10 frequency ratio
freqratio = 1.05

# degree of the polynomial fit to apply (piece-wise)
fitfunction_deg = 1

# constant to widen the steep frequency range by [Notes 1]
freqConst = 1.3

# constants to help find the minimum and mid-points of the inertial range
inertial_mid_const = 10
inertial_min_const = 40

