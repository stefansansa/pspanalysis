# README #

This project is for the analysis of PSP data. Code is translated from Matlab files written by R Wicks.

This code is written in Python, utilising Pandas DataFrame structures to handle time series data.
CDF data files from PSP are read using the `pycdf` library from `spacepy` (version 0.2.2). 

This is meant to be a collection of helper functions to do the heavy lifting of reading and processing the PSP data, allowing for analysis code that actually does useful scientific work to be added to this codebase, utilising the helper functions.
    
## How to run ##

1. Make all user-defined changes in the `env.py` file
    - set dates, file save locations, etc.
1. Run the `main()` function in `pspbreaks.py`
    - I use `ipython`, so I usually go (in IPython terminal):
        - `>>> import pspbreaks as ppp; ppp.main()`
1. The data and plot files will be saved in the locations specified in the `env.py` file

### main() function ###

Run the `main()` function in the `pspbreaks.py` file. 
This function will:

1. interpret user-set time instance `time_selected` in `env.py` 
1. interpret the work to be done from the **calculation and processing** section of `env.py`
1. call the relevant functions to do this work:
    - `break_freq_download_calc()` to download relevant data and eventually calculate the break frequency
    - `spectraplot()` to make plots of the spectrum and break frequency and save the image files 
    - `len_scale_download_calc()` to download relevant data and eventually calculate the length scale
    - the assumption is that similar functions and appropriate `env` flags will be added to do further calculations

### Break frequency calculation ###

* `break_freq_download_calc()`
    - call `psp_urlbuilder()` to generate the appropriate url according to `env.py` flags and the known URL / file naming conventions
    - call `download_data()` with full URL to download the relevant file
    - call `load_cdf_file()` to load the CDF data set from the downloaded data file
    - call `read_mag_data()` with CDF data variable to read the MAG instrument data and return pandas dataframe with appropriate variables as defined in `env.py`
    - call `mag_break_freq_calc()`  with the pandas DF of MAG data to 
        - calculate trace FFT of MAG (`psp_trace_fourier_power()`) 
        - and estimate the break frequency at every time chunk (`chunktime` in `env.py`)
    
### Length Scale Calculation ###

* `len_scale_download_calc()`
    - call `psp_urlbuilder()` to generate the appropriate url according to `env.py` flags and the known URL / file naming conventions
    - call `download_data()` with full URL to download the relevant SPI and SPC files
    - call `load_cdf_file()` to load the CDF data sets from the downloaded data files
    - get overlap between the three data sets (break-frequency time series BFT, SPI, SPC)
    - resample SPI and SPC data to same time-resolution as BFT
    - send relevant arguments to `calc_lengthscale()` to calculate the lengthscale


## TODO ##

* Testing
    * Absolutely no real testing was done on this code. I can only offer vague guarantee that this will run on my own laptop, nothing more.
    * I ran this with
        - python 3.7.4
        - matplotlib 3.1.3
        - pandas 0.25.3
        - spacepy 0.2.2
* Enable saving to matlab `.mat` file
    * This can be done, but I need to change the way the data is stored within python
* Comparison of SPI and SPC data quality to see if it is possible to automatically select the instrument that provides more useful data 

# Tutorial

## Startup and run

Start ipython. 
I use the `pylab` extension that automatically loads plotting and numerical packages, making the shell interface more interactive 
(somewhat like Matlab command-prompt). 

`$ ipython --pylab`

Once Ipython has started, import `pspbreaks`.
It will load the `env.py` file automatically:

`>>> import pspbreaks as ppp`

Run the main function, **after setting your ENV variables** (see next section below):

`>>> spi,spc,mag = ppp.main()`

The main function returns the dataframes containing the SPI, SPC and MAG data.


## Set user-defined variables

This file is an attempt to isolat user choice from the actual code.
It's not perfect yet and as you'll see in the `pspbreaks.py` functions, there is still some 
*"hard-coding"* of things that should rather be listed here.

I wrote doc-strings for each of the variables.
Read them from `env.py` directly; doc-strings for variables are not really python-supported, so you can't print them out like for a function, but still good coding practise for user ENV variables.
If some are missing / badly written tell me.

1. Repository location
    - `repo_dir = os.path.expanduser('./')`
    - The `./` refers to the current location, i.e. *where `env.py` is right now*
    - Be careful setting this. I'm not responsible for you overwriting / losing data.
    - Should there be an explicit OK from the user? I.e. before running anything the full repo location is printed out and the user is asked to OK this directory, notifying them that new directories and files will be created in this location.
1. Path to CDF library
    - `cdflib_dir = os.path.expanduser('~/local/cdf/lib')`
    - This is the location on my system. Set it for yours. 
    - See <https://spacepy.github.io/pycdf.html?highlight=cdf>
1. Print lots of messages while running:
    - `verbose = True`
    - This flag enables the function `printmessage()` to print to screen. If `verbose=False` then these messages won't be printed.
1. Set the work to be done
    - `calc_break_freq_flag = True`
    - `calc_len_scale_flag = False`
    - These flags decide which functions are run in `main()`
    - This is a workaround / shortcut to set the workflow for now. Eventually you'll write your own 
    functions and just call the helpers in the `pspbreaks.py` module.
1. Set the timeframe
    - `time_selected = '2019040406'`
    - Note the format YYYYMMDDHH
        - this is set by `file_datepart_format_mag = '%Y%m%d%H'` but shouldn't be changed without good reason
    - Whatever you set here, the date/time is floored to the nearest `mag_time_delta`. The cadence of the MAG instrument files.
    This ensures that if you are interested in 1032 UT on 2020/01/02 the files will be downloaded corresponding to 
        - MAG: 2020/01/02 06:00 UT
        - SPI / SPC: 2020/01/02 (whole day)
1. Instrument specific settings: MAG
    - Similar for SPI and SPC instruments. Not discussed in the README.
    - `mag_qflag_max = 0`
        - The maximum that the quality flag can be. This means that all data with `qflag > 0` will be discarded by setting that instance to NaN (see `mask_bad_quality()`)  
    - `mag_data_index_col = 0`
        - This is the number of the CDF-data variable that contains the epoch information.
        I choose to address CDF variables by number (zero-indexed) rather than name. 
        This may come back to bite us later, but this is what the ENV file is for.
        - Explicitly giving an index column tells `cdf_to_df()` how long the variables are.
        A CDF is a collection of variables in a mini database, and these variables need not be the same length. *A CDF is not a matrix*.
        If the given `index_col` is of length `N` then `cdf_to_cdf()` knows to only convert variables of length `N` to a PANDAS DataFrame (DF). 
    - `mag_cdf_vars = [1, 5]`
        - Which variables to use. Addressed by number and not name.
        - If `mag_cdf_vars=None` **and** `mag_data_index_col=None` then `cdf_to_cdf()` takes the set of variables with longest length and converts to a DF.
    - `mag_cdf_varnames = ['BRTN', 'rate']`
        - If we want to give explicit, custom names to our variables, then use this to name them.
        - Note: length of this list must be the same length as the numbered list of variables `mag_cdf_vars`
        - Note: multi-component variables (like `BRTN` -- magnetic field in RTN comps) are subscripted by `__i` where `i` is the zero-indexed number of the component. Note the doulbe "_".
        I.e. in this case `BRTN` will be realised in the DF as three separate variables:
            - `BRTN__0` (R)
            - `BRTN__1` (T)
            - `BRTN__1` (N)
    - `file_datepart_format_mag = '%Y%m%d%H'`
        - Denotes the date format in the filename.
        - C-like datestring format descriptors. 
    - `mag_time_delta = '6H'`
        - Cadence at which new data files are created. This is critical to ensure right data is utilised for the specific period.
        - Note the format: e.g. 6H for 6 hours, 1d for 1 day, etc.
        - See <https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#offset-aliases> for full list of frequency descriptors.
    - `mag_data_version = 'v01'`
        - Version of MAG data.
    - `mag_file_ext = '.cdf'`
        - File extension. 
    - `baseurl_mag = 'http://research.ssl.berkeley.edu/data/psp/data/sci/fields/l2/mag_RTN'`
        - Base URL of the PSP data website
    - `file_flags_mag = 'fld_l2_mag_RTN'`
        - Flags in the file naming format that denotes instrument details.
        Change this to work with different instrument / level.
    - `mag_errorval = -1e31`
        - The value used in CDF data file to denote missing value.
    


# Notes

1. Why `freqConst=1.3` in `env.py`? This is a bit similar to the last point, I know that the steep spectral range is less than a decade in frequency wide, the gradient fitting window is a factor of 2.5, so I want it to be a bit wider that that in each direction (lower and higher f) so that I can catch at least two but maybe 3 independent meaurements of the gradient. 
1. Why `inertial_mid_const = 10`, `inertial_min_const = 40` in `env.py`?
    1. Constants to help find the minimum and mid-point of the inertial range. 
    1. Trying to fit the inertial range, which I know is a wideband feature, but we are using the low frequency end of the FFT, which has the poorest sampling by the FFT that has been over-sampled by the interpolation, so I want a wide range of frequencies to the low frequency end. However, I know that the spectral break should be between the central frequency here and the value Highw_Range(SteepFreq),so I divide by 10, since that should get the frequency low enough to be in the inertial range, but then multiply by 2 to get high frequency for the fit so that I do not get too close to the break, and divide by 4 (making /40 in total) so that my overall fitting window is x8 width but is biased to lower frequencies.