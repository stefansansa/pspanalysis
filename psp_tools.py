"""
Helpers for `pspbreaks.py`

Called by functions in `pspbreaks` module.


"""

import env
import os
import datetime as dt
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def psp_urlbuilder(dt_user, dt_fmt, baseurl, file_hdr, file_flags, data_version,
                   file_ext):
    """
    Build url for PSP MAG data download
    Args:
        file_ext:
        data_version:
        file_flags:
        file_hdr:
        baseurl:
        dt_fmt:
        dt_user: [pandas datetime] User datetime floored to appropriate
        frequency that matches the MAG file frequency

    Returns:
        url_full: [str] full path url to the file matching `dt_user`

    """
    # create datetime string
    # dt_user_str = dt_user.strftime(env.file_datepart_format_mag)
    dt_user_str = dt_user.strftime(dt_fmt)

    # build URL
    url_0 = '/'.join([baseurl, str(dt_user.year), str(dt_user.month).zfill(2)])

    # build filename
    filename = '_'.join([file_hdr, file_flags, dt_user_str, data_version]) + file_ext

    url_full = '/'.join((url_0, filename))

    return url_full


def print_fn_only(fn_str, dir_del='/'):
    """
    Only print the filename part of a full path.

    Args:
        fn_str: [str] full file path
        dir_del: [str] directory delimiter (usually "/")

    Returns:
        only the last part of the path, after the last instance of <dir_del>

    """
    return fn_str[fn_str.rfind(dir_del) + 1:]


def download_data(url, outpath=None):
    """
    Download file from a URL

    Args:
        url: [str] URL pointing to the file
        outpath: [str] Path pointing to save location of the downloaded file

    Returns:
        responsecode: [int] Response code from the `requests.get()` function. `responsecode==200` denotes succes.
    """

    import shutil
    import urllib.request as request
    from contextlib import closing
    import requests

    fileurl = url
    filename = print_fn_only(url, dir_del='/')
    print(filename)

    if outpath is None:
        outpath = './' + filename

    printmessage('Downloading >>> \n>> %s' % fileurl)

    output_filepath = os.path.join(outpath, filename)

    # check if file exists
    if os.path.exists(output_filepath):
        print('File < %s > exists. Skipping download\n' % filename)
        return 1

    try:
        responsecode = requests.get(fileurl).status_code
        if responsecode == 200:
            with closing(request.urlopen(fileurl)) as r:
                # with open(os.path.join(env.data_dir, filename_mag), 'wb') as f:
                with open(output_filepath, 'wb') as f:
                    shutil.copyfileobj(r, f)
            printmessage('file saved as\n--> %s\n' % output_filepath)
            result = 2
        else:
            raise ConnectionError('File not found at the given URL.')
    except ConnectionError as err:
        print(err)
        return env.ERRORVAL

    return result


def load_cdf_file(fname):
    """
    Wrapper function to load a CDF file.

    Args:
        fname:

    Returns:
        cdf_data_open

    """

    # set location of CDF library. Not sure why this is needed, but pycdf
    # doesn't work otherwise
    os.environ["CDF_LIB"] = env.cdflib_dir

    from spacepy import pycdf

    printmessage('Opening %s' % print_fn_only(fname))

    # try to get the data, throw error if not possible
    try:
        if os.path.exists(fname):
            cdf_data_open = pycdf.CDF(fname)
        else:
            raise FileExistsError('ERROR: File %s does not exist.' % fname)
    except FileExistsError as err:
        print(err, 'Returning ERRORVAL (%f)' % env.ERRORVAL)
        return env.ERRORVAL

    return cdf_data_open


def callingcdf(cdf_dat, index_col=None, cdf_vars=None, varnames_list=None,
               missing_value=None):
    """
    This is just a wrapper for cdf_to_df, wrapped in try-catch block to deal
    with errors.

    Args:
        cdf_dat: [cdf] CDF variable
        index_col: [int] The column to use as index
        cdf_vars: [list of int] List of variables to use from the CDF
        varnames_list: [list of str] List of variable names to use
        missing_value: [float] Missing value for CDF

    Returns:
        df: [pandas DataFrame] Appropriate variables stored in a dataframe.

    """
    try:
        df = cdf_to_df(cdf_dat, index_col=index_col, cdf_vars=cdf_vars,
                       varnames_list=varnames_list)
    except (TypeError, ValueError) as err:
        print(err, 'Returning EMPTY DataFrame.')
        df = pd.DataFrame()

    return df


def cdf_to_df(cdf_dat, index_col=None, cdf_vars=None, varnames_list=None,
              missing_value=None):
    """
    Convert CDF data structure to PANDAS DataFrame. If neither `index_col` nor
    `cdf_vars` are supplied the choice is made to use the set of longest-length
    variables for the DataFrame.

    Args:
        cdf_dat: [cdf] The SPC CDF data structure
        index_col: [int] column number to use for index
        cdf_vars: [list of int] List of variable index numbers to read from CDF file
        varnames_list: [list of str] List of variable names if not want to use the CDF names
        missing_value: [float] value to indicate missing data

    Returns:
        pd_df: [pandas DataFrame] The dataframe containing the data
    """

    # if variable selection not specified
    if cdf_vars is None:
        lenlist = [np.max(cdf_dat[k].shape) for k in cdf_dat.keys()]
        # if index column also not given, pick the variables of maximum length (we assume this is the data)
        if index_col is None:
            # max length
            maxL = np.max(lenlist)
            printmessage('No index_col given, taking all variables matching max length (%d)' % maxL)
            cdf_vars = np.where(lenlist == maxL)[0]
            pd_df = pd.DataFrame(index=None)
        else:
            # select the variables matching the length of the user-supplied index column
            if type(index_col) is int:
                ix_col_len = len(cdf_dat[index_col])
                printmessage('Using index_col [%d], length %d' % (index_col, ix_col_len))
            elif type(index_col) is str:
                ix_col_len = len(cdf_dat[index_col])
                printmessage('Using index_col [%s], length %d' % (index_col, ix_col_len))
            else:
                raise TypeError('TYPE_ERROR: index_col must be type [str] or [int]')

            cdf_vars = np.where(np.array(lenlist) == ix_col_len)[0]
            pd_df = pd.DataFrame(index=cdf_dat[index_col][:])
    else:
        if index_col is None:
            printmessage('No index column supplied')
            pd_df = pd.DataFrame()
        else:
            pd_df = pd.DataFrame(index=cdf_dat[index_col][:])
            # remove index_col from list of vars
            cdf_vars = list(filter(index_col.__ne__, cdf_vars))

    sub_comp_warn_flag = False

    # list of all variables in the CDF file
    if varnames_list is None:
        printmessage('No varnames given, using CDF names')
        varnames_list = []
        for k in cdf_vars:
            varnames_list.append(cdf_dat[k].name())
    elif len(varnames_list) != len(cdf_vars):
        raise ValueError('VALUE_ERROR: Length of <varnames_list> (%d) and <cdf_vars> (%d) '
                         ' don\'t match; using <cdf_vars> '
                         'with original variable names.\n' %
                         (len(varnames_list), len(cdf_vars)))

    # for every variable in the list, assign to DF
    for vi in range(len(cdf_vars)):
        v = cdf_vars[vi]
        varname = varnames_list[vi]
        varshape = cdf_dat[v].shape

        if len(varshape) == 2:
            # if it is a multi-column variable, then use subscripts
            for j in range(varshape[1]):
                # add subscript to variable name (note double __)
                varname = varnames_list[vi] + '__' + str(j)
                pd_df[varname] = cdf_dat[v][:, j]
                sub_comp_warn_flag = True
                print('Multi-component: %s' % varname)
        elif len(varshape) == 1:
            # else if only single column, do as usual
            # print('%d: %s' % (v, varname))
            pd_df[varname] = cdf_dat[v][:]
        else:
            # if it has more than two dimensions, then quit
            raise ValueError('VALUE_ERROR: Only CDF variables of dimension '
                             '1 or 2 can be handled. %s dimension too high.'
                             % varname)

    # mask errors with NAN
    if missing_value is not None:
        pd_df.mask(pd_df == missing_value, inplace=True)

    if sub_comp_warn_flag:
        print('DF created with variables: ')
        print(list(pd_df.columns))
        print('\nNote mutli-component vars with subscripts __0, __1, etc.')

    return pd_df


def printmessage(s, warnflag=False):
    """
    Only prints the message to screen if ENV.VERBOSE is TRUE.

    Args:
        s: [str] message to be printed
        warnflag: [bool] If it's a warning, then print it regardless of verbose flag setting

    Returns:

    """
    if env.verbose:
        print(s)
    elif warnflag:
        print(s)

    return


def mask_bad_quality(df, qual_flag, qual_flag_max, maskval=np.nan):
    """
    Mask bad quality data in a PANDAS DATAFRAME.

    Args:
        df: [pandas dataframe] the data frame
        qual_flag: [str] name of the quality flag variable
        qual_flag_max: [float] number of maximum good quality flag
        maskval: [float] value to mask with (default is NAN)

    Returns:
        df: [pandas dataframe] masked dataframe

    """
    qual_flag_values = df[qual_flag].values.copy()
    df[df[qual_flag] > qual_flag_max] = maskval
    df[qual_flag] = qual_flag_values

    if df.dropna().shape[0] == 0:
        printstr = 'WARNING: quality_flag drops all data in DF\n' \
                   'qual_flag_max is %f,  max(qualflag) is %f'\
                    % (qual_flag_max, df[qual_flag].max())
        printmessage(printstr, warnflag=True)

    return df


def smooth(x, n=5):
    """
    Generic smoothing function. At this stage it is just running mean with
    window width set by n.

    Args:
        x: [ndarray] Signal to smooth
        n: [int] Window width

    Returns:
        xs: [ndarray] Smoothed signal of same length as *x*
    """
    xs = np.convolve(x, np.ones(n) / n, mode='same')

    return xs


def fit(x, y, deg=1, fullyes=False):
    """
    Fit function wrapper that calls `nupmpy.polyfit`. Returns the fit parameters as well as the standard deviation
    of the fit.

    Args:
        x: [ndarray] X-coordinates of the data points
        y: [ndarray] Y-coordinates of the data points (same shape as *x*)
        deg: [int] Degree of the polynomial
        fullyes: [boolean] Whether to return the full set of arguments from the fit function or not.
        Argument forwarded to `numpy.polyfit()`

    Returns:
        fitpars: [list] List of fit parameters containing at least the polynomial coefficients, and also residuals,
        rank, etc. See numpy.polyfit for full details.
        fitpars_std: [numpy.ndarray] The standard deviation of each fit parameter estimate.

    """

    try:
        if np.any(np.isnan(x + y)):
            raise ValueError('Input argument *x* or *y* contains NAN.')
    except ValueError as err:
        err_fitpars = env.ERRORVAL*np.ones(deg+1)
        err_cov = env.ERRORVAL*np.ones(deg+1)
        return err_fitpars, err_cov

    if fullyes:
        fitpars, cov = np.polyfit(x, y, deg=deg, cov=True)
        fitpars_std = np.sqrt(np.diag(cov))
    else:
        fitpars = np.polyfit(x, y, deg=deg)
        fitpars_std = env.ERRORVAL

    return fitpars, fitpars_std


def savepickle(picklefilename=None, varslist=[]):
    """
    Save a list of variables in to a single pickle file.

    Args:
        picklefilename: [str] Name of the pickle file to save to, including extension (usually `.pkl`)
        varslist: [list] List of variables to convert to pickle

    Returns:

    """
    import pickle

    if picklefilename is None:
        nou = dt.datetime.now()
        picklefilename = dt.datetime.strftime(nou, env.pkl_default_filename)

    if isinstance(varslist, pd.DataFrame):
        varslist.to_pickle(picklefilename)
    elif isinstance(varslist, list):
        try:
            if len(varslist) < 1:
                raise ValueError('ERROR: No data variables given. `varslist` is empty. ')
        except ValueError as err:
            print(err, 'No file created. Exiting.')
            return

        with open(picklefilename, 'wb') as f:
            pickle.dump(varslist, f)
    else:
        try:
            raise TypeError('Input argument *varslist* has to be a *list* or *pandas.DataFrame*')
        except TypeError as err:
            print(err, 'File not saved, continuing')

    return


def replace_filename_extension(oldfilename, newextension, addon=False):
    """
    Replace the extension of the file name with *newextension*

    Args:
        oldfilename: [str] file name to be changed
        newextension: [str] the new extension
        addon: [boolean] whether or not to add on the new extension, or replace old extension with new

    Returns:
        newfilename: [str] filename with the new extension
    """

    # extension is the part after the last period in the filename
    dot_ix = oldfilename.rfind('.')

    # if oldfilename doesn't have extension, then just add the new extension
    if dot_ix == -1:
        addon = True

    # if desired, just add the new extension forming double extension file like `filename.old.new`
    if addon:
        dot_ix = len(oldfilename)

    newfilename = oldfilename[:dot_ix] + '.' + newextension.strip('.')

    return newfilename


def newindex(df, ix_new, interp_method='linear'):
    """
    Reindex a DataFrame according to the new index *ix_new* supplied.

    Args:
        df: [pandas DataFrame] The dataframe to be reindexed
        ix_new: [np.array] The new index
        interp_method: [str] Interpolation method to be used; forwarded to `pandas.DataFrame.reindex.interpolate`

    Returns:
        df3: [pandas DataFrame] DataFrame interpolated and reindexed to *ixnew*

    """

    # create combined index from old and new index arrays
    ix_com = np.unique(np.append(df.index, ix_new))

    # sort the combined index (ascending order)
    ix_com.sort()

    # re-index and interpolate over the non-matching points
    df2 = df.reindex(ix_com).interpolate(method=interp_method, limit=env.interpolation_limit)

    # drop all the old index points by re-indexing to new index
    df3 = df2.reindex(ix_new)

    return df3


def plot_fixrate(df, df_new, L, r):
    """
    Make a summary plot of the difference between the original data frame *df* and fixed *df_new*.

    Args:
        df: [pandas DataFrame] Original dataframe
        df_new: [pandas DataFrame] Dataframe returned by fix_data_rate()
        L: [float] The sample cycle duration
        r: [pandas DataSeries] Data rate variable of *df*

    Returns:
        999 [int]
    """

    # get the time index in to a non-index variable
    df['t'] = df.index.values

    # running difference of *t* is the *cadence* of the sampling
    df['delta'] = df.t.diff()

    # remove NANs
    df['delta'] = df.delta.fillna(pd.Timedelta(seconds=0))

    # get the time index in to a non-index variable
    df_new['t'] = df_new.index.values

    # running difference of *t* is the *cadence* of the sampling
    df_new['delta'] = df_new.t.diff()

    # remove NANs
    df_new['delta'] = df_new.delta.fillna(pd.Timedelta(seconds=0))

    # open new figure window
    plt.figure()

    # plot the delta_t
    df.delta.plot(marker='.', label='original')

    # plot the delta_t as it should be according to the data rate
    (L / r).mul(1e9).plot(label='acc to datarate')

    # plot the delta_t of the fixed-data-rate dataframe
    df_new.delta.plot(marker='x', ls='', label='fixed')

    # apply figure labels, titles, etc
    plt.ylabel('$\Delta t$ [nanoseconds]')
    plt.xlabel('Time [UT]')
    plt.grid(which='both')
    plt.title(str(df.index[0]) + ' --> ' + str(df.index[-1]))
    plt.tight_layout()
    plt.legend()

    return 999


def listsearch(search_string, input_list):
    """
    Return matching items from a list

    Args:
        search_string: [str] String to search for (starting only)
        input_list: [list] List to search in

    Returns:
        found_list: [list] List of matching items

    """

    found_list = []
    i = 0

    for si in input_list:

        if si.startswith(search_string):
            # print('%d: %s' % (i, si))
            found_list.append(si)

        i += 1

    return found_list


def window_selector(N, win_name='Hanning'):
    """
    Simply a wrapper for *get_window* from *scipy.signal*. Return the window coefficients.

    Args:
        N: [int] Window length
        win_name: [str] Name of the window

    Returns:
        w: [ndarray] Window coefficients
    """
    import scipy.signal as signal

    w = signal.windows.get_window(win_name, N)

    return w


def chunkify(ts_in, chunk_duration):
    """
    Divide a given timeseries in to chunks of *chunk_duration*

    Args:
        ts_in: [pd.Timeseries] Input timeseries
        chunk_duration: [float] Duration of the chunks in seconds

    Returns:

    """
    print('converting to chunks of len %.2f sec' % chunk_duration)

    dchunk_str = str(chunk_duration) + 'S'

    chunks_time = pd.date_range(ts_in[0].ceil('1s'), ts_in[-1].floor('1s'), freq=dchunk_str)

    return chunks_time
