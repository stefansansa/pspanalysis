import matplotlib.pyplot as plt
import numpy as np


def importComp():
    #------------------------------------------------------------------------
    
    SMALL_SIZE = 10
    MEDIUM_SIZE = 12
    BIGGER_SIZE = 18
    
    plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    #------------------------------------------------------------------------
    
    N = 100
    r = np.linspace(0.1,1.0,num = N)
    
    B_mag = np.zeros(N)
    Dens = np.zeros(N)
    Temp = np.zeros(N)
    
    V_sw = 400./1.5e8*60.*60 #solar wind speed in AU/hr
    Omega = 2.*3.14/25.4/24. #solar rotation rate in /AU
    SunBeta = Omega/V_sw #Only valid for the equatorial plane 
    
    B_earth = 3. #Magnetic field magnitude at Earth in nT
    B_0 = B_earth/np.sqrt(1. + SunBeta*SunBeta) #reference value at B_0 in nT
    
    Dens_Earth = 6. #Density at Earth; particles/cc
    
    Temp_Earth = 5.e4 # Temperature at Earth; K
    
    
    for i in range(0,N):
    	B_mag[i] =  B_0/r[i]/r[i]*np.sqrt(1. + r[i]*SunBeta*r[i]*SunBeta) #Parker HMF with source surface at 0AU
    	Dens[i] = Dens_Earth/r[i]/r[i] #1/r^2 density decrease
    	Temp[i] = Temp_Earth*r[i]**(-4./3.) #Adiabatic decrease for proton temperature
    	
    #--------------------------------------------------------------------
#    fig = plt.figure(figsize=[10,10])
    
    
#    subplot1 = fig.add_subplot(311)
#    #subplot1.set_xlabel('Time (hr)')
#    #subplot1.set_title('aap')
#    subplot1.set_ylabel('Magnetic field magnitude (nT)')
#    subplot1.set_yscale('log')
#    subplot1.set_xscale('linear')
#    #subplot1.set_ylim([0.001,10.])
#    #subplot1.ticklabel_format(useOffset=False)
#    subplot1.set_xlim([0.0,1.25])
#    subplot1.plot(r, B_mag, color = 'r', linestyle = '-')
#    #subplot1.legend()
#    
#    subplot1 = fig.add_subplot(312)
#    #subplot1.set_xlabel('Time (hr)')
#    #subplot1.set_title('aap')
#    subplot1.set_ylabel('Proton number density (#/cc)')
#    subplot1.set_yscale('log')
#    subplot1.set_xscale('linear')
#    #subplot1.set_ylim([0.001,10.])
#    #subplot1.ticklabel_format(useOffset=False)
#    subplot1.set_xlim([0.0,1.25])
#    subplot1.plot(r, Dens, color = 'b', linestyle = '-')
#    #subplot1.legend()
#    
#    subplot1 = fig.add_subplot(313)
#    subplot1.set_xlabel('r (AU)')
#    #subplot1.set_title('aap')
#    subplot1.set_ylabel('Temperature (K)')
#    subplot1.set_yscale('log')
#    subplot1.set_xscale('linear')
#    #subplot1.set_ylim([0.001,10.])
#    #subplot1.ticklabel_format(useOffset=False)
#    subplot1.set_xlim([0.0,1.25])
#    subplot1.plot(r, Temp, color = 'g', linestyle = '-')
#    #subplot1.legend()
#    
#    fig.savefig('Inputs.pdf', dpi=300, bbox_inches='tight')
    #--------------------------------------------------------------------
    
    # Everythinig now in SI units!
    # I define all quantities as in https://ui.adsabs.harvard.edu/abs/2018ApJ...856..159E/abstract
    
    # define physical constants
    mp = 1.6726219e-27  # mass of a proton, kg
    kB = 1.3806485e-23  # Boltzmann’s constant, m2 kg s-2 K-1
    ec = 1.60217662e-19 #Elementary charge, C
    u0 = 1.25663706e-6 #Permeability of free space, m kg s-2 A-2
    e0 = 8.85418782e-12 #Permittivity of free space, m-3 kg-1 s4 A2
    c = 2.99792458e8 #Speed of light, m/s
    
    B_mag_new = B_mag*1e-9 #Magnetic field now in T
    Dens_new = Dens*100.*100.*100. #Density now in particles/m^3
    
    V_therm_proton = np.zeros(N) #Thermal proton speed
    V_A = np.zeros(N) #Aflven speed
    Omega_p = np.zeros(N) #Proton cyclotron frequency
    w_p = np.zeros(N) #Proton plasma frequency
    kd = np.zeros(N) #Proton cyclotron resonance (this is wavenumber; not length scale...)
    p_g = np.zeros(N) #Proton gyroscale
    p_ii = np.zeros(N) #Proton inertial lentgh
    
    V_therm_proton = np.sqrt(3.*kB*Temp/mp) #in m/s
    Omega_p = ec*B_mag_new/mp #in rad/s
    w_p = np.sqrt(Dens_new*ec*ec/mp/e0) #in rad/s
    V_A = c*Omega_p/w_p# in m/s
    
    kd = Omega_p/(V_A + V_therm_proton) #in 1/m
    l_d = 1./kd #in m
    p_g = V_therm_proton/Omega_p #in m
    p_ii = V_A/Omega_p #in m
    
    #--------------------------------------------------------------------
#    fig = plt.figure(figsize=[10,10])
#    
#    
#    subplot1 = fig.add_subplot(311)
#    #subplot1.set_xlabel('Time (hr)')
#    #subplot1.set_title('aap')
#    subplot1.set_ylabel('Frequency (rad/s)')
#    subplot1.set_yscale('log')
#    subplot1.set_xscale('linear')
#    #subplot1.set_ylim([0.001,10.])
#    #subplot1.ticklabel_format(useOffset=False)
#    subplot1.set_xlim([0.0,1.25])
#    subplot1.plot(r, Omega_p, color = 'r', linestyle = '-', label = 'Proton cyclotron frequency')
#    subplot1.plot(r, w_p, color = 'b', linestyle = '--', label = 'Proton plasma frequency')
#    subplot1.legend()
#    
#    subplot1 = fig.add_subplot(312)
#    #subplot1.set_xlabel('Time (hr)')
#    #subplot1.set_title('aap')
#    subplot1.set_ylabel('Speed (km/s)')
#    subplot1.set_yscale('linear')
#    subplot1.set_xscale('linear')
#    #subplot1.set_ylim([0.001,10.])
#    #subplot1.ticklabel_format(useOffset=False)
#    subplot1.set_xlim([0.0,1.25])
#    subplot1.plot(r, V_A/1000., color = 'r', linestyle = '-', label = 'Alfven speed')
#    subplot1.plot(r, V_therm_proton/1000., color = 'b', linestyle = '--', label = 'Proton thermal speed')
#    subplot1.legend()
    V_A = V_A/1000
    V_therm_proton = V_therm_proton/1000
    
#    subplot1 = fig.add_subplot(313)
#    subplot1.set_xlabel('r (AU)')
#    #subplot1.set_title('aap')
#    subplot1.set_ylabel('Length scale (km)')
#    subplot1.set_yscale('log')
#    subplot1.set_xscale('linear')
#    #subplot1.set_ylim([0.001,10.])
#    #subplot1.ticklabel_format(useOffset=False)
#    subplot1.set_xlim([0.0,1.25])
#    subplot1.plot(r, l_d/1000., color = 'r', linestyle = '-', label = 'Cyclotron resonance scale')
#    subplot1.plot(r, p_g/1000., color = 'b', linestyle = '-', label = 'Proton gyroscale')
#    subplot1.plot(r, p_ii/1000., color = 'g', linestyle = '-.', label = 'Proton inertial length')
#    subplot1.legend()
    l_d = l_d/1000
    p_g = p_g/1000
    p_ii = p_ii/1000
    
#    fig.savefig('plasma_quantities.pdf', dpi=300, bbox_inches='tight')
    #--------------------------------------------------------------------
#    plt.show()
    
    return r, B_mag, Dens, Temp, Omega_p, w_p, V_A, V_therm_proton, l_d, p_g, p_ii 
